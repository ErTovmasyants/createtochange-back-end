<?php

namespace TypiCMS\Modules\Comics\Composers;

use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Gate;
use Maatwebsite\Sidebar\SidebarGroup;
use Maatwebsite\Sidebar\SidebarItem;

class SidebarViewComposer
{
    public function compose(View $view)
    {
        if (Gate::denies('read comics')) {
            return;
        }
        $view->sidebar->group(__('Content'), function (SidebarGroup $group) {
            $group->id = 'content';
            $group->weight = 30;
            $group->addItem(__('Comics'), function (SidebarItem $item) {
                $item->id = 'comics';
                $item->icon = config('typicms.comics.sidebar.icon');
                $item->weight = config('typicms.comics.sidebar.weight');
                $item->route('admin::index-comics');
                $item->append('admin::create-comic');
            });
        });
    }
}
