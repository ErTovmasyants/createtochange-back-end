<?php

namespace TypiCMS\Modules\Comics\Facades;

use Illuminate\Support\Facades\Facade;

class ComicCategories extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'ComicCategories';
    }
}
