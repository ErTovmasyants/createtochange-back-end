<?php

namespace TypiCMS\Modules\Comics\Facades;

use Illuminate\Support\Facades\Facade;

class Comics extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'Comics';
    }
}
