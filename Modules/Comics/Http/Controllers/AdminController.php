<?php

namespace TypiCMS\Modules\Comics\Http\Controllers;

use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Maatwebsite\Excel\Facades\Excel;
use TypiCMS\Modules\Core\Http\Controllers\BaseAdminController;
use TypiCMS\Modules\Comics\Exports\Export;
use TypiCMS\Modules\Comics\Http\Requests\FormRequest;
use TypiCMS\Modules\Comics\Models\Comic;
use TypiCMS\Modules\Comics\Models\ComicCategory;
use TypiCMS\Modules\Versions\Models\Version;
use TypiCMS\Modules\Users\Models\User;
use Illuminate\Support\Facades\Validator;
use TypiCMS\Modules\Core\Services\FileUploader;
use Illuminate\Support\Facades\Log;
use Croppa;
use Exception;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

class AdminController extends BaseAdminController
{
    private $path;

    public function __construct()
    {
        $this->path = 'storage/comics/';
    }

    public function index(): View
    {
        $statuses = ['active','draft','pending'];
        return view('comics::admin.index')->with(compact('statuses'));
    }

    public function indexStatus($status): View
    {
        $statuses = ['active','draft','pending'];
        return view('comics::admin.index-status')->with(compact('statuses','status'));
    } 

    public function export(Request $request)
    {
        $filename = date('Y-m-d').' '.config('app.name').' comics.xlsx';

        return Excel::download(new Export($request), $filename);
    }

    public function create(): View
    {
        $model = new Comic();

        return view('comics::admin.create')
            ->with(compact('model'));
    }

    public function edit(Comic $comic): View
    {
        $user = User::select(DB::raw("CONCAT(first_name,' ',last_name) as full_name"))->where('id',$comic->user_id)->first();
        return view('comics::admin.edit')
            ->with(['model' => $comic,'user' => $user]);
    }

    public function pendingview(Comic $comic): View
    {
        $category = ComicCategory::published()->where('id',$comic->category_id)->get()->first();
    $current_category = ComicCategory::published()
        ->with('image')
        ->whereSlugIs($category->slug)
        ->firstOrFail();

    $model = Comic::where('status', 0)
        ->where('draft', 0)
        ->where('type', $comic->type)
        ->with('category')
        ->where('category_id', $current_category->id)
        ->where('slug', $comic->slug)->get()->first();

    if (!$model) {
        abort('404');
    }

    $vmodel = Version::where('comics_id', $model->id)->where('parent_id', 0)->get()->first();

    return view('comics::public.show')
        ->with(compact('model', 'vmodel'));
    }

    public function store(FormRequest $request): RedirectResponse
    {
        $comic = Comic::create($request->validated());
        return $this->redirect($request, $comic);
    }

    public function update(Comic $comic, FormRequest $request,FileUploader $fileUploader): RedirectResponse
    {
        $data = $request->except('_token', 'image','exit');
  
        if ($request->hasFile('image')) {
            $validator = Validator::make($request->all(), [
                'image' => 'image',
            ]);
            if ($validator->passes()) {
                $file = $fileUploader->handle($request->file('image'),  'comics/'.$comic->user_id.'/comics_'.$comic->id,'public');
                $data['image'] = $file['filename'];
            }
        }

        if ($request->input('slug')  && !strpos($data['slug'],hash('crc32b', $comic->user_id.$comic->id))) {
            $data['slug'] =  $data['slug'].'-'.hash('crc32b', $comic->user_id.$comic->id);
            $comic->update($data);
        }
        $comic->update($data);

        return $this->redirect($request, $comic);
    }

    public function makeDir($comic){
        $path =  $this->path.$comic->user_id;
        if (!File::exists($path)) {
            Storage::makeDirectory($path, 0755, true, true);
        }
    }

    public function deleteImage($comic): void
    {
        $this->makeDir($comic);
        if ($filename = Comic::where('id', $comic->user_id)->value('image')) {
            try {
                Croppa::delete( $this->path.$comic->user_id.'/'.$filename);
            } catch (Exception $e) {
                Log::info($e->getMessage());
            }
        }
    }
}
