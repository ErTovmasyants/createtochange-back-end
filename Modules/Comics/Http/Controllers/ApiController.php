<?php

namespace TypiCMS\Modules\Comics\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;
use TypiCMS\Modules\Comics\Models\Comic;
use TypiCMS\Modules\Core\Filters\FilterOr;
use TypiCMS\Modules\Core\Http\Controllers\BaseApiController;
use TypiCMS\Modules\Users\Models\User;

class ApiController extends BaseApiController
{
    public function index(Request $request): LengthAwarePaginator
    {
      
        $data = QueryBuilder::for(Comic::class)
            ->selectFields('comics.id,comics.title,comics.type,comics.user_id,comics.status,comics.draft,comics.updated_at')
            ->selectSub(User::select('nickname')->whereColumn('user_id', 'users.id'), 'nickname')
            ->allowedSorts(['comics.title', 'users.nickname', 'comics.type','updated_at'])
            ->join('users', 'comics.user_id', '=', 'users.id')
            ->allowedFilters([
                AllowedFilter::custom('comics.title,users.nickname,comics.type,comics.updated_at', new FilterOr()),
            ])
            ->allowedIncludes(['image'])
            ->paginate($request->input('per_page'));
        return $data;
    }

    public function indexByStatus($param, Request $request): LengthAwarePaginator
    {
        $status = 0;
        $draft = 0;
        if ($param) {
            if ($param == 'active') {
                $status = 1;
            } elseif ($param == 'draft') {
                $draft = 1;
            }
        }
        $data = QueryBuilder::for(Comic::class)
        ->selectFields('comics.id,comics.title,comics.type,comics.user_id,comics.status,comics.draft,comics.updated_at')
        ->selectSub(User::select('nickname')->whereColumn('user_id', 'users.id'), 'nickname')
        ->allowedSorts(['comics.title', 'users.nickname', 'comics.type','updated_at'])
        ->join('users', 'comics.user_id', '=', 'users.id')
            ->where('status', $status)
            ->where('draft', $draft)
            ->allowedFilters([
                AllowedFilter::custom('comics.title,users.nickname,comics.type,comics.updated_at', new FilterOr()),
            ])
            ->allowedIncludes(['image'])
            ->paginate($request->input('per_page'));

        return $data;
    }

    protected function updatePartial(Comic $comic, Request $request)
    {
        foreach ($request->only('status') as $key => $content) {
            if ($comic->isTranslatableAttribute($key)) {
                foreach ($content as $lang => $value) {
                    $comic->setTranslation($key, $lang, $value);
                }
            } else {
                $comic->{$key} = $content;
            }
        }

        $comic->save();
    }

    public function destroy(Comic $comic)
    {
        $comic->delete();
    }
}
