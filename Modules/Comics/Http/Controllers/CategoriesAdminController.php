<?php

namespace TypiCMS\Modules\Comics\Http\Controllers;

use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use TypiCMS\Modules\Core\Http\Controllers\BaseAdminController;
use TypiCMS\Modules\Comics\Http\Requests\CategoryFormRequest;
use TypiCMS\Modules\Comics\Models\Comic;
use TypiCMS\Modules\Comics\Models\ComicCategory;

class CategoriesAdminController extends BaseAdminController
{
    public function index(): View
    {
        return view('comics::admin.index-categories');
    }

    public function create(): View
    {
        $model = new ComicCategory();

        return view('comics::admin.create-category')
            ->with(compact('model'));
    }

    public function edit(ComicCategory $category): View
    {
        return view('comics::admin.edit-category')
            ->with(['model' => $category]);
    }

    public function store(CategoryFormRequest $request): RedirectResponse
    {
        $category = ComicCategory::create($request->validated());

        return $this->redirect($request, $category);
    }

    public function update(ComicCategory $category, CategoryFormRequest $request): RedirectResponse
    {
        $category->update($request->validated());
        (new Comic())->flushCache();

        return $this->redirect($request, $category);
    }
}
