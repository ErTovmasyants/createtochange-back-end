<?php

namespace TypiCMS\Modules\Comics\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;
use TypiCMS\Modules\Core\Filters\FilterOr;
use TypiCMS\Modules\Core\Http\Controllers\BaseApiController;
use TypiCMS\Modules\Comics\Models\Comic;
use TypiCMS\Modules\Comics\Models\ComicCategory;

class CategoriesApiController extends BaseApiController
{
    public function index(Request $request): LengthAwarePaginator
    {
        $data = QueryBuilder::for(ComicCategory::class)
            ->selectFields($request->input('fields.comic_categories'))
            ->allowedSorts(['status_translated', 'position', 'title_translated'])
            ->allowedFilters([
                AllowedFilter::custom('title', new FilterOr()),
            ])
            ->allowedIncludes(['image'])
            ->paginate($request->input('per_page'));

        return $data;
    }

    protected function updatePartial(ComicCategory $category, Request $request)
    {
        foreach ($request->only('status', 'position') as $key => $content) {
            if ($category->isTranslatableAttribute($key)) {
                foreach ($content as $lang => $value) {
                    $category->setTranslation($key, $lang, $value);
                }
            } else {
                $category->{$key} = $content;
            }
        }
        (new Comic())->flushCache();
        $category->save();
    }

    public function destroy(ComicCategory $category)
    {
        if ($category->comics->count() > 0) {
            return response(['message' => 'This category cannot be deleted as it contains comics.'], 403);
        }
        $category->delete();
    }
}
