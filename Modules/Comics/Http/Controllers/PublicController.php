<?php

namespace TypiCMS\Modules\Comics\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\View\View;
use TypiCMS\Modules\Comics\Http\Requests\FormRequest;
use TypiCMS\Modules\Comics\Models\Comic;
use TypiCMS\Modules\Comics\Models\ComicCategory;
use TypiCMS\Modules\Core\Http\Controllers\BasePublicController;
use TypiCMS\Modules\Core\Services\FileUploader;
use TypiCMS\Modules\Users\Models\User;
use TypiCMS\Modules\Versions\Http\Requests\FormRequest as VFR;
use TypiCMS\Modules\Versions\Models\Version;

class PublicController extends BasePublicController
{
    private $comicsVersions = [];
    private $comicsVersionsarr = [];

    public function __construct()
    {
        $this->middleware('auth')->only([
            'create',
            'checkUser',
        ]);
    }

    public function index()
    {
        return redirect()->route(config('app.locale').'::comics-type', 'reading');
    }

    public function indexOfType($type)
    {
        $this->checkType($type);
        $categories = ComicCategory::published()
            ->order()
            ->get();
        $models = Comic::order()
            ->where('status', 1)
            ->where('draft', 0)
            ->where('type', $type)
            ->paginate(config('typicms.comics.front_per_page'));

        return view('comics::public.index')
            ->with(compact('models', 'categories', 'type'));
    }

    public function indexOfCategory($type, $category)
    {
        $this->checkType($type);
        $categories = ComicCategory::published()
            ->order()
            ->get();

        $current_category = ComicCategory::published()
            ->with('image')
            ->whereSlugIs($category)
            ->firstOrFail();

        $models = Comic::order()
            ->where('status', 1)
            ->where('draft', 0)
            ->where('type', $type)
            ->where('category_id', $current_category->id)
            ->paginate(config('typicms.comics.front_per_page'));

        return view('comics::public.index')
            ->with(compact('models', 'categories', 'current_category', 'type'));
    }

    public function show($type, $category, $slug): View
    {
        $current_category = ComicCategory::published()
            ->with('image')
            ->whereSlugIs($category)
            ->firstOrFail();

        $model = Comic::where('status', 1)
            ->where('draft', 0)
            ->where('type', $type)
            ->with('category')
            ->where('category_id', $current_category->id)
            ->where('slug', $slug)->get()->first();

        if (!$model) {
            abort('404');
        }

        $vmodel = Version::where('comics_id', $model->id)->where('parent_id', 0)->get()->first();

        return view('comics::public.show')
            ->with(compact('model', 'vmodel'));
    }

    public function create($step, $id = null, $type = null, $vID = null): View
    {
        $this->authCheck();
        if ($type) {
            $this->checkType($type);
        } else {
            if ($step == 2) {
                abort('404');
            }
        }

        abort_if(!$step, '404');
        $draft = [];

        if ($step != 1 && $step != 2) {
            abort('404');
        }

        if ($id) {
            $comics = Comic::where('id', $id)->where('user_id', auth()->user()->id)->get()->first();
            if (!$comics) {
                abort('404');
            }

            if ($type) {
                if ($type != $comics->type) {
                    abort('404');
                }
            }

            $draft = Comic::where('status', 0)
                ->where('draft', 1)
                ->with('category')
                ->where('user_id', auth()->user()->id)
                ->where('id', $id)->get()->first();

            if (!$draft) {
                abort('404');
            }
        }

        $categories = ComicCategory::published()
            ->order()
            ->get();

        if (isset($comics)) {
            if (!$comics->image) {
                abort('404');
            }
        }
        if ($step == 1) {
            return view('comics::public.create1')->with(compact('categories', 'draft'));
        }

        if ($step == 2) {
            if ($id) {
                $models = Version::nestable($id);
                $model = Version::where('comics_id', $id)->where('user_id', auth()->user()->id)->get()->first();
                if ($models->count() <= 0) {
                    if ($model == null) {
                        $model = Version::createFirstVersion($id);
                        $models = Version::nestable($id);
                    }
                }
                $vmodel = [];
                $imgVidoFiles = [];
                if ($vID) {
                    $vmodel = Version::where('id', $vID)->where('comics_id', $id)->where('user_id', auth()->user()->id)->get()->first();
                    if (!$vmodel) {
                        abort('404');
                    }
                    $imgVidoFiles = json_decode($vmodel->name);
                }

                return view('comics::public.create2')->with(compact('categories', 'imgVidoFiles', 'vID', 'vmodel', 'draft', 'models', 'comics', 'step', 'model', 'id', 'type'));
            }
            abort('404');
        }
    }

    public function save(FormRequest $request, FileUploader $fileUploader)
    {
        $user_id = base64_decode($request->key);
        $draft = base64_decode($request->keyDr);
        $comicsID = base64_decode($request->keyID);

        if ($draft != 1 && $draft != 0) {
            abort('404');
        }

        $nickname = User::where('id', $user_id)->where('nickname', base64_decode($request->keyVal))->pluck('nickname')->first();

        if (!$nickname) {
            abort('404');
        }

        if ($request->input('action') == 'draft') {
            $request->request->add(['step' => 1]);
        } elseif ($request->input('action') == 'next') {
            $request->request->add(['step' => 2]);
        }

        $request->request->add(['draft' => 1]);
        $request->request->add(['status' => 0]);

        $request->request->add(['user_id' => $user_id]);
        $data = $request->except('_token', 'image', 'action', 'keyVal', 'key', 'keyDr', 'keyID', 'filexist', 'pdfexist');

        $slug = mb_strtolower(preg_replace('/[[:space:]]+/', '-', $request->title));
        if ($slug) {
            $data['slug'] = $slug.'-'.hash('crc32b', $user_id.time());
        }

        if ($data['step'] != 1 && $data['step'] != 2) {
            abort('404');
        }

        if ($draft == 1) {
            $comic = Comic::where('id', $comicsID)->get()->first();
            if ($comic) {
                $comic->update($data);
            }
        } elseif ($draft == 0) {
            $comic = Comic::create($data);
            $comicsID = $comic->id;
        }

        if ($request->input('action') == 'next') {
            if (($comic->image && $request->hasFile('image') == true) || (!$comic->image && $request->hasFile('image') == true) || (!$comic->image && $request->hasFile('image') == false) || ($comic->image && $request->input('filexist') != 'filexist')) {
                $validator = Validator::make(
                    $request->all(),
                    [
                        'image' => 'image|required',
                    ]
                );

                if ($validator->fails()) {
                    return redirect()->back()
                        ->withInput()
                        ->withErrors($validator);
                }
            }

            $pdfpath = 'comics/pdf/';

            if ($request->file('pdf')) {
                $path = $pdfpath;
                $file = $fileUploader->handle($request->file('pdf'), $path, 'public');
                $data['pdf'] = $file['filename'];

                if ($comic) {
                    $comic->update($data);
                }
            } else {
                if ($request->input('pdfexist') != 'pdfexist') {
                    if (isset($comic->pdf)) {
                        Storage::delete($pdfpath.'/'.$comic->pdf);
                        $data['pdf'] = null;
                        $comic->update($data);
                    }
                }
            }
        }

        if ($request->hasFile('image')) {
            $file = $fileUploader->handle($request->file('image'), 'comics/'.$user_id.'/comics_'.$comicsID, 'public');
            $data['image'] = $file['filename'];
            Storage::delete('comics/'.$user_id.'/comics_'.$comicsID.'/'.$comic->image);
        }

        $comic = $this->CheckComics($comicsID);

        if ($comic) {
            $comic->update($data);
        }

        if ($request->input('action') == 'draft') {
            return redirect()->route(config('app.locale').'::account-user-draft', [$nickname]);
        }
        if ($request->input('action') == 'next') {
            $models = Version::nestable($comicsID);
            $model = Version::where('comics_id', $comicsID)->where('user_id', $user_id)->where('parent_id', 0)->get()->first();
            if ($models->count() <= 0) {
                if ($model == null) {
                    $model = Version::createFirstVersion($comicsID);
                }

                return redirect()->route(config('app.locale').'::comics-create', ['2', $comic->id, $comic->type, $model->id]);
            }

            return redirect()->route(config('app.locale').'::comics-create', ['2', $comic->id, $comic->type]);
        }
    }

    public function Vsave(Request $request, FileUploader $fileUploader)
    {
        $user_id = str_replace('854952goFuckYourself', '', base64_decode($request->u_htua_i));
        $usernickname = str_replace('hixc5t6565____dsfsdfdfvcv___43543543fbdfdgdfgfdgfgoFuckYourself', '', base64_decode($request->u_htua_n));
        $versionID = str_replace('63598254515616goFuckYourself', '', base64_decode($request->verID));
        $versionType = str_replace('goanimatedgoFuckYourself', '', base64_decode($request->verT));
        $comicsID = str_replace('8645314986513goFuckYourself', '', base64_decode($request->cID));
        $versionModal = Version::find($versionID);
        $nickname = User::where('id', $user_id)->where('nickname', $usernickname)->pluck('nickname')->first();
        $allimg = json_decode($request->allimgs, true);
        $videomp4 = json_decode($request->videomp4, true);
        if (!$nickname) {
            abort('404');
        }

        $this->CheckComics($comicsID);
        if ($versionType) {
            if ($versionType != 'reading' && $versionType != 'animated') {
                return redirect()->back();
            }
        } else {
            return redirect()->back();
        }

        $path = 'comics/'.$user_id.'/comics_'.$comicsID.'/version';

        if ($request->input('filexist') != 'filexist') {
            if ($versionType == 'reading') {
                $messages = [
                    'name.required' => 'Image is required.',
                    'name.*.max' => 'The image may not be greater than 2048 kilobytes.',
                ];
                $validator = Validator::make($request->all(), [
                    'name' => 'required',
                    'name.*' => 'mimes:jpeg,jpg,png|max:2048',
                ], $messages);
            } elseif ($versionType == 'animated') {
                $messages = [
                    'name.required' => 'Video is required.',
                    'name.max' => 'The video may not be greater than 100 MB.',
                ];
                $validator = Validator::make($request->all(), [
                    'name' => 'required|mimes:mp4|max:100000',
                ], $messages);
            }

            if ($validator->fails()) {
                return redirect()->back()
                    ->withInput()
                    ->withErrors($validator);
            }

            if ($request->hasFile('name')) {
                $count = 0;
                if ($versionType == 'reading') {
                    $path = $path.'/'.$versionType;
                    foreach ($request->file('name') as $uploadedFile) {
                        ++$count;
                        $file = $fileUploader->handle($uploadedFile, $path, 'public');
                        $FileData['image'.$count] = $file['filename'];
                    }
                } elseif ($versionType == 'animated') {
                    $path = $path.'/'.$versionType;
                    $file = $fileUploader->handle($request->file('name'), $path, 'public');
                    $FileData['video_mp4'] = $file['filename'];
                }
                $versionModal->name = json_encode($FileData);
            }
            $versionModal->path = $path;
        } else {
            if ($versionType == 'reading') {
                if (!empty($allimg)) {
                    $path = $path.'/'.$versionType;
                    if ($allimg != $versionModal->name) {
                        $blob = $this->find_string_in_array($allimg, 'blob');
                        if (!empty($blob)) {
                            foreach (array_keys($blob) as $key => $img) {
                                $file = $fileUploader->handle($request->file('name')[array_keys($request->file('name'))[$key]], $path, 'public');
                                $blob[$img] = $file['filename'];
                            }
                            $allimg = array_replace($allimg, $blob);
                        }
                        $versionModal->name = json_encode($allimg);
                    }
                } else {
                    $messages = [
                        'name.required' => 'Image is required.',
                        'name.*.max' => 'The image may not be greater than 2048 kilobytes.',
                    ];
                    $validator = Validator::make($request->all(), [
                        'name' => 'required',
                        'name.*' => 'mimes:jpeg,jpg,png|max:2048',
                    ], $messages);
                    if ($validator->fails()) {
                        return redirect()->back()
                            ->withErrors($validator);
                    }
                }
            }
            if ($versionType == 'animated') {
                if (!empty($videomp4)) {
                    $path = $path.'/'.$versionType;
                    if (json_encode($videomp4) != $versionModal->name) {
                        $blob = $this->find_string_in_array($videomp4, 'blob');
                        if (!empty($blob)) {
                            $file = $fileUploader->handle($request->file('name'), $path, 'public');
                            $blob['video_mp4'] = $file['filename'];
                        }
                        $versionModal->name = json_encode($blob);
                    }
                } else {
                    $messages = [
                        'name.required' => 'Video is required.',
                        'name.max' => 'The video may not be greater than 100 MB.',
                    ];
                    $validator = Validator::make($request->all(), [
                        'name' => 'required|mimes:mp4|max:100000',
                    ], $messages);
                    if ($validator->fails()) {
                        return redirect()->back()
                            ->withErrors($validator);
                    }
                }
            }
        }
        $data = $request->except('_token', 'verID', 'u_htua_i', 'verT', 'cID');

        if (isset($data['title'])) {
            if ($versionModal->title == $data['title'] && $allimg == $versionModal->name) {
                return redirect()->back()
                    ->withErrors('Nothing to change');
            }
            $versionModal->title = $data['title'];
        }

        if ($versionModal->parent_id > 0) {
            $messages = [
                'title.required' => 'Title  is required.',
            ];
            $validator = Validator::make($request->all(), [
                'title' => 'required',
            ], $messages);

            if ($validator->fails()) {
                return redirect()->back()
                    ->withErrors($validator);
            }
        }

        $versionModal->type = $versionType;
        $versionModal->save();

        return redirect()
            ->route(config('app.locale').'::comics-create', [2, $comicsID, $versionType, $versionModal->id])
            ->with('status', __('Changes Saved Successfuly.'));
    }

    public function find_string_in_array($arr, $string)
    {
        return array_filter($arr, function ($value) use ($string) {
            return mb_strpos($value, $string) !== false;
        });
    }

    public function addVersionFinish(Request $request)
    {
        $this->authCheck();
        $comicsID = str_replace('8645314986513goFuckYourself', '', base64_decode($request->cID));
        $user_id = str_replace('854952goFuckYourself', '', base64_decode($request->u_htua_i));
        $versionType = str_replace('goanimatedgoFuckYourself', '', base64_decode($request->verT));
        $this->checkType($versionType);
        $uModal = User::find($user_id);
        $cModal = $this->CheckComics($comicsID);
        if ($request->input('action') == 'draft') {
            $cModal->draft = 1;
            $cModal->status = 0;
        } elseif ($request->input('action') == 'publish') {
            $firstlevel = Version::where('comics_id', $comicsID)->where('type', $versionType)->where('parent_id', 0)->first();
            $allV = Version::where('comics_id', $comicsID)->where('type', $versionType)->where('parent_id', '>', '0')->get();

            if (empty($firstlevel->name)) {
                return redirect()->back()
                    ->withErrors("The starting Version can't be empty");
            }

            if ($allV) {
                foreach ($allV as $v) {
                    if (!$v->title) {
                        return redirect()->back()->with('errorId', $v->id)
                            ->withErrors("You can't publish comics without fill version title. Fill it or delete the version");
                    }
                    if (!$v->name) {
                        return redirect()->back()->with('errorId', $v->id)
                            ->withErrors("You can't publish comics without upload version file. upload it or delete the version");
                    }
                }
            }

            $cModal->draft = 0;
            $cModal->status = 0;
            $nestedVersion = Version::nestable($comicsID);

            $jsonresult = $this->getVersionsJson($nestedVersion, 0, $firstlevel->id);
            if ($jsonresult) {
                $cModal->versions = json_encode($jsonresult);
            }

        }
        $cModal->save();
        if ($request->input('action') == 'draft') {
            return redirect()->away($uModal->draftUrl());
        }
        if ($request->input('action') == 'publish') {
            return redirect()->away($uModal->pendingtUrl());
        }
    }

    private function getVersionsJson($curV, $cnt = 0, $parent = null)
    {
        $t = [];
        if ($curV) {
            $count = 0;
            $i = 0;
            foreach ($curV as $vers) {
                ++$count;
                ++$i;
                if ($cnt > 0) {
                    $count = $cnt.'.'.$i;
                }
                if (isset($vers->id)) {
                   $gr['versions'][$count] = [
                        'button' => [
                            'number' => $count.' Version',
                            'text' => $vers->title,
                        ],
                        'content' => json_decode($vers->name),
                        'path' => $vers->path,
                    ];

                    if (isset($vers->items)) {
                        if ($vers->items->count() > 0) {
                            if ($parent == $vers->parent_id) {
                                 $result = $this->getVersionsJson($vers->items, $count, $vers->id);
                                $gr['versions'][$count] = array_merge($gr['versions'][$count], $result);
                            }
                        }
                    }
                    $t =  $gr;
                }
            }

            return $t;
        }

        return false;
    }

    public function addVersion(VFR $request)
    {
        $user_id = base64_decode($request->keyU);
        $nickname = User::where('id', $user_id)->where('nickname', base64_decode($request->keyVal))->pluck('nickname')->first();
        $type = str_replace('goanimatedgoFuckYourself', '', base64_decode($request->verT));
        if (!$nickname) {
            return false;
        }

        if ($type != 'reading' && $type != 'animated') {
            return false;
        }

        $comics = Comic::where('id', $request->comics_id)->where('user_id', $user_id)->get()->first();
        $data = [
            'user_id' => $user_id,
            'comics_id' => $request->comics_id,
            'parent_id' => $request->id,
            'type' => $type,
        ];

        if ($comics) {
            $result = Version::create($data);
        }

        return $result;
    }

    public function removeVersion(VFR $request)
    {
        $user_id = base64_decode($request->keyU);
        $nickname = User::where('id', $user_id)->where('nickname', base64_decode($request->keyVal))->pluck('nickname')->first();

        if (!$nickname) {
            return false;
        }

        $v = Version::where('id', $request->id)->where('user_id', $user_id)->get()->first();
        if ($v) {
            $curV = Version::find($v->id);
            $array_of_ids = $this->getChildren($curV);
            array_push($array_of_ids, $v->id);
            foreach ($array_of_ids as $id) {
                $vers = Version::find($id);
                $files = json_decode($vers->name);
                if ($files) {
                    foreach ($files as $file) {
                        Storage::delete($vers->path.'/'.$file);
                    }
                }
            }
            Version::destroy($array_of_ids);
        }

        return true;
    }

    public function downloadFile($file)
    {
        return response()->download(storage_path('app/public/comics/pdf/'.$file));
    }

    private function getChildren($curV)
    {
        $ids = [];
        foreach ($curV->children as $vers) {
            $ids[] = $vers->id;
            $ids = array_merge($ids, $this->getChildren($vers));
        }

        return $ids;
    }

    public function getVersionJson($id)
    {
        if ($id) {
            $data = Comic::where('id', $id)->get()->first();

            return $data['versions'];
        }
    }

    private function checkType($type)
    {
        if ($type != 'reading' && $type != 'animated') {
            abort('404');
        }
    }

    private function authCheck()
    {
        $user = auth()->user();
        if (!auth()->check()) {
            abort('404');
        }

        if ($user->isSuperUser()) {
            abort('404');
        }
    }

    private function CheckComics($comicsID)
    {
        $cModal = Comic::find($comicsID);
        if (!$cModal) {
            abort('404');
        }

        return $cModal;
    }
}
