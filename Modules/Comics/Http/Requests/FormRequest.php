<?php

namespace TypiCMS\Modules\Comics\Http\Requests;

use TypiCMS\Modules\Core\Http\Requests\AbstractFormRequest;

class FormRequest extends AbstractFormRequest
{
    public function rules()
    {
        return [
            'image' => 'nullable',
            'pdf' => 'nullable|file',
            'title' => 'nullable|max:255',
            'slug' => 'nullable|alpha_dash|max:255|unique:comics,slug,'.$this->id,
            'status' => 'boolean',
            'body' => 'nullable',
            'co_owners' => 'nullable',
            'lang' => 'required|nullable',
            'type' => 'required|nullable',
            'step' => 'nullable|integer',
            'draft' => 'boolean|integer',
            'versions' => 'nullable',
            'category_id' => 'required',
            'user_id' => 'nullable',
        ];
    }
}
