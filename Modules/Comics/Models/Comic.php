<?php

namespace TypiCMS\Modules\Comics\Models;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;
use Laracasts\Presenter\PresentableTrait;
use TypiCMS\Modules\Comics\Presenters\ModulePresenter;
use TypiCMS\Modules\Core\Models\Base;
use TypiCMS\Modules\Files\Traits\HasFiles;
use TypiCMS\Modules\History\Traits\Historable;

class Comic extends Base
{
    use HasFiles;
    use Historable;
    use PresentableTrait;

    protected $presenter = ModulePresenter::class;

    protected $guarded = [];

    public function uri($locale = null): string
    {
        $locale = $locale ?: config('app.locale');
        $route = $locale.'::'.$this->getTable();
        if (Route::has($route)) {
            if ($this->slug) {
                return route($route, [$this->ComicsType(), $this->category->slug, $this->slug]);
            }
        }

        return url('/');
    }

    public function getTypeUri($locale = null, $type=null)
    {
        $locale = $locale ?: config('app.locale');
        $route = $locale.'::comics-type';
        if (Route::has($route)) {
            return route($route, $type);
        }

        return url('/');
    }

    public function category(): BelongsTo
    {
        return $this->belongsTo(ComicCategory::class);
    }

    public function imagepath(): string
    {
        return Storage::url('comics/'.$this->user_id.'/comics_'.$this->id.'/'.$this->image);
    }

    public function getTypes(): array
    {
        return [
            'reading' => 'Reading',
            'animated' => 'Animated',
        ];
    }

    public function ComicsType(): string
    {
        return $this->type;
    }

    public function getLanguages($locale = null)
    {
        $locale = $locale ?: config('app.locale');
        $lngs = config('typicms.'.$locale.'.comics_languages');
        if ($lngs) {
            $languages = [];
            foreach (explode(',', $lngs) as $val) {
                $languages[mb_strtolower($val)] = $val;
            }

            return $languages;
        }
    }
}
