<?php

namespace TypiCMS\Modules\Comics\Providers;

use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\ServiceProvider;
use TypiCMS\Modules\Core\Facades\TypiCMS;
use TypiCMS\Modules\Core\Observers\SlugObserver;
use TypiCMS\Modules\Comics\Composers\SidebarViewComposer;
use TypiCMS\Modules\Comics\Facades\Comics;
use TypiCMS\Modules\Comics\Models\Comic;
use TypiCMS\Modules\Comics\Models\ComicCategory;
use TypiCMS\Modules\Comics\Facades\ComicCategories;

class ModuleServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->mergeConfigFrom(__DIR__.'/../config/config.php', 'typicms.comics');
        $this->mergeConfigFrom(__DIR__.'/../config/permissions.php', 'typicms.permissions');
        $this->mergeConfigFrom(__DIR__.'/../config/config-comic_categories.php', 'typicms.comic_categories');
        
        $modules = $this->app['config']['typicms']['modules'];
        $this->app['config']->set('typicms.modules', array_merge(['comics' => ['linkable_to_page']], $modules));

        $this->loadViewsFrom(null, 'comics');

        $this->publishes([
            __DIR__.'/../database/migrations/create_comics_table.php.stub' => getMigrationFileName('create_comics_table'),
        ], 'migrations');

        AliasLoader::getInstance()->alias('Comics', Comics::class);
        AliasLoader::getInstance()->alias('ComicCategories', ComicCategories::class);
        // Observers
        // Comic::observe(new SlugObserver());

        /*
         * Sidebar view composer
         */
        $this->app->view->composer('core::admin._sidebar', SidebarViewComposer::class);

        /*
         * Add the page in the view.
         */
        $this->app->view->composer('comics::public.*', function ($view) {
            $view->page = TypiCMS::getPageLinkedToModule('comics');
        });
    }

    public function register()
    {
        $app = $this->app;

        /*
         * Register route service provider
         */
        $app->register(RouteServiceProvider::class);
        $app->bind('ComicCategories', ComicCategory::class);
        $app->bind('Comics', Comic::class);
    }
}
