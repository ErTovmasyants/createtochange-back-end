<?php

namespace TypiCMS\Modules\Comics\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Route;
use TypiCMS\Modules\Core\Facades\TypiCMS;
use TypiCMS\Modules\Comics\Http\Controllers\AdminController;
use TypiCMS\Modules\Comics\Http\Controllers\ApiController;
use TypiCMS\Modules\Comics\Http\Controllers\CategoriesAdminController;
use TypiCMS\Modules\Comics\Http\Controllers\CategoriesApiController;
use TypiCMS\Modules\Comics\Http\Controllers\PublicController;

class RouteServiceProvider extends ServiceProvider
{
    public function map()
    {
        /*
         * Front office routes
         */
        if ($page = TypiCMS::getPageLinkedToModule('comics')) {
            $middleware = $page->private ? ['public', 'auth'] : ['public'];
            foreach (locales() as $lang) {
                if ($page->isPublished($lang) && $uri = $page->uri($lang)) {
                    Route::middleware($middleware)->prefix($uri)->name($lang.'::')->group(function (Router $router) {
                        $router->get('/', [PublicController::class, 'index'])->name('index-comics');
                        $router->get('create/{step}/{id?}/{type?}/{vID?}', [PublicController::class, 'create'])->name('comics-create');
                        $router->get('{type}', [PublicController::class, 'indexOfType'])->name('comics-type');
                        $router->get('{type}/{category}', [PublicController::class, 'indexOfCategory'])->name('comics-category');
                        $router->get('{type}/{category}/{slug}', [PublicController::class, 'show'])->name('comics');
                    });
                }
            }
            Route::post('save', [PublicController::class, 'save'])->name('comics-save');
            Route::post('vsave', [PublicController::class, 'Vsave'])->name('versioncontent-save');
            Route::post('sV', [PublicController::class, 'addVersion'])->name('version-save');
            Route::post('sVF', [PublicController::class, 'addVersionFinish'])->name('versionfinish-save');
            Route::get('gVj/{id}', [PublicController::class, 'getVersionJson'])->name('versionjson-get');
            Route::get('download/{file_name}', [PublicController::class, 'downloadFile'])->name('downloadFile');
            Route::delete('rV/{id?}', [PublicController::class, 'removeVersion'])->name('version-remove');
        }
    
       
        /*
         * Admin routes
         */
        Route::middleware('admin')->prefix('admin')->name('admin::')->group(function (Router $router) {
            $router->get('comics/status/{status}', [AdminController::class, 'indexStatus'])->middleware('can:read comics');
            $router->get('comics', [AdminController::class, 'index'])->name('index-comics')->middleware('can:read comics');
            $router->get('comics/pending/{comic}', [AdminController::class, 'pendingview'])->name('pending-view-comics')->middleware('can:read comics');
            $router->get('comics/export', [AdminController::class, 'export'])->name('export-comics')->middleware('can:read comics');
            $router->get('comics/create', [AdminController::class, 'create'])->name('create-comic')->middleware('can:create comics');
            $router->get('comics/{comic}/edit', [AdminController::class, 'edit'])->name('edit-comic')->middleware('can:read comics');
            $router->post('comics', [AdminController::class, 'store'])->name('store-comic')->middleware('can:create comics');
            $router->put('comics/{comic}', [AdminController::class, 'update'])->name('update-comic')->middleware('can:update comics');

            $router->get('comics/categories', [CategoriesAdminController::class, 'index'])->name('index-comic_categories')->middleware('can:read comic_categories');
            $router->get('comics/categories/create', [CategoriesAdminController::class, 'create'])->name('create-comic_category')->middleware('can:create comic_categories');
            $router->get('comics/categories/{category}/edit', [CategoriesAdminController::class, 'edit'])->name('edit-comic_category')->middleware('can:read comic_categories');
            $router->post('comics/categories', [CategoriesAdminController::class, 'store'])->name('store-comic_category')->middleware('can:create comic_categories');
            $router->put('comics/categories/{category}', [CategoriesAdminController::class, 'update'])->name('update-comic_category')->middleware('can:update comic_categories');
        });

        /*
         * API routes
         */
        Route::middleware(['api', 'auth:api'])->prefix('api')->group(function (Router $router) {
            $router->get('comics/{status}/status', [ApiController::class, 'indexByStatus'])->middleware('can:read comics');
            $router->get('comics', [ApiController::class, 'index'])->middleware('can:read comics');
            $router->patch('comics/{comic}', [ApiController::class, 'updatePartial'])->middleware('can:update comics');
            $router->delete('comics/{comic}', [ApiController::class, 'destroy'])->middleware('can:delete comics');

            $router->get('comics/categories', [CategoriesApiController::class, 'index'])->middleware('can:read comic_categories');
            $router->patch('comics/categories/{category}', [CategoriesApiController::class, 'updatePartial'])->middleware('can:update comic_categories');
            $router->post('comics/categories/sort', [CategoriesApiController::class, 'sort'])->middleware('can:update comic_categories');
            $router->delete('comics/categories/{category}', [CategoriesApiController::class, 'destroy'])->middleware('can:delete comic_categories');
        });
    }
}
