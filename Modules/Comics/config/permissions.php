<?php

return [
    'comics' => [
        'read comics' => 'Read',
        'create comics' => 'Create',
        'update comics' => 'Update',
        'delete comics' => 'Delete',
    ],
];
