<?php

namespace TypiCMS\Modules\Faqs\Composers;

use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Gate;
use Maatwebsite\Sidebar\SidebarGroup;
use Maatwebsite\Sidebar\SidebarItem;

class SidebarViewComposer
{
    public function compose(View $view)
    {
        if (Gate::denies('read faqs')) {
            return;
        }
        $view->sidebar->group(__('Content'), function (SidebarGroup $group) {
            $group->id = 'content';
            $group->weight = 30;
            $group->addItem(__('Faqs'), function (SidebarItem $item) {
                $item->id = 'faqs';
                $item->icon = config('typicms.faqs.sidebar.icon');
                $item->weight = config('typicms.faqs.sidebar.weight');
                $item->route('admin::index-faqs');
                $item->append('admin::create-faq');
            });
        });
    }
}
