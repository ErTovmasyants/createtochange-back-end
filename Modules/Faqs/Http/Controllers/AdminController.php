<?php

namespace TypiCMS\Modules\Faqs\Http\Controllers;

use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Maatwebsite\Excel\Facades\Excel;
use TypiCMS\Modules\Core\Http\Controllers\BaseAdminController;
use TypiCMS\Modules\Faqs\Exports\Export;
use TypiCMS\Modules\Faqs\Http\Requests\FormRequest;
use TypiCMS\Modules\Faqs\Models\Faq;

class AdminController extends BaseAdminController
{
    public function index(): View
    {
        return view('faqs::admin.index');
    }

    public function export(Request $request)
    {
        $filename = date('Y-m-d').' '.config('app.name').' faqs.xlsx';

        return Excel::download(new Export($request), $filename);
    }

    public function create(): View
    {
        $model = new Faq();

        return view('faqs::admin.create')
            ->with(compact('model'));
    }

    public function edit(Faq $faq): View
    {
        return view('faqs::admin.edit')
            ->with(['model' => $faq]);
    }

    public function store(FormRequest $request): RedirectResponse
    {
        $faq = Faq::create($request->validated());

        return $this->redirect($request, $faq);
    }

    public function update(Faq $faq, FormRequest $request): RedirectResponse
    {
        $faq->update($request->validated());

        return $this->redirect($request, $faq);
    }
}
