<?php

namespace TypiCMS\Modules\Faqs\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;
use TypiCMS\Modules\Core\Filters\FilterOr;
use TypiCMS\Modules\Core\Http\Controllers\BaseApiController;
use TypiCMS\Modules\Faqs\Models\Faq;

class ApiController extends BaseApiController
{
    public function index(Request $request): LengthAwarePaginator
    {
        $data = QueryBuilder::for(Faq::class)
            ->selectFields($request->input('fields.faqs'))
            ->allowedSorts(['status_translated', 'title_translated','position'])
            ->allowedFilters([
                AllowedFilter::custom('title', new FilterOr()),
            ])
            ->allowedIncludes(['image'])
            ->paginate($request->input('per_page'));

        return $data;
    }

    protected function updatePartial(Faq $faq, Request $request)
    {
        foreach ($request->only('status', 'position') as $key => $content) {
            if ($faq->isTranslatableAttribute($key)) {
                foreach ($content as $lang => $value) {
                    $faq->setTranslation($key, $lang, $value);
                }
            } else {
                $faq->{$key} = $content;
            }
        }

        $faq->save();
    }

    public function destroy(Faq $faq)
    {
        $faq->delete();
    }
}
