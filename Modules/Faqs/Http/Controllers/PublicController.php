<?php

namespace TypiCMS\Modules\Faqs\Http\Controllers;

use Illuminate\View\View;
use TypiCMS\Modules\Core\Http\Controllers\BasePublicController;
use TypiCMS\Modules\Faqs\Models\Faq;

class PublicController extends BasePublicController
{
    public function index(): View
    {
        $models = Faq::published()->orderBy('position')->with('image')->get();

        return view('faqs::public.index')
            ->with(compact('models'));
    }

    public function show($slug): View
    {
        $model = Faq::published()->whereSlugIs($slug)->firstOrFail();

        return view('faqs::public.show')
            ->with(compact('model'));
    }
}
