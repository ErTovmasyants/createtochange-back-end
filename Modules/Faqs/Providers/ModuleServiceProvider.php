<?php

namespace TypiCMS\Modules\Faqs\Providers;

use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\ServiceProvider;
use TypiCMS\Modules\Core\Facades\TypiCMS;
use TypiCMS\Modules\Core\Observers\SlugObserver;
use TypiCMS\Modules\Faqs\Composers\SidebarViewComposer;
use TypiCMS\Modules\Faqs\Facades\Faqs;
use TypiCMS\Modules\Faqs\Models\Faq;

class ModuleServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->mergeConfigFrom(__DIR__.'/../config/config.php', 'typicms.faqs');
        $this->mergeConfigFrom(__DIR__.'/../config/permissions.php', 'typicms.permissions');

        $modules = $this->app['config']['typicms']['modules'];
        $this->app['config']->set('typicms.modules', array_merge(['faqs' => ['linkable_to_page']], $modules));

        $this->loadViewsFrom(null, 'faqs');

        $this->publishes([
            __DIR__.'/../database/migrations/create_faqs_table.php.stub' => getMigrationFileName('create_faqs_table'),
        ], 'migrations');

        AliasLoader::getInstance()->alias('Faqs', Faqs::class);

        // Observers
        Faq::observe(new SlugObserver());

        /*
         * Sidebar view composer
         */
        $this->app->view->composer('core::admin._sidebar', SidebarViewComposer::class);

        /*
         * Add the page in the view.
         */
        $this->app->view->composer('faqs::public.*', function ($view) {
            $view->page = TypiCMS::getPageLinkedToModule('faqs');
        });
    }

    public function register()
    {
        $app = $this->app;

        /*
         * Register route service provider
         */
        $app->register(RouteServiceProvider::class);

        $app->bind('Faqs', Faq::class);
    }
}
