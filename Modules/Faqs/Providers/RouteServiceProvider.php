<?php

namespace TypiCMS\Modules\Faqs\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Route;
use TypiCMS\Modules\Core\Facades\TypiCMS;
use TypiCMS\Modules\Faqs\Http\Controllers\AdminController;
use TypiCMS\Modules\Faqs\Http\Controllers\ApiController;
use TypiCMS\Modules\Faqs\Http\Controllers\PublicController;

class RouteServiceProvider extends ServiceProvider
{
    public function map()
    {
        /*
         * Front office routes
         */
        if ($page = TypiCMS::getPageLinkedToModule('faqs')) {
            $middleware = $page->private ? ['public', 'auth'] : ['public'];
            foreach (locales() as $lang) {
                if ($page->isPublished($lang) && $uri = $page->uri($lang)) {
                    Route::middleware($middleware)->prefix($uri)->name($lang.'::')->group(function (Router $router) {
                        $router->get('/', [PublicController::class, 'index'])->name('index-faqs');
                        $router->get('{slug}', [PublicController::class, 'show'])->name('faq');
                    });
                }
            }
        }

        /*
         * Admin routes
         */
        Route::middleware('admin')->prefix('admin')->name('admin::')->group(function (Router $router) {
            $router->get('faqs', [AdminController::class, 'index'])->name('index-faqs')->middleware('can:read faqs');
            $router->get('faqs/export', [AdminController::class, 'export'])->name('export-faqs')->middleware('can:read faqs');
            $router->get('faqs/create', [AdminController::class, 'create'])->name('create-faq')->middleware('can:create faqs');
            $router->get('faqs/{faq}/edit', [AdminController::class, 'edit'])->name('edit-faq')->middleware('can:read faqs');
            $router->post('faqs', [AdminController::class, 'store'])->name('store-faq')->middleware('can:create faqs');
            $router->put('faqs/{faq}', [AdminController::class, 'update'])->name('update-faq')->middleware('can:update faqs');
        });

        /*
         * API routes
         */
        Route::middleware(['api', 'auth:api'])->prefix('api')->group(function (Router $router) {
            $router->get('faqs', [ApiController::class, 'index'])->middleware('can:read faqs');
            $router->patch('faqs/{faq}', [ApiController::class, 'updatePartial'])->middleware('can:update faqs');
            $router->delete('faqs/{faq}', [ApiController::class, 'destroy'])->middleware('can:delete faqs');
        });
    }
}
