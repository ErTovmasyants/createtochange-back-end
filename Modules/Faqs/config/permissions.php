<?php

return [
    'faqs' => [
        'read faqs' => 'Read',
        'create faqs' => 'Create',
        'update faqs' => 'Update',
        'delete faqs' => 'Delete',
    ],
];
