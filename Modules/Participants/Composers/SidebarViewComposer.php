<?php

namespace TypiCMS\Modules\Participants\Composers;

use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Gate;
use Maatwebsite\Sidebar\SidebarGroup;
use Maatwebsite\Sidebar\SidebarItem;

class SidebarViewComposer
{
    public function compose(View $view)
    {
        if (Gate::denies('read participants')) {
            return;
        }
        $view->sidebar->group(__('Content'), function (SidebarGroup $group) {
            $group->id = 'content';
            $group->weight = 30;
            $group->addItem(__('Participants'), function (SidebarItem $item) {
                $item->id = 'participants';
                $item->icon = config('typicms.participants.sidebar.icon');
                $item->weight = config('typicms.participants.sidebar.weight');
                $item->route('admin::index-participants');
                $item->append('admin::create-participant');
            });
        });
    }
}
