<?php

namespace TypiCMS\Modules\Participants\Facades;

use Illuminate\Support\Facades\Facade;

class Participants extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'Participants';
    }
}
