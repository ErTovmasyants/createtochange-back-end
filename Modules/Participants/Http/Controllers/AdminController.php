<?php

namespace TypiCMS\Modules\Participants\Http\Controllers;

use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Maatwebsite\Excel\Facades\Excel;
use TypiCMS\Modules\Core\Http\Controllers\BaseAdminController;
use TypiCMS\Modules\Participants\Exports\Export;
use TypiCMS\Modules\Participants\Http\Requests\FormRequest;
use TypiCMS\Modules\Participants\Models\Participant;

class AdminController extends BaseAdminController
{
    public function index(): View
    {
        return view('participants::admin.index');
    }

    public function export(Request $request)
    {
        $filename = date('Y-m-d').' '.config('app.name').' participants.xlsx';

        return Excel::download(new Export($request), $filename);
    }

    public function create(): View
    {
        $model = new Participant();

        return view('participants::admin.create')
            ->with(compact('model'));
    }

    public function edit(participant $participant): View
    {
        return view('participants::admin.edit')
            ->with(['model' => $participant]);
    }

    public function store(FormRequest $request): RedirectResponse
    {
        $participant = Participant::create($request->validated());

        return $this->redirect($request, $participant);
    }

    public function update(participant $participant, FormRequest $request): RedirectResponse
    {
        $participant->update($request->validated());

        return $this->redirect($request, $participant);
    }
}
