<?php

namespace TypiCMS\Modules\Participants\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;
use TypiCMS\Modules\Core\Filters\FilterOr;
use TypiCMS\Modules\Core\Http\Controllers\BaseApiController;
use TypiCMS\Modules\Participants\Models\Participant;

class ApiController extends BaseApiController
{
    public function index(Request $request): LengthAwarePaginator
    {
        $data = QueryBuilder::for(Participant::class)
            ->selectFields($request->input('fields.participants'))
            ->allowedSorts(['status_translated', 'title_translated'])
            ->allowedFilters([
                AllowedFilter::custom('title', new FilterOr()),
            ])
            ->allowedIncludes(['image'])
            ->paginate($request->input('per_page'));

        return $data;
    }

    protected function updatePartial(Participant $participant, Request $request)
    {
        foreach ($request->only('status') as $key => $content) {
            if ($participant->isTranslatableAttribute($key)) {
                foreach ($content as $lang => $value) {
                    $participant->setTranslation($key, $lang, $value);
                }
            } else {
                $participant->{$key} = $content;
            }
        }

        $participant->save();
    }

    public function destroy(Participant $participant)
    {
        $participant->delete();
    }
}
