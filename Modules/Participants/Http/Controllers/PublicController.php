<?php

namespace TypiCMS\Modules\Participants\Http\Controllers;

use Illuminate\View\View;
use TypiCMS\Modules\Core\Http\Controllers\BasePublicController;
use TypiCMS\Modules\Participants\Models\Participant;

class PublicController extends BasePublicController
{
    public function index(): View
    {
        $models = Participant::published()->order()->with('image')->get();

        return view('participants::public.index')
            ->with(compact('models'));
    }

    public function show($slug): View
    {
        $model = Participant::published()->whereSlugIs($slug)->firstOrFail();

        return view('participants::public.show')
            ->with(compact('model'));
    }
}
