<?php

namespace TypiCMS\Modules\Participants\Providers;

use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\ServiceProvider;
use TypiCMS\Modules\Core\Facades\TypiCMS;
use TypiCMS\Modules\Core\Observers\SlugObserver;
use TypiCMS\Modules\Participants\Composers\SidebarViewComposer;
use TypiCMS\Modules\Participants\Facades\Participants;
use TypiCMS\Modules\Participants\Models\Participant;

class ModuleServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->mergeConfigFrom(__DIR__.'/../config/config.php', 'typicms.participants');
        $this->mergeConfigFrom(__DIR__.'/../config/permissions.php', 'typicms.permissions');

        $modules = $this->app['config']['typicms']['modules'];
        $this->app['config']->set('typicms.modules', array_merge(['participants' => ['linkable_to_page']], $modules));

        $this->loadViewsFrom(null, 'participants');

        $this->publishes([
            __DIR__.'/../database/migrations/create_participants_table.php.stub' => getMigrationFileName('create_participants_table'),
        ], 'migrations');

        AliasLoader::getInstance()->alias('Participants', Participants::class);

        // Observers
        // Participant::observe(new SlugObserver());

        /*
         * Sidebar view composer
         */
        $this->app->view->composer('core::admin._sidebar', SidebarViewComposer::class);

        /*
         * Add the page in the view.
         */
        $this->app->view->composer('participants::public.*', function ($view) {
            $view->page = TypiCMS::getPageLinkedToModule('participants');
        });
    }

    public function register()
    {
        $app = $this->app;

        /*
         * Register route service provider
         */
        $app->register(RouteServiceProvider::class);

        $app->bind('Participants', Participant::class);
    }
}
