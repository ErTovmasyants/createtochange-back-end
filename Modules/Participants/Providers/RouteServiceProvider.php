<?php

namespace TypiCMS\Modules\Participants\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Route;
use TypiCMS\Modules\Core\Facades\TypiCMS;
use TypiCMS\Modules\Participants\Http\Controllers\AdminController;
use TypiCMS\Modules\Participants\Http\Controllers\ApiController;
use TypiCMS\Modules\Participants\Http\Controllers\PublicController;

class RouteServiceProvider extends ServiceProvider
{
    public function map()
    {
        /*
         * Front office routes
         */
        if ($page = TypiCMS::getPageLinkedToModule('participants')) {
            $middleware = $page->private ? ['public', 'auth'] : ['public'];
            foreach (locales() as $lang) {
                if ($page->isPublished($lang) && $uri = $page->uri($lang)) {
                    Route::middleware($middleware)->prefix($uri)->name($lang.'::')->group(function (Router $router) {
                        $router->get('/', [PublicController::class, 'index'])->name('index-participants');
                        $router->get('{slug}', [PublicController::class, 'show'])->name('participant');
                    });
                }
            }
        }

        /*
         * Admin routes
         */
        Route::middleware('admin')->prefix('admin')->name('admin::')->group(function (Router $router) {
            $router->get('participants', [AdminController::class, 'index'])->name('index-participants')->middleware('can:read participants');
            $router->get('participants/export', [AdminController::class, 'export'])->name('export-participants')->middleware('can:read participants');
            $router->get('participants/create', [AdminController::class, 'create'])->name('create-participant')->middleware('can:create participants');
            $router->get('participants/{participant}/edit', [AdminController::class, 'edit'])->name('edit-participant')->middleware('can:read participants');
            $router->post('participants', [AdminController::class, 'store'])->name('store-participant')->middleware('can:create participants');
            $router->put('participants/{participant}', [AdminController::class, 'update'])->name('update-participant')->middleware('can:update participants');
        });

        /*
         * API routes
         */
        Route::middleware(['api', 'auth:api'])->prefix('api')->group(function (Router $router) {
            $router->get('participants', [ApiController::class, 'index'])->middleware('can:read participants');
            $router->patch('participants/{participant}', [ApiController::class, 'updatePartial'])->middleware('can:update participants');
            $router->delete('participants/{participant}', [ApiController::class, 'destroy'])->middleware('can:delete participants');
        });
    }
}
