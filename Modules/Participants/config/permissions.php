<?php

return [
    'participants' => [
        'read participants' => 'Read',
        'create participants' => 'Create',
        'update participants' => 'Update',
        'delete participants' => 'Delete',
    ],
];
