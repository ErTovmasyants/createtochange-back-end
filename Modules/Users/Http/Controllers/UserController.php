<?php

namespace TypiCMS\Modules\Users\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use TypiCMS\Modules\Comics\Models\Comic;
use TypiCMS\Modules\Users\Http\Requests\UserFormRequest;
use TypiCMS\Modules\Users\Models\User;

class UserController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    // protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        // $this->middleware('guest', ['except' => 'logout']);
        $this->middleware('auth')->only([
            'edit',
            'update',
            'showDraft',
        ]);
    }

    public function edit($nickname)
    {
        if (auth()->user()->isSuperUser()) {
            return redirect('/');
        }

        if($nickname !== auth()->user()->nickname){
            return redirect('/');
        }
        
        $countries = DB::table('country')->select('iso', 'nicename', 'id')->get();
        return view('users::edit')->with(compact('countries'));
    }

    /**
     * Show the application's login form.
     *
     * @param mixed      $nickname
     * @param null|mixed $type
     *
     * @return \Illuminate\Http\Response
     */
    public function showAccount($nickname, $type = null)
    {
        $us = new User();
        $icons = [];
        $accountData = User::select('id', 'image', 'country', 'city', 'phone', 'first_name', 'last_name', 'email', 'bio', 'socials')->where('nickname', $nickname)->first();

        if ($accountData) {
            if ($accountData->country) {
                $country = DB::table('country')->select('nicename')->where('iso', $accountData->country)->first();
                if ($country) {
                    $accountData->country = $country->nicename;
                }
            }

            if ($accountData->city) {
                $city = DB::table('cities')->select('city')->where('id', $accountData->city)->first();
                if ($city) {
                    $accountData->city = $city->city;
                }
            }

            if ($accountData->socials) {
                $arr = json_decode($accountData->socials);
                foreach ($arr as $item) {
                    $icons[$us->validUrl($item)][] = $item;
                }
            }
            if ($type == null) {
                $type = 'reading';
            }else{
                if($type != 'reading' && $type != 'animated'){
                    return redirect('/');
                }
            }

            $comicses = Comic::order()
                ->where('status', 1)
                ->where('draft', 0)
                ->where('user_id', $accountData->id)
                ->where('type', $type)->get();

            $draft = Comic::order()
                ->where('status', 0)
                ->where('user_id', $accountData->id)
                ->where('draft', 1)->get();
            $draftCount = $draft->count();

            $comicsespending = Comic::order()
            ->where('status', 0)
            ->where('user_id', $accountData->id)
            ->where('draft', 0)
            ->get();

            $pendingCount = $comicsespending->count();
            $draft = 0;
            $pending = 0;
            $pageTitle = __('My Comicses');
            return view('users::account')->with(compact('pageTitle','draft','pending','nickname', 'accountData', 'icons', 'comicses', 'type', 'draftCount','pendingCount'));
        }

        return redirect('/');
    }

    public function showDraft($nickname)
    {
        $us = new User();
        $icons = [];
        $accountData = User::select('id', 'image', 'country', 'city', 'phone', 'first_name', 'last_name', 'email', 'bio', 'socials')->where('nickname', $nickname)->first();

        if ($accountData) {
            if ($accountData->country) {
                $country = DB::table('country')->select('nicename')->where('iso', $accountData->country)->first();
                if ($country) {
                    $accountData->country = $country->nicename;
                }
            }

            if ($accountData->city) {
                $city = DB::table('cities')->select('city')->where('id', $accountData->city)->first();
                if ($city) {
                    $accountData->city = $city->city;
                }
            }

            if ($accountData->socials) {
                $arr = json_decode($accountData->socials);
                foreach ($arr as $item) {
                    $icons[$us->validUrl($item)][] = $item;
                }
            }

            $comicses = Comic::order()
                ->where('status', 0)
                ->where('user_id', $accountData->id)
                ->where('draft', 1)
                ->get();
            $comicsespending = Comic::order()
                ->where('status', 0)
                ->where('user_id', $accountData->id)
                ->where('draft', 0)
                ->get();

            $pendingCount = $comicsespending->count();
            $draftCount = $comicses->count();
            $draft = 1;
            $pending = 0;
            $pageTitle = __('Draft');
            return view('users::account')->with(compact('pageTitle','draft','pending', 'nickname', 'accountData', 'icons', 'comicses', 'draftCount','pendingCount'));
        }

        return redirect('/');
    }
    public function showPending($nickname)
    {
        $us = new User();
        $icons = [];
        $accountData = User::select('id', 'image', 'country', 'city', 'phone', 'first_name', 'last_name', 'email', 'bio', 'socials')->where('nickname', $nickname)->first();

        if ($accountData) {
            if ($accountData->country) {
                $country = DB::table('country')->select('nicename')->where('iso', $accountData->country)->first();
                if ($country) {
                    $accountData->country = $country->nicename;
                }
            }

            if ($accountData->city) {
                $city = DB::table('cities')->select('city')->where('id', $accountData->city)->first();
                if ($city) {
                    $accountData->city = $city->city;
                }
            }

            if ($accountData->socials) {
                $arr = json_decode($accountData->socials);
                foreach ($arr as $item) {
                    $icons[$us->validUrl($item)][] = $item;
                }
            }

            $comicses = Comic::order()
                ->where('status', 0)
                ->where('user_id', $accountData->id)
                ->where('draft', 0)
                ->get();

                $draft = Comic::order()
                ->where('status', 0)
                ->where('user_id', $accountData->id)
                ->where('draft', 1)->get();
            $draftCount = $draft->count();

            $pendingCount = $comicses->count();
            $pending = 1;
            $draft = 0;
            $pageTitle = __('Pending');
            return view('users::account')->with(compact('pageTitle','draft','pending', 'nickname', 'accountData', 'icons', 'comicses','draftCount', 'pendingCount'));
        }

        return redirect('/');
    }
    public function update(User $user, UserFormRequest $request)
    {
        $data = $request->validated();
        $links = [];
        if (isset($data['image'])) {
            $path = public_path('avatar/'.$user->id);
            $name = $request->file('image')->getClientOriginalName();
            $imageName = time().$name;
            if (!File::exists($path)) {
                Storage::makeDirectory($path, 0755, true, true);
            }
            $request->image->move($path, $imageName);
            $data['image'] = $imageName;
        }
        if ($request->filled('socials')) {
          
            foreach ($request->input('socials') as $link) {
               
                if(!is_null($link)){
                    if (filter_var($link, FILTER_VALIDATE_URL) == false) {
                        return redirect()
                            ->back()
                            ->withErrors(__($link.' not valid'));
                    }
                   $links[] = $link;
                } 
            }

            $data['socials'] = json_encode($links);
        } else {
            $data['socials'] = json_encode([]);
        }

        if ($request->filled('new_password')) {
            if ($request->filled('current_password')) {
                $hashedPassword = auth()->user()->password;
                if (!Hash::check($request->input('current_password'), $hashedPassword)) {
                    return redirect()
                        ->back()
                        ->withErrors('The Current password doesnt matched');
                }
                if (Hash::check($request->new_password, $hashedPassword)) {
                    return redirect()
                        ->back()
                        ->withErrors('New password can not be the old password!');
                }
            } else {
                return redirect()
                    ->back()
                    ->withErrors('The Current password required');
            }
            if ($request->filled('password_confirmation')) {
                if ($request->input('password_confirmation') == $request->input('new_password')) {
                    $data['password'] = Hash::make($request->input('new_password'));
                }
            } else {
                return redirect()
                    ->back()
                    ->withErrors('Password Confirmation is required');
            }
        }

        if ($request->filled('email')) {
            unset($data['email']);
        }

        $user->update($data);

        return redirect()
            ->route(config('app.locale').'::account-user',['nickname'=>auth()->user()->nickname]);
    }

    public function removeComics($id)
    {
        if ($id) {
            $comics = Comic::find($id);
            Storage::delete('comics/'.$comics->user_id.'/comics_'.$comics->id.'/'.$comics->image);
            $comics->delete();
            return true;
        }

        return false;
    }

    public function checknick($nickname){
        $result = User::where('nickname',$nickname)->get()->first();
        return $result;
    }
    public function checkemail($email){
        $result = User::where('email',$email)->get()->first();
        return $result;
    }
}
