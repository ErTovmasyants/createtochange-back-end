<?php

namespace TypiCMS\Modules\Users\Http\Requests;

use TypiCMS\Modules\Core\Http\Requests\AbstractFormRequest;

class FormRequestRegister extends AbstractFormRequest
{
    public function rules()
    {
        return [
            'email' => 'required|email:rfc,dns|max:255|unique:users,email,'.$this->id,
            'nickname' => 'required|min:4|max:60|unique:users',
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'number' => 'nullable|max:60',
            'country' => 'nullable',
            'city' => 'nullable',
            'phone' => 'nullable',
            'bio' => 'nullable',
            'password' => 'required|min:8|max:255|confirmed',
        ];
    }
}
