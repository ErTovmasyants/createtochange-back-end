<?php

namespace TypiCMS\Modules\Users\Http\Requests;
use App\Rules\MatchOldPassword;
use TypiCMS\Modules\Core\Http\Requests\AbstractFormRequest;

class PasswordRequest extends AbstractFormRequest
{
    public function rules()
    {
        return [
            // 'current_password' => ['required', new MatchOldPassword],
            'new_password' => ['required'],
            'new_confirm_password' => ['same:new_password'],
        ];
    }
}
