<?php

namespace TypiCMS\Modules\Users\Http\Requests;

use TypiCMS\Modules\Core\Http\Requests\AbstractFormRequest;

class UserFormRequest extends AbstractFormRequest
{
    public function rules()
    {
        return [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'city' => 'nullable|max:255',
            'image' => 'nullable|image|mimes:jpeg,png,jpg,svg|max:2048',
            'country' => 'nullable|max:255',
            'phone' => 'nullable|max:100',
            'socials' => 'nullable|max:100',
            'bio' => 'nullable',
            'nickname' => 'required|min:4|max:100',
        ];
    }
}
