<?php

namespace TypiCMS\Modules\Users\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\MustVerifyEmail;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Contracts\Auth\MustVerifyEmail as MustVerifyEmailContract;
use Illuminate\Contracts\Translation\HasLocalePreference;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;
use Laracasts\Presenter\PresentableTrait;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Traits\HasRoles;
use TypiCMS\Modules\History\Traits\Historable;
use TypiCMS\Modules\Users\Notifications\ResetPassword;
use TypiCMS\Modules\Users\Notifications\VerifyEmail;
use TypiCMS\Modules\Users\Presenters\ModulePresenter;

class User extends Model implements AuthenticatableContract, AuthorizableContract, CanResetPasswordContract, MustVerifyEmailContract, HasLocalePreference
{
    use Authenticatable;
    use Authorizable;
    use CanResetPassword;
    use HasRoles;
    use Historable;
    use MustVerifyEmail;
    use Notifiable;
    use PresentableTrait;

    protected $presenter = ModulePresenter::class;

    protected $guarded = [];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $casts = [
        'preferences' => 'array',
    ];

    public function preferredLocale()
    {
        return $this->locale;
    }

    public function uri($locale = null)
    {
        return '/';
    }

    public function editUrl(): string
    {
        $route = 'admin::edit-'.Str::singular($this->getTable());
        if (Route::has($route)) {
            return route($route, $this->id);
        }

        return route('admin::dashboard');
    }

    public function accountEditUrl($locale = null): string
    {
        $locale = $locale ?: config('app.locale');
        $route = $locale.'::edit-account-user';
        if (Route::has($route)) {
            return route($route, auth()->user()->nickname);
        }

        return route('/');
    }

    public function indexUrl(): string
    {
        $route = 'admin::index-'.$this->getTable();
        if (Route::has($route)) {
            return route($route);
        }

        return route('admin::dashboard');
    }

    public function draftUrl($locale = null): string
    {
        $locale = $locale ?: config('app.locale');
        $route =  $locale.'::account-user-draft';
        if (Route::has($route)) {
            return route($route,$this->nickname);
        }

        return route('admin::dashboard');
    }

    public function pendingtUrl($locale = null): string
    {
        $locale = $locale ?: config('app.locale');
        $route =  $locale.'::account-user-pending';
        if (Route::has($route)) {
            return route($route,$this->nickname);
        }

        return route('admin::dashboard');
    }

    public static function boot(): void
    {
        parent::boot();
        static::creating(function ($user) {
            $user->api_token = Str::uuid();
        });
    }

    public function isSuperUser(): bool
    {
        return (bool) $this->superuser;
    }

    public function getAllPermissionsAttribute(): array
    {
        $user = auth()->user();
        if ($user->isSuperUser()) {
            return ['all'];
        }
        $permissions = [];
        foreach (Permission::all() as $permission) {
            if ($user->can($permission->name)) {
                $permissions[] = $permission->name;
            }
        }

        return $permissions;
    }

    public function sendPasswordResetNotification($token): void
    {
        $this->notify(new ResetPassword($token));
    }

    public function sendEmailVerificationNotification(): void
    {
        $this->notify(new VerifyEmail());
    }

    public function validUrl($url)
    {
        $parse = parse_url($url);
        if (isset($parse['host'])) {
            $strname = preg_replace('#^www\.(.+\.)#i', '$1', $parse['host']);
            $strname = explode('.', $strname);
            $names = [
                'twitter',
                'facebook',
                'linkedin',
                'instagram',
                'youtube',
                'behance',
            ];
            if (in_array($strname[0], $names)) {
                return $strname[0];
            }else{
                return '';
            }
        }
        return '';
    }
}
