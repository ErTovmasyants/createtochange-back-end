<?php

namespace TypiCMS\Modules\Versions\Composers;

use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Gate;
use Maatwebsite\Sidebar\SidebarGroup;
use Maatwebsite\Sidebar\SidebarItem;

class SidebarViewComposer
{
    public function compose(View $view)
    {
        // if (Gate::denies('read versions')) {
        //     return;
        // }
        // $view->sidebar->group(__('Content'), function (SidebarGroup $group) {
        //     $group->id = 'content';
        //     $group->weight = 30;
        //     $group->addItem(__('Versions'), function (SidebarItem $item) {
        //         $item->id = 'versions';
        //         $item->icon = config('typicms.versions.sidebar.icon');
        //         $item->weight = config('typicms.versions.sidebar.weight');
        //         $item->route('admin::index-versions');
        //         $item->append('admin::create-version');
        //     });
        // });
    }
}
