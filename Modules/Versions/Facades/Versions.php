<?php

namespace TypiCMS\Modules\Versions\Facades;

use Illuminate\Support\Facades\Facade;

class Versions extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'Versions';
    }
}
