<?php

namespace TypiCMS\Modules\Versions\Http\Controllers;

use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Maatwebsite\Excel\Facades\Excel;
use TypiCMS\Modules\Core\Http\Controllers\BaseAdminController;
use TypiCMS\Modules\Versions\Exports\Export;
use TypiCMS\Modules\Versions\Http\Requests\FormRequest;
use TypiCMS\Modules\Versions\Models\Version;

class AdminController extends BaseAdminController
{
    public function index(): View
    {
        return view('versions::admin.index');
    }

    public function export(Request $request)
    {
        $filename = date('Y-m-d').' '.config('app.name').' versions.xlsx';

        return Excel::download(new Export($request), $filename);
    }

    public function create(): View
    {
        $model = new Version();

        return view('versions::admin.create')
            ->with(compact('model'));
    }

    public function edit(version $version): View
    {
        return view('versions::admin.edit')
            ->with(['model' => $version]);
    }

    public function store(FormRequest $request): RedirectResponse
    {
        $version = Version::create($request->validated());

        return $this->redirect($request, $version);
    }

    public function update(version $version, FormRequest $request): RedirectResponse
    {
        $version->update($request->validated());

        return $this->redirect($request, $version);
    }
}
