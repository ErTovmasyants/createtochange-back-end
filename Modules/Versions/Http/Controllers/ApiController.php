<?php

namespace TypiCMS\Modules\Versions\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;
use TypiCMS\Modules\Core\Filters\FilterOr;
use TypiCMS\Modules\Core\Http\Controllers\BaseApiController;
use TypiCMS\Modules\Versions\Models\Version;

class ApiController extends BaseApiController
{
    public function index(Request $request): LengthAwarePaginator
    {
        $data = QueryBuilder::for(Version::class)
            ->selectFields($request->input('fields.versions'))
            ->allowedSorts(['title'])
            ->allowedFilters([
                AllowedFilter::custom('title', new FilterOr()),
            ])
            ->paginate($request->input('per_page'));

        return $data;
    }

    // protected function updatePartial(Version $version, Request $request)
    // {
    //     foreach ($request->only('status') as $key => $content) {
    //         if ($version->isTranslatableAttribute($key)) {
    //             foreach ($content as $lang => $value) {
    //                 $version->setTranslation($key, $lang, $value);
    //             }
    //         } else {
    //             $version->{$key} = $content;
    //         }
    //     }

    //     $version->save();
    // }

    public function destroy(Version $version)
    {
        $version->delete();
    }
}
