<?php

namespace TypiCMS\Modules\Versions\Http\Controllers;

use Illuminate\View\View;
use TypiCMS\Modules\Core\Http\Controllers\BasePublicController;
use TypiCMS\Modules\Versions\Models\Version;

class PublicController extends BasePublicController
{
    public function index(): View
    {
        $models = Version::published()->order()->with('image')->get();

        return view('versions::public.index')
            ->with(compact('models'));
    }

    public function show($slug): View
    {
        $model = Version::published()->whereSlugIs($slug)->firstOrFail();
        return view('versions::public.show')
            ->with(compact('model'));
    }
}
