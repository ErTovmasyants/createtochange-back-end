<?php

namespace TypiCMS\Modules\Versions\Http\Requests;

use TypiCMS\Modules\Core\Http\Requests\AbstractFormRequest;

class FormRequest extends AbstractFormRequest
{
    public function rules()
    {
        return [
            'name' => 'nullable',
            'title' => 'nullable|max:255',
            'comics_id' => 'nullable',
            'user_id' => 'nullable',
        ];
    }
}
