<?php

namespace TypiCMS\Modules\Versions\Models;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Laracasts\Presenter\PresentableTrait;
use TypiCMS\Modules\Core\Models\Base;
use TypiCMS\Modules\Files\Models\File;
use TypiCMS\Modules\Files\Traits\HasFiles;
use TypiCMS\Modules\History\Traits\Historable;
use TypiCMS\Modules\Versions\Presenters\ModulePresenter;
use TypiCMS\NestableCollection;
use TypiCMS\NestableTrait;
use Illuminate\Support\Facades\Storage;
class Version extends Base
{
    use HasFiles;
    use Historable;
    use NestableTrait;
    use PresentableTrait;

    protected $presenter = ModulePresenter::class;

    protected $guarded = [];

    public $translatable = [
    ];

    public function parent(): BelongsTo
    {
        return $this->belongsTo(self::class, 'id');
    }

    public function children(): HasMany
    {
        return $this->hasMany(self::class, 'parent_id');
    }

    public static function nestable($id): NestableCollection
    {
        $nestedCollection = Version::where('comics_id', $id)
            ->where('parent_id', '>', '0')
            ->where('user_id', auth()->user()->id)
            ->orderBy('position', 'asc')
            ->get()
            ->noCleaning()
            ->nest();

        return $nestedCollection;
    }

    public static function createFirstVersion($id)
    {
        $data = [
            'user_id' => auth()->user()->id,
            'comics_id' => $id,
            'parent_id' => 0,
        ];

        $result = Version::create($data);

        return $result;
    }

    public function getFirstLevel($id){
       return Version::where('user_id',auth()->user()->id)->where('comics_id',$id)->where('parent_id',0)->get()->first();
    }

    public function getThumbAttribute(): string
    {
        return $this->present()->image(null, 54);
    }

    public function image(): BelongsTo
    {
        return $this->belongsTo(File::class, 'image_id');
    }

    public function firstlevelimages(){
        $images = json_decode($this->name);
        $fullnames = [];
        foreach ($images as $img){
            $fullnames[] = Storage::url($this->path.'/'.$img);;
        }
        return  $fullnames;
    }

    public function getFirstLevelVideo(){
        $video = json_decode($this->name);
        $fullnames = Storage::url($this->path.'/'.$video->video_mp4);;
        return  $fullnames;
    }
}
