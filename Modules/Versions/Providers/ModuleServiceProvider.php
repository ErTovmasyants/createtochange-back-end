<?php

namespace TypiCMS\Modules\Versions\Providers;

use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\ServiceProvider;
use TypiCMS\Modules\Core\Facades\TypiCMS;
use TypiCMS\Modules\Core\Observers\SlugObserver;
use TypiCMS\Modules\Versions\Composers\SidebarViewComposer;
use TypiCMS\Modules\Versions\Facades\Versions;
use TypiCMS\Modules\Versions\Models\Version;

class ModuleServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->mergeConfigFrom(__DIR__.'/../config/config.php', 'typicms.versions');
        $this->mergeConfigFrom(__DIR__.'/../config/permissions.php', 'typicms.permissions');

        $modules = $this->app['config']['typicms']['modules'];
        $this->app['config']->set('typicms.modules', array_merge(['versions' => ['linkable_to_page']], $modules));

        $this->loadViewsFrom(null, 'versions');

        $this->publishes([
            __DIR__.'/../database/migrations/create_versions_table.php.stub' => getMigrationFileName('create_versions_table'),
        ], 'migrations');

        AliasLoader::getInstance()->alias('Versions', Versions::class);

        // Observers
        // Version::observe(new SlugObserver());

        /*
         * Sidebar view composer
         */
        $this->app->view->composer('core::admin._sidebar', SidebarViewComposer::class);

        /*
         * Add the page in the view.
         */
        $this->app->view->composer('versions::public.*', function ($view) {
            $view->page = TypiCMS::getPageLinkedToModule('versions');
        });
    }

    public function register()
    {
        $app = $this->app;

        /*
         * Register route service provider
         */
        $app->register(RouteServiceProvider::class);

        $app->bind('Versions', Version::class);
    }
}
