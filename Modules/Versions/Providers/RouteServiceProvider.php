<?php

namespace TypiCMS\Modules\Versions\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Route;
use TypiCMS\Modules\Core\Facades\TypiCMS;
use TypiCMS\Modules\Versions\Http\Controllers\AdminController;
use TypiCMS\Modules\Versions\Http\Controllers\ApiController;
use TypiCMS\Modules\Versions\Http\Controllers\PublicController;

class RouteServiceProvider extends ServiceProvider
{
    public function map()
    {
        /*
         * Front office routes
         */
        if ($page = TypiCMS::getPageLinkedToModule('versions')) {
            $middleware = $page->private ? ['public', 'auth'] : ['public'];
            foreach (locales() as $lang) {
                if ($page->isPublished($lang) && $uri = $page->uri($lang)) {
                    Route::middleware($middleware)->prefix($uri)->name($lang.'::')->group(function (Router $router) {
                        $router->get('/', [PublicController::class, 'index'])->name('index-versions');
                        $router->get('{slug}', [PublicController::class, 'show'])->name('version');
                    });
                }
            }
        }

        /*
         * Admin routes
         */
        Route::middleware('admin')->prefix('admin')->name('admin::')->group(function (Router $router) {
            $router->get('versions', [AdminController::class, 'index'])->name('index-versions')->middleware('can:read versions');
            $router->get('versions/export', [AdminController::class, 'export'])->name('export-versions')->middleware('can:read versions');
            $router->get('versions/create', [AdminController::class, 'create'])->name('create-version')->middleware('can:create versions');
            $router->get('versions/{version}/edit', [AdminController::class, 'edit'])->name('edit-version')->middleware('can:read versions');
            $router->post('versions', [AdminController::class, 'store'])->name('store-version')->middleware('can:create versions');
            $router->put('versions/{version}', [AdminController::class, 'update'])->name('update-version')->middleware('can:update versions');
        });

        /*
         * API routes
         */
        Route::middleware(['api', 'auth:api'])->prefix('api')->group(function (Router $router) {
            $router->get('versions', [ApiController::class, 'index'])->middleware('can:read versions');
            $router->patch('versions/{version}', [ApiController::class, 'updatePartial'])->middleware('can:update versions');
            $router->delete('versions/{version}', [ApiController::class, 'destroy'])->middleware('can:delete versions');
        });
    }
}
