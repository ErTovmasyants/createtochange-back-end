<?php

return [
    'versions' => [
        'read versions' => 'Read',
        'create versions' => 'Create',
        'update versions' => 'Update',
        'delete versions' => 'Delete',
    ],
];
