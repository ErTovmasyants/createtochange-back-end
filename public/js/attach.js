var addedAttach = null;
var addedActions = null;
var addAttachBtn = null;
var attachedBlock = null;

$(document).ready(function() {
    if ($('.tools_block').length > 0) {
        addedAttach = '<div class="attach_block">' + $('.tools_block .attach_block').html() + '</div>';

        addedActions = '<div class="actions_block">' + $('.tools_block .actions_block').html() + '</div>';
        addAttachBtn = $('.tools_block .add_attach').length > 0 ? '<span class="add_attach icon_upload">' + $('.tools_block .add_attach').html() + '</span>' : null;
        $('.tools_block').remove();
    };

    $(document).on('click', '.add_attach', function() {
        $('.upload_section .files_list').append(addedAttach);
        attachFile($('.files_list .attach_block:last-child > .attach_label input[type="file"]'));
        setTimeout(function() {
            $('.upload_section .section_inner').animate({ scrollTop: $('.files_list').height() }, 700);
        }, 100)
        $('.attach_error').remove();
        $(this).remove();
    });

    $(document).on('click', '.move_btn', function() {
        $('.attach_block.movable').removeClass('moveable');
        $(this).parents('.attach_block').addClass('moveable');
        $('.moveable').append('<div class="move_btns"><span class="move_up"></span><span class="move_down"></span></div>');
    });

    $(document).on('click', function(e) {
        if ($('.moveable').length > 0 && !$(e.target).is('.move_btns *') && !$(e.target).is('.move_btn')) {
            $('.moveable').removeClass('moveable');
            $('.move_btns').remove();
        }
    })

    var moving = false;

    $(document).on('click', '.move_up, .move_down', function() {
        var moveableHeight = $('.moveable').height() + 26;


        if (!moving) {

            if ($(this).hasClass('move_up') && $('.moveable').index() != 0) {
                moving = true;
                var prevHeight = $('.moveable').prev().height() + 26;
                var prevTop = $('.moveable').prev().offset().top;
                $('.moveable').addClass('moving').css('transform', 'translateY(' + -prevHeight + 'px)');
                $('.moveable').prev().addClass('moving').css('transform', 'translateY(' + moveableHeight + 'px)');
                $('.section_inner').animate({ scrollTop: prevTop - $('.files_list').offset().top }, 1000);
                setTimeout(function() {
                    $('.moveable').removeClass('moving').css('transform', 'none');
                    $('.moveable').prev().removeClass('moving').css('transform', 'none');
                    $('.moveable').prev().before($('.moveable')[0]);
                    moving = false;
                }, 1000)
            } else if ($(this).hasClass('move_down') && $('.moveable').index() != $('.attach_block').length - 1) {
                moving = true;
                var nextHeight = $('.moveable').next().height() + 26;
                var nextTop = $('.moveable').next().offset().top;
                $('.moveable').addClass('moving').css('transform', 'translateY(' + nextHeight + 'px)');
                $('.moveable').next().addClass('moving').css('transform', 'translateY(' + -moveableHeight + 'px)');
                $('.section_inner').animate({ scrollTop: nextTop - $('.files_list').offset().top - ($('.moveable').height() - nextHeight) }, 1000);
                setTimeout(function() {

                    $('.moveable').removeClass('moving').css('transform', 'none');
                    $('.moveable').next().removeClass('moving').css('transform', 'none');
                    $('.moveable').next().after($('.moveable')[0]);
                    moving = false;
                }, 1000)
            }
        }
    })
})

function attachFile($attachField) {
    var attachError = null;
    var fieldParent = $attachField.parents('.attach_block');
    $attachField.on('change', function() {
        var fileSize = this.files[0] ? this.files[0].size : null;
        var fileMaxSize = $attachField.data('maxsize') ? $attachField.data('maxsize') * 1024 * 1024 : null;
        var formats = $(this).attr('accept') ? $(this).attr('accept').toString() : null;
        var fullPath = $(this).val();
        var fileType = fullPath.substr(fullPath.lastIndexOf('.')).toLowerCase();

        if (fileMaxSize && fileSize > fileMaxSize) {
            if (!attachError) {
                fieldParent.append('<span class="attach_error"></span>');
                attachError = fieldParent.find('.attach_error');
            };
            attachError.text($attachField.data('sizeerror'));
            $attachField.val('');
            if ($attachField.parents('.attach_block').hasClass('comics_poster')) {
                $attachField.parents('.attach_block').find('.image_block').empty();
                $attachField.parents('.attach_block').find('.has-error').removeClass('has-error');
            }
            if ($attachField.parents('.upload_section').length > 0 && $attachField.parents('.attach_block').find('.actions_block').length > 0) {
                $attachField.parents('.attach_block').find('.image_block, .video_block').remove();

            }
        } else if (formats && !formats.includes(fileType)) {
            if (!attachError) {
                fieldParent.append('<span class="attach_error"></span>');
                attachError = fieldParent.find('.attach_error');
            };
            attachError.text($attachField.data('typeerror'));
            $attachField.val('');
            if ($attachField.parents('.attach_block').hasClass('comics_poster')) {
                $attachField.parents('.attach_block').find('.image_block').empty();
                $attachField.parents('.attach_block').find('.has-error').removeClass('has-error');
            };
            if ($attachField.parents('.upload_section').length > 0 && $attachField.parents('.attach_block').find('.actions_block').length > 0) {
                $attachField.parents('.attach_block').find('.image_block, .video_block').remove();
                if ($attachField.data('type') && $attachField.data('type') == 'video') {
                    $('.upload_section .files_list').append(addedAttach);
                    attachFile($('.upload_section .files_list input[type="file"]'));
                }
            }
        } else {
            if (fullPath) {
                if (attachError) {
                    $(attachError).remove();
                    attachError = null;
                }
                $attachField.parents('.attach_block').find('.has-error').removeClass('has-error');


                if ($attachField.parents('.upload_section').length > 0) {
                    $('.attach_error').remove();
                    if ($attachField.parents('.actions_list').length < 1) {
                        if ($attachField.data('type') && $attachField.data('type') == 'video') {
                            fieldParent.append('<div class="video_block"><video preload="auto"><source src="' + window.URL.createObjectURL($attachField[0].files[0]) + '#t=1"></video><span class="video_screen" data-video="start_video"></span></div>');
                            fieldParent.find('.video_block').append(addedActions);
                        } else {
                            fieldParent.append('<div class="image_block"></div>');
                            fieldParent.find('.image_block').append('<img src="' + window.URL.createObjectURL($attachField[0].files[0]) + '"/>');
                            fieldParent.find('.image_block').append(addedActions);
                        }

                        fieldParent.after(addAttachBtn);
                        fieldParent.find('.actions_block .attach_label').prepend($attachField[0]);
                        fieldParent.find('> .attach_label').remove();
                        attachFile(fieldParent.find('.actions_block input[type="file"]'));
                    } else {
                        if ($attachField.data('type') && $attachField.data('type') == 'video') {
                            fieldParent.find('video').attr('src', window.URL.createObjectURL($attachField[0].files[0]) + '#t=1');
                            if (fieldParent.find('video').attr('data-videokey')) {
                                fieldParent.find('video').removeAttr('data-videokey');
                            }
                            $('.video_block').removeClass('playing');
                        } else {
                            fieldParent.find('.image_block img').attr('src', window.URL.createObjectURL($attachField[0].files[0]));
                            if (fieldParent.find('.image_block img').attr('data-imgkey')) {
                                fieldParent.find('.image_block img').removeAttr('data-imgkey');
                            }

                        }
                    }
                } else if ($attachField.data('preview') && $attachField.data('preview') == 'on') {
                    console.log($attachField, $attachField.data('preview'));
                    if (fieldParent.find('.image_block img').length < 1) {
                        fieldParent.find('.image_block').append('<img src="' + window.URL.createObjectURL($attachField[0].files[0]) + '"/>');
                    } else {
                        fieldParent.find('.image_block img').attr('src', window.URL.createObjectURL($attachField[0].files[0]));
                        if (fieldParent.find('.image_block img').attr('data-imgkey')) {
                            fieldParent.find('.image_block img').removeAttr('data-imgkey');
                        }
                    }
                } else if ($attachField.data('type') && $attachField.data('type') == "simple") {
                    var startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'));
                    var filename = fullPath.substring(startIndex);
                    if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
                        filename = filename.substring(1);
                    }
                    if (!attachedBlock) {
                        $attachField.parents('label').after('<div class="attached_block"><div class="file_name"></div><span class="file_remove"></span></div>');
                        attachedBlock = $attachField.parents('label').next('.attached_block');
                        attachName = attachedBlock.find('.file_name');
                    }
                    attachName.text(filename);
                    $attachField.parents('label').addClass('hidden');
                };

                fieldParent.find('.attach_remove').removeClass('disable');
            }
        }
    })

    $('.delete_popup .delete_btn').click(function() {
        var removedFileData = $(this).attr('data-file');
        var removedFile = $('[data-file="' + removedFileData + '"]:not(.delete_btn)')
        var removedFileBlock = $('[data-file="' + removedFileData + '"]:not(.delete_btn):not(.attach_block)').parents('.attach_block');
        var emptyField = removedFileBlock.find('input[type="file"]');
        var removePopupBtn = removedFileBlock.find('.attach_remove');
        removedFile.remove();
        emptyField.val('');
        removePopupBtn.addClass('disable');
        $('.delete_popup').removeClass('showed');
        $('body').removeClass('popup_opened');
        if ($('.upload_section').length > 0 && $('.upload_section .attach_block').length < 1) {
            $('.upload_section .files_list').append(addedAttach);
            attachFile($('.upload_section .files_list input[type="file"]'));
            $('.add_attach').remove();
        }
    });

    $(document).on('click', '.file_remove', function() {
        if ($('#PdfBlock').length > 0) {
            attachedBlock = $('#PdfBlock');
        }
        if (attachedBlock) {
            $(this).parents('.attach_block').find('input').val('');
            $(this).parents('.attach_block').find('.attach_label').removeClass('hidden');
            $(attachedBlock).remove();
            attachedBlock = null;

        }
    })
}