$(document).ready(function() {
    $('select[name="country"]').on('change', function() {

        var countryID = $(this).val();
        var url = $('.countryAjaxUrl').val();
        var field_name = $('.cityFieldName').val();
        var selectedCity = $('.selectedCity').val();
        if (countryID) {
            $.ajax({
                url: url + '/' + encodeURI(countryID),
                type: "GET",
                data: { 'country_code': encodeURI(countryID) },
                dataType: "json",
                success: function(data) {

                    if (data.length > 0) {
                        $('.cityField').remove();
                        var selectcity = ' <select name="city" id="city" data-placeholder="' + field_name + '" data-search="5"></select>';
                        if (!$('select[name="city"]').length) {
                            $('.cityBlock').append(selectcity);
                            var minSearch = $(this).data('search') ? $(this).data('search') : Infinity;
                            $('select[name="city"]').select2({
                                placeholder: $(this).data('placeholder'),
                                minimumResultsForSearch: minSearch,
                                width: '100%'
                            });
                        }

                        $('select[name="city"]').empty();
                        $('select[name="city"]').removeAttr("disabled");
                        $.each(data, function(key, value) {
                            let slc = '';
                            if (selectedCity == value.id) {
                                slc = 'selected';
                            }
                            $('select[name="city"]').append('<option value="' +
                                value.id + '"' + slc + '>' + value.city + '/' + value.admin_name + '</option>');
                        });
                    } else {
                        var inputcity = '<input class="cityField" type="text" name="city" id="city" placeholder="' + field_name + '" autocomplete="off">';
                        $('select[name="city"]').select2('destroy')
                        $('select[name="city"]').remove();
                        if (!$('.cityField').length) {
                            $('.cityBlock').append(inputcity);
                        }

                    }
                }
            });
        }
    });

    if ($('.profile_inner').length > 0) {
        $('#country option').filter(':selected').trigger('change');
    }

    $(document).on('click', '.showLoader', function() {
        _showLoader();
    });

    function _showLoader() {
        $('body').removeClass('popup_opened');
        $('.popup_block').removeClass('showed');
        $('html').removeClass('loaded');
    }

    $(document).on('click', '.addVersion', function() {
        var vID = $(this).attr('data-parent-id');
        var url = $('.addVu').val();
        var cID = $('.cID').val();
        var keyU = $('.keyU').val();
        var keyVal = $('.keyVal').val();
        var curU = $('.curU').val();
        var verT = $('.verT').val();
        if (vID) {
            $.ajax({
                type: 'POST',
                url: url,
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                data: {
                    id: vID,
                    comics_id: cID,
                    keyVal: keyVal,
                    keyU: keyU,
                    verT: verT
                },
                success: function(data) {
                    if (data) {
                        window.location = curU + '/' + data.id;
                    } else {
                        alert('Something went wrong');
                    }
                },
                fail: function() {
                    alert('Something went wrong');
                }
            });
        }
    });


    $(document).on('click', '.removeVbtn', function() {

        var vID = $(this).attr('data-id');
        var rVu = $('.rVu').val();
        var curU = $('.curU').val();
        var keyU = $('.keyU').val();
        var keyVal = $('.keyVal').val();

        if (vID) {
            _showLoader();
            $.ajax({
                type: 'DELETE',
                url: rVu + '/' + vID,
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                data: {
                    id: vID,
                    keyVal: keyVal,
                    keyU: keyU
                },
                success: function(data) {
                    if (data) {
                        window.location = curU;
                    } else {
                        alert('Something went wrong');
                    }
                },
                fail: function() {
                    alert('Something went wrong');
                }
            });
        }
    });
});