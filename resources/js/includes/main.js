var $mobileSize = 960;

function isTouchDevice() {
    return 'ontouchstart' in document.documentElement;
};

function detectDevice() {
    if (navigator.userAgent.search("Safari") >= 0 && navigator.userAgent.search("Chrome") < 0) {
        $('body').addClass('ios_device');
    };
    if (isTouchDevice()) {
        $('html').addClass('touch');
    } else {
        $('html').addClass('web');
    }
}

function closeAllMenues(evt) {
    detectDevice();
    if (isTouchDevice() && window.innerWidth >= $mobileSize) {
        $('.header .main_menu li').removeClass('opened');
        $('.header .submenu_list').fadeOut(300);
    };

    if ($('.profile_btn').length > 0) {
        $('.sign_block').removeClass('opened');
        $('.profile_menu').slideUp(300);
    };
    if ($('.category_filter').length > 0) {
        $('.category_filter').removeClass('opened');
    }
}

function countFilterWidth() {
    var filterWidth = 0;
    var containerWidth = $('.category_filter').width();
    var listWidth = $('.category_filter ul').width();
    var btnPadding = 2 * parseInt($('.category_filter a').css('padding-left'));
    var moreBtnText = window.innerWidth > 576 ? $('.category_filter').data('more') : $('.category_filter .selected').data('text');
    var dropWidth = window.innerWidth >= 1200 ? 220 : 180;
    var inDropLeft = containerWidth - dropWidth;
    $('.category_filter a span').each(function() {
        filterWidth += $(this).width() + btnPadding;
        if (filterWidth > listWidth) {
            $(this).parents('li').addClass('in_drop').css('margin-left', inDropLeft);
        } else {
            $(this).parents('li').removeClass('in_drop');
        };
        if ($(this).parents('li').index() == $(this).parents('ul').find('li').length - 1 && filterWidth <= containerWidth) {
            $('.in_drop').removeClass('in_drop');
            $('.more_btn').remove();
        }
    });
    setTimeout(function() {
        if ($('.in_drop').length > 0) {
            if ($('.category_filter .more_btn').length < 1) {
                $('.category_filter').append('<button class="more_btn icon_down"><span>' + moreBtnText + '<span></button>');
                if ($('.in_drop').eq(0).prev().offset().top > $('.category_filter ul').offset().top) {
                    $('.in_drop').eq(0).prev().addClass('in_drop').css('margin-left', inDropLeft);
                }
            } else {
                $('.more_btn span').text(moreBtnText);
            }
            $('.more_btn').click(function(evt) {
                if (!$('.category_filter').hasClass('opened')) {
                    closeAllMenues(evt);
                    evt.stopPropagation();
                    $('.category_filter').addClass('opened');
                }
            })
        } else {
            $('.more_btn').remove();
        }
    }, 1)
}

function ignorBodyClick(evt) {
    evt.stopPropagation();
}

function ignorMobileBodyClick(evt) {
    if (window.innerWidth < 992) {
        evt.stopPropagation();
    }
}

function dropList(dropButton, dropList, dropItem, dropElement) {
    if (dropButton.parents(dropItem).hasClass('opened')) {
        dropButton.parents(dropItem).removeClass('opened').find(dropElement).slideUp(300);
    } else {
        dropButton.parents(dropList).find('.opened').removeClass('opened');
        dropButton.parents(dropList).find(dropElement).slideUp(300);
        dropButton.parents(dropItem).addClass('opened').find(dropElement).stop(true, true).slideDown(300);
        setTimeout(function() {
            if ($(dropList).find('.opened').length > 0) {
                if (dropButton.parents(dropItem).offset().top < $(document).scrollTop()) {
                    $('body,html').animate({ scrollTop: dropButton.parents(dropItem).offset().top }, 300);
                }
            }
        }, 300)
    }

};

function mobMenuTrigger(e) {
    e.preventDefault();
    if ($('body').hasClass('menu_opened')) {
        $('body').removeClass('menu_opened');
    } else {
        $('.header .main_menu li').removeClass('opened');
        $('.header .submenu_list').hide();
        $('.header .menu_block').animate({ scrollTop: 0 }, 0);
        $('body').addClass('menu_opened');
    }
}

function detectContentHeight() {
    var freeSpace = window.innerHeight - $('.header').height() - $('.footer').height();
    if (freeSpace > 0) {
        $('.content').css('min-height', freeSpace);
    } else {
        $('.content').css('min-height', 0);
    };
    $('.footer').css('opacity', 1);
}

var passwordConfirm = null;

function checkFields() {
    $('form input, form textarea').change(function() {
        if ($(this).val().length > 0) {
            $(this).parent().find('.individual_hint').show();
            $(this).parent().find('.standard_hint').hide();
            if ($(this).data('field') == "new_pass") {
                $('[data-field="old_pass"]').attr('data-validation', 'required');
                $('[data-field="new_pass_confirm').addClass('confirm_field');
            }
        } else {
            $(this).parent().find('.individual_hint').hide();
            $(this).parent().find('.standard_hint').show();
            if ($(this).data('field') == "new_pass") {
                $('[data-field="old_pass"]').removeAttr('data-validation').parents('.field_block').removeClass('has-error');
                $('[data-field="new_pass_confirm').removeClass('confirm_field').parents('.field_block').removeClass('has-error');
            }
        }

        if ($('.confirm_field').length > 0) {
            $('.confirm_field').on('keyup change', function() {
                if ($(this).val() == $(this).parents('form').find('.password_field').val()) {
                    $(this).parent().removeClass('has-error');
                    passwordConfirm = true;
                }
            })
        };
        setTimeout(function() {
            if ($('.has-error').length < 1) {
                $('.validate_btn').removeAttr('disabled');
            }
        }, 100)

    });
}

function checkFieldsDraft() {
    $('form input, form textarea').removeAttr('data-validation').parents('.field_block').removeClass('has-error')
}


function checkPassConfirm() {
    var passValue = $('.confirm_field').parents('form').find('.password_field').val();
    var passConfirm = $('.confirm_field').val();
    if (passValue && passValue != passConfirm) {
        $('.confirm_field').parent().addClass('has-error');
        passwordConfirm = null;
    } else {
        passwordConfirm = true;
    }
}

var stopSubmit = true;

function checkForm(e) {
    setTimeout(function() {
        $('.validate_btn').removeAttr('disabled');
    }, 10)
    var $button = $(this);
    if ($button.parents('form').find('.confirm_field').length > 0) {
        checkPassConfirm();
    } else {
        passwordConfirm = true;
    }
    $.validate({
        onError: function() {
            $button.attr('disabled', 'disabled');
        },
        onSuccess: function() {
            if (!passwordConfirm) {
                $button.attr('disabled', 'disabled');
                return false;
            }

            $('html').removeClass('loaded');
            if ($button.data('popup') && stopSubmit == true) {

                $('body').addClass('popup_opened');
                $('.' + $button.data('popup')).addClass('showed');
                stopSubmit = null;
                return false;
            }
        }

    });
    setTimeout(function() {
        if ($button.hasClass('checkout_submit') && $('.has-error').length > 0) {
            $('body, html').animate({ scrollTop: $('.has-error').eq(0).offset().top - $('.header').height() }, 1000);
        }
    }, 100)


};

function openLanguages(evt) {
    evt.preventDefault();
    if (!$('.language_block').hasClass('opened')) {
        closeAllMenues(evt);
        evt.stopPropagation();
        $('.language_block').addClass('opened');
        $('.language_list').stop(true, true).slideDown();
    };
}

function dropToggle(e) {
    e.preventDefault();
    if ($(this).parent().hasClass('opened')) {
        $(this).parent().removeClass('opened').find('.drop_element').slideUp(300);
    } else {
        $('.drops_list li').removeClass('opened');
        $('.drop_element').slideUp(300);
        $(this).parent().addClass('opened').find('.drop_element').stop(true, true).slideDown(300);
        setTimeout(function() {
            if ($('.drops_list li.opened').offset().top < $(document).scrollTop()) {
                $('body,html').animate({ scrollTop: $('.drops_list li.opened').offset().top }, 300);
            }
        }, 300)
    }
};

function openSubWithClick(evt) {
    evt.preventDefault();
    if (isTouchDevice() && window.innerWidth >= $mobileSize) {
        if (!$(this).parents('li').hasClass('opened')) {
            closeAllMenues(evt);
            evt.stopPropagation();
            $(this).parents('li').addClass('opened').find('.submenu_list').stop(true, true).slideDown(300);
        }
    } else if (window.innerWidth < $mobileSize) {
        if ($(this).parents('li').hasClass('opened')) {
            $(this).parents('li').removeClass('opened').find('.submenu_list').slideUp(300);
        } else {
            $('.header .main_menu > li.opened').removeClass('opened');
            $('.header .main_menu .submenu_list').slideUp(300);
            $(this).parents('li').addClass('opened').find('.submenu_list').stop(true, true).slideDown(300);
        }
    }
}

var delayTime = null;

function openSubWithHover() {
    if (!isTouchDevice() && window.innerWidth >= $mobileSize) {
        if (delayTime) {
            clearTimeout(delayTime);
        };
        var $item = $(this).parents('li');
        $item.addClass('hovered');
        delayTime = setTimeout(function() {
            if ($item.hasClass('hovered')) {
                $item.addClass('opened').find('.submenu_list').stop(true, true).slideDown(300);
            }
        }, 300)
    };
}

function mouseLeaveItem() {
    $(this).parents('li').removeClass('hovered');
}

function closeSubWithHover() {
    if (!isTouchDevice()) {
        $(this).removeClass('opened').find('.submenu_list').fadeOut(300);
    }
}

function comboHover() {
    $(this).parents('.combo_hover').addClass('hovered');
}

function comboUnHover() {
    $(this).parents('.combo_hover').removeClass('hovered');
}

function tabSwitch(e) {
    e.preventDefault();
    if (!$(this).hasClass('selected')) {
        $(this).parents('.tab_buttons').find('a').removeClass('selected');
        $(this).parents('.tab_section').find('.tab_block').removeClass('selected');
        $(this).addClass('selected');
        $('.tab_block.' + $(this).data('tab')).addClass('selected');
        if ($('.tab_section').data('hash') && $('.tab_section').data('hash') == 'true');
        window.location.hash = $(this).data('tab');
    }
}

function detectCallPosibillity() {
    if (/Android|iPhone|iPad|iPod|BlackBerry|BB|PlayBook|IEMobile|Windows Phone|Kindle|Silk|Opera Mini/i.test(navigator.userAgent)) {
        $('.phone_link').addClass('clickable');
    }
    $('.phone_link').click(function(e) {
        if (!$(this).hasClass('clickable')) {
            e.preventDefault();
        }
    })
}

function initSlider(sliderEl, sliderOpts) {
    sliderEl.slick(sliderOpts);
}

function accordionToggle(_button, _block, _list) {
    if (_button.parent(_block).hasClass('opened')) {
        _button.parent(_block).removeClass('opened');
        _button.parent(_block).find(_list).slideUp(300);
    } else {
        $(_block).removeClass('opened');
        $(_list).slideUp(300);
        _button.parent(_block).addClass('opened').find(_list).stop(true, true).slideDown(300);
    }
}

function goToTarget() {
    var endPoint = $(this).data('endpoint');
    $('html,body').animate({ scrollTop: $('[data-target="' + endPoint + '"]').offset().top - $('.header_inner').height() }, 500);
    if ($('[data-target="' + endPoint + '"]').parent().hasClass('tab_buttons')) {
        $('[data-target="' + endPoint + '"]').trigger('click');
    }
}

function openPopup(evt) {
    evt.preventDefault();
    $('body').addClass('popup_opened');
    var popupName = '.' + $(this).data('popup');
    $(popupName).addClass('showed');
}

function closePopup() {
    $('body').removeClass('popup_opened');
    $('.popup_block').removeClass('showed');
    if ($('.validate_btn[data-popup]').length > 0) {
        stopSubmit = true;
    }
}

function changeCount(countBlock, decreaseBtn, increaseBtn, countInput) {

    $(countInput).each(function() {
        var maxValue = $(this).data('max') ? $(this).data('max') : Math.pow(10, $(this).attr('maxlength')) - 1;
        if ($(this).val() == 1) {
            $(this).parents(countBlock).find(decreaseBtn).addClass('inactive');
        } else if ($(this).val() == maxValue) {
            $(this).parents(countBlock).find(increaseBtn).addClass('inactive');
        }
    });

    $(document).on('change', countInput, function() {
        var thisDecrease = $(this).parents(countBlock).find(decreaseBtn);
        var thisIncrease = $(this).parents(countBlock).find(increaseBtn);
        var maxValue = $(this).data('max') ? $(this).data('max') : Math.pow(10, $(this).attr('maxlength')) - 1;
        if ($(this).val() <= 1) {
            $(this).val(1);
            thisDecrease.addClass('inactive');
            thisIncrease.removeClass('inactive');
        } else if ($(this).val() >= maxValue) {
            $(this).val(maxValue);
            thisIncrease.addClass('inactive');
            thisDecrease.removeClass('inactive');
        } else {
            thisIncrease.removeClass('inactive');
            thisDecrease.removeClass('inactive');
        }
    })

    $(document).on('click', decreaseBtn, function() {
        var thisInput = $(this).parent().find('input');
        var thisIncrease = $(this).parent().find(increaseBtn);
        var _value = thisInput.val();
        thisIncrease.removeClass('inactive');
        if (_value > 1) {
            _value--;
            thisInput.val(_value);
        }
        if (_value == 1) {
            $(this).addClass('inactive');
        }
    });

    $(document).on('click', increaseBtn, function() {
        var thisInput = $(this).parent().find('input');
        var thisDecrease = $(this).parent().find(decreaseBtn);
        var _value = thisInput.val();
        var maxValue = thisInput.data('max') ? thisInput.data('max') : Math.pow(10, thisInput.attr('maxlength')) - 1;
        thisDecrease.removeClass('inactive');
        if (_value < maxValue) {
            _value++;
            thisInput.val(_value);
        }
        if (_value == maxValue) {
            $(this).addClass('inactive');
        }
    });
}
$(document).ready(function() {
    //detect device type
    detectDevice();
    detectCallPosibillity();

    //close dropdowns with outside click
    $('body').click(closeAllMenues);

    //opening/closing mobile menu
    $('.menu_btn').click(mobMenuTrigger);
    $('.header .main_menu .submenu_btn').hover(openSubWithHover, mouseLeaveItem);
    $('.header .main_menu > li').hover(function() {}, closeSubWithHover);
    $('.header .submenu_btn').click(openSubWithClick);

    // form front validation
    if ($('.validate_btn').length > 0) {
        checkFields();
        $('.draft_btn').click(checkFieldsDraft);
        $('.validate_btn').click(checkForm);
    };

    //hover effect with multiple links hover
    $('.combo_link').hover(comboHover, comboUnHover);

    $('.profile_btn').click(function(evt) {
        evt.preventDefault();
        if (!$('.sign_block').hasClass('opened')) {
            closeAllMenues(evt);
            evt.stopPropagation();
            $('.sign_block').addClass('opened');
            $('.profile_menu').stop(true, true).slideDown(300);
        }
    });
    if ($('.waves').length > 0) {
        $(window).scroll(function() {
            if ($(document).scrollTop() + window.innerHeight * 0.8 >= $('.footer_middle').offset().top && !$('.waves').hasClass('showed')) {
                $('.waves').addClass('showed');
            }
        }).trigger('scroll');
    };
    if ($('.participants').length > 0) {
        if ($('.members_list').length > 0) {
            $('.members_list').scrollbar();
            $(window).scroll(function() {
                if ($(document).scrollTop() + window.innerHeight >= $('.members_list').offset().top + $('.members_list').height() / 2 && !$('.members_list ul').hasClass('showed')) {
                    $('.members_list ul').addClass('showed');
                }
            }).trigger('scroll');

        }
    };

    if ($('.faq_list').length > 0) {
        $('.faq_list .toggle_btn, .faq_list .question_block').click(function() {
            accordionToggle($(this), 'li', '.answer_block');
        });
        $('.faq_list li').eq(0).find('.toggle_btn').trigger('click');
    }

    $('.tab_buttons a').click(tabSwitch);
    if ($('.tab_section').length > 0 && $('.tab_buttons .selected').length < 1) {
        if (window.location.hash) {
            var $tab = window.location.hash.replace('#', '');
            $('.tab_section a[data-tab="' + $tab + '"]').trigger('click')
        } else {
            $('.tab_buttons li:first-child a').trigger('click');
        }
    }

    $('.type_switch').click(function() {
        var inputType = $(this).parents('.field_block').find('input').attr('type') == 'text' ? 'password' : 'text';
        $(this).parents('.field_block').find('input').attr('type', inputType);
    });

    if ($('select').length > 0) {
        $('select').each(function() {
            var minSearch = $(this).data('search') ? $(this).data('search') : Infinity;
            $(this).select2({
                placeholder: $(this).data('placeholder'),
                minimumResultsForSearch: minSearch,
                width: '100%'
            });
        })
    };


    $('.popup_btn').click(openPopup);
    $('.popup_block').click(function(e) {
        if (!$(e.target).is('.popup_inner *') || $(e.target).is('.cancel_btn') || $(e.target).is('.popup_close')) {
            closePopup();
        }
    });


    $('.add_field').click(function() {
        var $addBtn = $(this);
        var empty = null;
        $('.input_block input').each(function() {
            if (!$(this).val()) {
                empty = true;
                $(this).focus();
            }
        });
        setTimeout(function() {
            if (!empty) {
                $addBtn.before('<div class="input_block">' +
                    '<input type="url" pattern="http(s?):\/\/(?:(?:[a-zA-Z\u00a1-\uffff0-9]+-?)*[a-zA-Z\u00a1-\uffff0-9]+)(?:\.(?:[a-zA-Z\u00a1-\uffff0-9]+-?)*[a-zA-Z\u00a1-\uffff0-9]+)*(?:\.(?:[a-zA-Z\u00a1-\uffff]{2,}))(?::\d{2,5})?(?:\/[^\s]*)?" name="socials[]" placeholder="Social media link"/>' +
                    '<span class="field_remove icon_close"></span>' +
                    '</div>');
                $addBtn.prev().find('input').focus();
            }
        }, 1)

    })
    $(document).on('click', '.field_remove', function() {
        $(this).parent().remove();
    });

    if ($('.back_to_top').length > 0) {
        $(window).scroll(function() {
            if ($(document).scrollTop() > 1.5 * window.innerHeight) {
                $('.back_to_top').addClass('active')
            } else {
                $('.back_to_top').removeClass('active')
            }
        }).trigger('click');

        $('.back_to_top').click(function() {
            $('body,html').animate({ scrollTop: 0 }, 700);
        })
    };

    function stopVideos() {
        var videos = document.getElementsByTagName('video');
        $('.video_block').removeClass('playing');
        for (var i = 0; i < videos.length; i++) {
            videos[i].pause();
            videos[i].controls = false;
        };
    }

    if ($('.comics_inner').length > 0) {
        var actionsBlock = '<div class="actions_block"><div class="page_container"></div></div>';
        var comicsJson = $('.comics_inner_urlJson').val();
        var back_btn_text = $('#back_btn_text').val();
        var continue_text = $('#continue_text').val();
        var back_start_text = $('#back_start_text').val();
        var pbPath = $('#pbPath').val();
        var comicsData, backBtn, chooseTitle = null;
        if ($('.comics_inner').data('type') !== 'video') {
            back_start_text = '';
        }
        $.ajax({
            type: 'GET',
            dataType: "json",
            url: comicsJson,
            cache: false,
            success: function(data) {
                if (data['versions']) {
                    comicsData = data;
                    backBtn = '<button class="back_btn">' + back_btn_text + '</button>';
                    backToStart = back_start_text ? '<button class="back_to_start">' + back_start_text + '</button>' : '';
                    chooseTitle = '<div class="page_title">' + continue_text + '</div>';
                }
            },

            complete: function() {
                $('.comics_block').append(actionsBlock);
                $('.actions_block .page_container').append(chooseTitle);
                $('.actions_block .page_container').append('<ul class="version_btns"></ul>');

                if (comicsData.versions) {
                    for (var key in comicsData.versions) {
                        $('.version_btns').append('<li><button data-version="' + key + '"><span class="version_number">' + comicsData.versions[key].button['number'] + '</span><span class="version_title">' + comicsData.versions[key].button['text'] + '</span></button>');
                    };
                }


                $(document).on('click', '.version_btns button', function() {
                    var $versionNum = $(this).data('version');
                    var $parrent = $(this).parents('.actions_block');
                    var versionLevels = $versionNum.toString().split('.').join('');
                    var branchLevel = versionLevels.length == 1 ? comicsData.versions[$versionNum] :
                        versionLevels.length == 2 ? comicsData.versions[versionLevels.charAt(0)].versions[$versionNum] :
                        versionLevels.length == 3 ? comicsData.versions[versionLevels.charAt(0)].versions[$versionNum.substring(0, 3)].versions[$versionNum] :
                        versionLevels.length == 4 ? comicsData.versions[versionLevels.charAt(0)].versions[$versionNum.substring(0, 3)].versions[$versionNum.substring(0, 5)].versions[$versionNum] : versionLevels.length == 5 ? comicsData.versions[versionLevels.charAt(0)].versions[$versionNum.substring(0, 3)].versions[$versionNum.substring(0, 5)].versions[$versionNum.substring(0, 7)].versions[$versionNum] : comicsData.versions[versionLevels.charAt(0)].versions[$versionNum.substring(0, 3)].versions[$versionNum.substring(0, 5)].versions[$versionNum.substring(0, 7)].versions[$versionNum.substring(0, 9)].versions[$versionNum]
                    if (!$(this).hasClass('active')) {
                        $parrent.after('<div class="version_block" data-version="' + $versionNum + '"></div>');
                        $parrent.next('.version_block').append('<div class="comics_block">' +
                            '<div class="page_container"></div>' +
                            actionsBlock +
                            '</div>');
                        if ($('.comics_inner').data('type') == 'video') {
                            stopVideos();
                            $parrent.next('.version_block').find('.comics_block > .page_container').append('<div class="video_block">' +
                                '<video preload="auto" id="video_' + $versionNum + '#t=1"></video>' +
                                '<span class="video_screen" data-video="video_' + $versionNum + '"></span>' +
                                '</div>');
                        }
                        for (var key in branchLevel.content) {
                            if ($('.comics_inner').data('type') == 'video') {
                                $parrent.next('.version_block').find('video').append('<source type="video/mp4" src="' + pbPath + branchLevel.path + '/' + branchLevel.content[key] + '#t=1"/>');
                                $parrent.next('.version_block').find('video').bind("ended", function() {
                                    $(this).parent('.video_block').removeClass('playing');
                                    $(this).closest('.comics_block').find('> .actions_block').fadeIn(3000);
                                });
                            } else {
                                $parrent.next('.version_block').find('.comics_block > .page_container').append('<a href="' + pbPath + branchLevel.path + '/' + branchLevel.content[key] + '" data-fancybox="comics_images' + $versionNum + '"><img src="' + pbPath + branchLevel.path + '/' + branchLevel.content[key] + '" alt="" title=""/></a>');
                            }

                        }
                        if (branchLevel.versions) {
                            $parrent.next('.version_block').find('.actions_block > .page_container').append('<div class="back_btns">' + backBtn + backToStart + '</div>' + chooseTitle + '<ul class="version_btns"></ul>');
                            for (var key in branchLevel.versions) {
                                $parrent.next('.version_block').find('.actions_block .version_btns').append('<li><button data-version="' + key + '"><span class="version_number">' + branchLevel.versions[key].button['number'] + '</span><span class="version_title">' + branchLevel.versions[key].button['text'] + '</span></button>')
                            }
                        } else {
                            $parrent.next('.version_block').find('.actions_block > .page_container').append('<div class="back_btns">' + backBtn + backToStart + '</div>');
                        }

                        $(this).addClass('active');
                    }
                    $(this).closest('.comics_block').find('> .version_block').removeClass('showed');
                    $(this).closest('.comics_block').find('> .version_block[data-version="' + $versionNum + '"]').addClass('showed');
                    if (!$('.comics_inner').data('type') || !$('.comics_inner').data('type') == 'video') {
                        $('.body,html').animate({ scrollTop: $parrent.next().offset().top }, 700);
                    } else {
                        var newVideoBlock = $(this).closest('.comics_block').find('> .version_block[data-version="' + $versionNum + '"]').find('.video_block');
                        var newVideo = $(this).closest('.comics_block').find('> .version_block[data-version="' + $versionNum + '"]').find('.video_block video')[0];
                        stopVideos();
                        newVideoBlock.closest('.comics_block').find('> .actions_block').hide();
                        if (newVideo.currentTime == newVideo.duration) {
                            newVideo.currentTime = 0;
                            newVideo.play();
                        } else {
                            newVideo.play();
                        }


                        newVideo.controls = true;
                        newVideo.addEventListener("timeupdate", function(event) {
                            if (this.currentTime + 5 >= this.duration) {
                                newVideoBlock.closest('.comics_block').find('> .actions_block').fadeIn(3000);
                            }
                        });
                        setTimeout(function() {
                            newVideoBlock.addClass('playing');
                        }, 0)


                    }
                    if ($('.playing').length > 0 && !$('.showed > .comics_block > .video_block').hasClass('playing')) {
                        stopVideos();
                    };
                });

                $(document).on('click', '.actions_block .back_btn', function() {
                    if ($('.comics_inner').data('type') && $('.comics_inner').data('type') == 'video') {
                        $(this).closest('.actions_block').hide();
                        $(this).closest('.version_block').removeClass('showed');
                        $('video').each(function() {
                            $(this)[0].currentTime = 0;
                            $(this)[0].pause();
                            $('.video_block').removeClass('playing');
                        })
                    } else {
                        $('.body,html').animate({ scrollTop: $(this).closest('.version_block').parent().find('> .actions_block').offset().top }, 700);
                    }
                });
                $(document).on('click', '.actions_block .back_to_start', function() {
                    $('.actions_block').hide();
                    $('.version_block').removeClass('showed');
                    $('.video_block').removeClass('playing');
                    $('video').each(function() {
                        $(this)[0].currentTime = 0;
                        $(this)[0].pause();
                    })
                })
            }
        });
    }

    $(document).on('click', '.video_block', function(e) {
        var $videoBlock = $(this);
        if (!$(e.target).is('.actions_block *')) {
            if ($('.playing').length > 0 && !$(this).hasClass('playing')) {
                stopVideos();
            }
            var currentVideo = $(this).find('video')[0];
            if (!currentVideo.paused) {
                $(this).removeClass('playing');
                currentVideo.pause();
                currentVideo.controls = false;
            } else {
                $(this).addClass('playing');
                currentVideo.play();
                currentVideo.controls = true;
                currentVideo.addEventListener("timeupdate", function(event) {
                    if (this.currentTime + 5 >= this.duration) {
                        $videoBlock.closest('.comics_block').find('> .actions_block').fadeIn(3000);
                    }
                });

            }

        }

    })

    $('video').bind("ended", function() {
        $(this).parent('.video_block').removeClass('playing');
        //$(this).closest('.comics_block').find('> .actions_block').slideDown(300);
        //$('body,html').animate({scrollTop: $(this).closest('.comics_block').find('> .actions_block').offset().top},500);
        //$(this)[0].currentTime = 0;
        $(this)[0].controls = false;
    });

    if ($('input[type="file"]').length > 0 || $('#allimgs').length > 0 || $('#videomp4').length > 0) {
        $('input[type="file"]').each(function() {
            attachFile($(this));
        })

        // if ($('.attach_block img').length < 1) {
        //     $('.attach_block').find('.attach_remove').addClass('disable');
        // }
        var attachCount = 1;
        $(document).on('click', '.attach_remove', function() {
            if (!$(this).hasClass('disable')) {
                var attachedFile = $(this).parents('.upload_section').length > 0 ? $(this).parents('.attach_block') : $(this).parents('.attach_block').find('img').length > 0 ? $(this).parents('.attach_block').find('img') : $(this).parents('.attach_block').find('video');
                if (!attachedFile.data('file') || attachedFile.data('file') && attachedFile.data('file') != $('.delete_popup .delete_btn').data('file')) {
                    attachedFile.attr('data-file', 'attached_' + attachCount);
                    $('.delete_popup .delete_btn').attr('data-file', 'attached_' + attachCount);
                    attachCount++;
                }
                $('body').addClass('popup_opened');
                $('.delete_popup').addClass('showed');
            }
        })
    }


    if ($('.create_page textarea').length > 0) {
        autosize($('textarea'));
    };

    $('.comics_tree .toggle_btn').click(function() {
        $('.comics_tree').toggleClass('closed');
    });

    $('.sublist_toggle').click(function() {
        $(this).parents('.version_block').toggleClass('clicked');
        $(this).parents('.version_block').next('ul').stop(true, true).slideToggle(300);
    });

    // if ($('.comics_tree .current').length > 0) {
    //     $('.current').parents('li').find('> .version_block:not(.current) .sublist_toggle').trigger('click');
    // };

    $(document).click(function(e) {
        if ($(e.target).is('.action_toggle') && !$(e.target).parent().hasClass('showed')) {
            e.preventDefault();
            e.stopPropagation();
            $(e.target).parent().addClass('showed').find('.actions_list').stop(true, true).slideDown(300);
        } else {
            $('.actions_block.showed').removeClass('showed').find('.actions_list').slideUp(300);

        };
    })
});

$(window).on('load', function() {

    $('.loader').on('mousewheel', function(e) {
        e.preventDefault();
        e.stopPropagation();
    })



    $(window).resize(function() {
        //detect content min height and show footer 
        detectContentHeight();
        if ($('.category_filter').length > 0) {
            countFilterWidth();
        }

    }).trigger('resize');

    $('html').addClass('loaded');

    setTimeout(function() {
        if ($('.main_section').length > 0) {
            $('.main_section').addClass('showed');
        };

        if ($('.welcome_block').length > 0) {
            $('.welcome_block img').addClass('showed');
        }
    }, 500);
})