require('./includes/main.js');

$('.removeComicsBtn').click(function(e) {

    $('body').removeClass('popup_opened');
    $('.popup_block').removeClass('showed');
    $('html').removeClass('loaded');

    let cID = $(this).data('id');
    let url = $('#removeComicsurl').val();
    var token = $("meta[name='csrf-token']").attr("content");
    if (cID) {
        $.ajax({
            type: 'DELETE',
            url: url + '/' + cID,
            data: {
                _token: token,
                id: cID
            },
            success: function(data) {
                if (data) {
                    location.reload();
                }
            }
        });
    }
});