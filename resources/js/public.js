/**
 * jQuery
 */
// import jQuery from 'jquery';
window.$ = window.jQuery = require('./includes/jquery-3.3.1.js');

/**
 * Get files from /resources/js/public
 */
var req = require.context('./public', true, /^(.*\.(js$))[^.]*$/im);
req.keys().forEach(function(key) {
    req(key);
});