@extends('core::public.master')

@section('title', 'Error 500 – '.$websiteTitle)

@section('bodyClass', 'error-500')
@section('css')
    <link href="{{ App::environment('production') ? mix('css/login.css') : asset('css/login.css') }}" rel="stylesheet">
@endsection
@section('js')
    <script src="{{ App::environment('production') ? mix('js/login.js') : asset('js/login.js') }}"></script>
@endsection

@section('content')
    <div class="content">
        <div class="sign_inner">
            <div class="welcome_block">
                <h1 class="large_title">@lang('Hi! Welcome back')</h1>
                <div class="welcome_text">@lang('Create to change is a platform where you can upload your comics or animated videos')</div>
                <div class="image_block">
                    <img src="{{ asset('img/welcome_image.png') }}" alt="Welcome Image" title="Welcome Image" />
                </div>
            </div>
            <div class="sign_block">
                <div class="title_block">
                    <h2 class="page_title"><span class="num404">500</span> @lang('Sorry, a server error occurred').</h2>
                </div>
                <div class="success_info">
                    <div class="login_success"> {!! trans('Error :code', ['code' => '500']) !!}.</div>
                    <a href="{{ TypiCMS::homeUrl() }}" class="homepage_btn">@lang('Go to homepage')</a>
                </div>
            </div>
        </div>
    </div>
@endsection

