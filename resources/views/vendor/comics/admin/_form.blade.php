@push('js')
    <script src="{{ asset('components/ckeditor4/ckeditor.js') }}"></script>
    <script src="{{ asset('components/ckeditor4/config-full.js') }}"></script>
@endpush

@component('core::admin._buttons-form', ['model' => $model])
@if($model->status == 0 && $model->draft == 0)
<a href="{{ route('admin::pending-view-comics', $model->id) }}" class="btn btn-sm btn-info ms-5" target="_blank">View Comics with Versions</a>
@endif

@endcomponent
{!! BootForm::hidden('id') !!}
<div class="row gx-3">
    <div class="col-md-6">
        {!! BootForm::hidden('status')->value(0) !!}
        {!! BootForm::checkbox(__('Published'), 'status') !!}
    </div>
    <div class="col-md-6">
    {!! BootForm::hidden('draft')->value(0) !!}
    {!! BootForm::checkbox(__('Draft'), 'draft') !!}
    </div>
</div>
{!! BootForm::hidden('image') !!}

@if (isset($model))
@if (isset($model->image) and $model->image)
<div class="fieldset-preview">
    <img class="img-fluid" style="width: 100px" src="{{ $model->imagepath() }}" alt="">
    <small class="text-danger delete-attachment" data-table="settings" data-id="" data-field="image">@lang('Delete')</small>
</div>
@endif
@endif

<div class="row gx-3">
    <div class="col-md-6">
        {!! BootForm::file(__('Image'), 'image') !!}
    </div>
    <div class="col-md-6">
        @isset($user)
        <div class="mb-3"> User: {{ isset($user->full_name) ? $user->full_name : '' }}</div>
        @endif
    </div>
</div>

<div class="row gx-3">
    <div class="col-md-6">
        {!! BootForm::text(__('Title'), 'title')->addClass('title-comics') !!}
    </div>
    <div class="col-md-6">
        {!! BootForm::text(__('Slug'), 'slug')->addClass('slug-comics') !!}
    </div>
</div>
<div class="row gx-3">
    <div class="col-md-4">
        {!! BootForm::select(__('Category'), 'category_id', ComicCategories::allForSelect())->required() !!}
    </div>
    <div class="col-md-4">
        {!! BootForm::select(__('Type'), 'type', $model->getTypes())->required() !!}
    </div>
    <div class="col-md-4">
        {!! BootForm::select(__('Language'), 'lang', $model->getLanguages())->required() !!}
    </div>
</div>

{!! BootForm::textarea(__('Co owners'), 'co_owners')->rows(4) !!}
{!! BootForm::textarea(__('Body'), 'body')->addClass('ckeditor-full') !!}
