@extends('core::admin.master')

@section('title', __('New comic category'))

@section('content')

    <div class="header">
        @include('core::admin._button-back', ['module' => 'comic_categories'])
        <h1 class="header-title">@lang('New comic category')</h1>
    </div>

    {!! BootForm::open()->action(route('admin::index-comic_categories'))->multipart()->role('form') !!}
        @include('comics::admin._form-category')
    {!! BootForm::close() !!}

@endsection
