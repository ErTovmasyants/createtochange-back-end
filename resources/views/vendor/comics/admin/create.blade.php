@extends('core::admin.master')

@section('title', __('New comics'))

@section('content')

    <div class="header">
        @include('core::admin._button-back', ['module' => 'comics'])
        <h1 class="header-title">@lang('New comics')</h1>
    </div>

    {!! BootForm::open()->action(route('admin::index-comics'))->multipart()->role('form') !!}
        @include('comics::admin._form')
    {!! BootForm::close() !!}

@endsection
