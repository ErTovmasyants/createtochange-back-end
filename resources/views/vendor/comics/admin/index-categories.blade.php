@extends('core::admin.master')

@section('title', __('Italian comic categories'))

@section('content')

<item-list
    url-base="/api/comics/categories"
    locale="{{ config('typicms.content_locale') }}"
    fields="id,position,status,title"
    table="comic_categories"
    title="categories"
    appends="thumb"
    :searchable="['title']"
    :sorting="['position']">

    <template slot="back-button">
        @include('core::admin._button-back', ['module' => 'comics'])
    </template>

    <template slot="add-button" v-if="$can('create comic_categories')">
        @include('core::admin._button-create', ['module' => 'comic_categories'])
    </template>

    <template slot="columns" slot-scope="{ sortArray }">
        <item-list-column-header name="checkbox" v-if="$can('update comic_categories')||$can('delete comic_categories')"></item-list-column-header>
        <item-list-column-header name="edit" v-if="$can('update comic_categories')"></item-list-column-header>
        <item-list-column-header name="status_translated" sortable :sort-array="sortArray" :label="$t('Status')"></item-list-column-header>
        <item-list-column-header name="position" sortable :sort-array="sortArray" :label="$t('Position')"></item-list-column-header>
        <item-list-column-header name="title_translated" sortable :sort-array="sortArray" :label="$t('Title')"></item-list-column-header>
    </template>

    <template slot="table-row" slot-scope="{ model, checkedModels, loading }">
        <td class="checkbox" v-if="$can('update comic_categories')||$can('delete comic_categories')"><item-list-checkbox :model="model" :checked-models-prop="checkedModels" :loading="loading"></item-list-checkbox></td>
        <td v-if="$can('update comic_categories')">@include('core::admin._button-edit', ['segment' => 'categories', 'module' => 'comic_categories'])</td>
        <td><item-list-status-button :model="model"></item-list-status-button></td>
        <td><item-list-position-input :model="model"></item-list-position-input></td>
        <td v-html="model.title_translated"></td>
    </template>

</item-list>

@endsection
