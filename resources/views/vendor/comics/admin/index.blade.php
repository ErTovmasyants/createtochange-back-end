@extends('core::admin.master')
@push('css')
    <style>
        .comics_active {
            color: #25b351 !important;
            font-weight: bold;
        }

        .comics_draft {
            color: gray !important;
            font-weight: bold;
        }

        .comics_pending {
            color: #FFAD33 !important;
            font-weight: bold;
        }
        .btn-comics_all:hover,
        .btn-comics_active:hover,
        .btn-comics_draft:hover,
        .btn-comics_pending:hover
        {
            color: white!important;
        }
        .btn-comics_all {
            color: #fff;
            border-color: #151816 !important;
            background-color: #151816 !important;
            font-weight: bold;
        }
        .btn-comics_active {
            color: #fff;
            border-color: #25b351 !important;
            background-color: #25b351 !important;
            font-weight: bold;
        }

        .btn-comics_draft {
            border-color: gray !important;
            background-color: gray !important;
            color: #fff !important;
            font-weight: bold;
        }

        .btn-comics_pending {
            border-color: #FFAD33  !important;
            background-color: #FFAD33  !important;
            color: #fff !important;
            font-weight: bold;
        }

    </style>
@endpush
@section('title', __('Comics'))

@section('content')

    <item-list url-base="/api/comics" locale="{{ config('typicms.content_locale') }}"
        fields="comics.id,comics.title,comics.type,comics.user_id,comics.status,comics.draft,comics.updated_at" table="comics" title="comics" :statuses="{{ json_encode($statuses) }}" :statusbtn='true'
        :exportable="false" :searchable="['comics.title,users.nickname,comics.type']" :sorting="['comics.title','-updated_at']">

        <template slot="add-button" v-if="$can('create comics')">
            @include('core::admin._button-create', ['module' => 'comics'])
            <a href="{{ route('admin::index-comic_categories') }}" class="btn btn-sm btn-secondary"
                v-if="$can('read comic_categories')">
                @lang('Categories')
            </a>
        </template>

        <template slot="columns" slot-scope="{ sortArray }">
            <item-list-column-header name="checkbox" v-if="$can('update comics')||$can('delete comics')">
            </item-list-column-header>
            <item-list-column-header name="edit" v-if="$can('update comics')"></item-list-column-header>
            <item-list-column-header name="title" sortable :sort-array="sortArray" :label="$t('Title')">
            </item-list-column-header>
            <item-list-column-header name="type" :label="$t('Type')"></item-list-column-header>
            <item-list-column-header name="nickname" :label="$t('Nickname')"></item-list-column-header>
            <item-list-column-header name="status" :label="$t('Status')"></item-list-column-header>
            <item-list-column-header name="updated_at" :label="$t('Created date')" sortable :sort-array="sortArray"></item-list-column-header>
        </template>

        <template slot="table-row" slot-scope="{ model, checkedModels, loading }">
            <td class="checkbox" v-if="$can('update comics')||$can('delete comics')">
                <item-list-checkbox :model="model" :checked-models-prop="checkedModels" :loading="loading">
                </item-list-checkbox>
            </td>
            <td v-if="$can('update comics')">@include('core::admin._button-edit', ['module' => 'comics'])</td>
            <td v-html="model.title"></td>
            <td v-html="model.type"></td>
            <td v-html="model.nickname"></td>
            <td class="comics_active" v-if="model.status == 1 && model.draft == 0">Active</td>
            <td class="comics_draft" v-if="model.status == 0 && model.draft == 1">Draft</td>
            <td class="comics_pending" v-if="model.status == 0 && model.draft == 0">Pending</td>
            <td>@{{ model.updated_at | date }}</td>
        </template>

    </item-list>

@endsection
