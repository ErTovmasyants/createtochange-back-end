<ul class="products_list">
    @foreach ($items as $comics)
        <li>
            <a class="product_block" href="{{ $comics->uri() }}">
                <span class="image_block">
                    <img src="{{ $comics->imagepath() }}" alt="" title="{{ $comics->title }}" />
                </span>
                <span class="comics_name">{{ $comics->title }}</span>
            </a>
        </li>
    @endforeach
</ul>