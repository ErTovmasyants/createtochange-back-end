<li>
    <a class="product_block" href="{{ $comic->uri() }}">
        <span class="image_block">
            <img src="{{ $comic->imagepath() }}" alt="" title=""/>
        </span>
        <span class="comics_name">{{ $comic->title }}</span>
    </a>
    <div class="comics_lg">
        <span class="lg_label">@lang('Language:')</span> {{ ucfirst($comic->lang) }}
    </div>
</li>