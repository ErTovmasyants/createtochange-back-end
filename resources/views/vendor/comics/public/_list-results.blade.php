<ul class="comic-list-results-list">
    @foreach ($items as $comic)
    <li class="comic-list-results-item">
        <a class="comic-list-results-item-link" href="{{ $comic->uri() }}" title="{{ $comic->title }}">
            <span class="comic-list-results-item-title">{{ $comic->title }}</span>
        </a>
    </li>
    @endforeach
</ul>
