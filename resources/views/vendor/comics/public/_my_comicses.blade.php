<ul class="products_list">
    @foreach ($items as $comics)
        @if ($draft == 1)
            @if ($comics->step !== null)
                @php
                    $type = '';
                    $vID = '';
                    if (!$comics->image) {
                        $comics->step = 1;
                    }
                @endphp
                @if ($comics->step == 2)
                    @php
                        $vID = '';
                        $type = $comics->type;
                        $Vresult = Versions::getFirstLevel($comics->id);
                        if ($Vresult) {
                            $vID = $Vresult->id;
                        }
                    @endphp
                @endif
            @endif
        @endif
        <li>
            <a class="product_block @if($draft == 0 && $comics->status == 0)pending_cursor @endif" href="@if ($draft == 0 && $comics->status == 1){{ $comics->uri() }}@elseif($draft == 1 && $comics->status == 0) {{ route(config('app.locale') . '::comics-create', [$comics->step, $comics->id, $type, $vID]) }} @elseif($draft == 0 && $comics->status == 0) javascript:void(0)@endif">
                <span
                    class="image_block @if (!$comics->image)no_image @endif">@if ($comics->image)<img src="{{ $comics->imagepath() }}" alt="" title="{{ $comics->title }}" />@endif
                    
                </span>
                <span class="
                    comics_name">{{ $comics->title }}</span>
            </a>
            <div class="comics_lg">
                <span class="lg_label">@lang('Language:')</span> {{ ucfirst($comics->lang) }}
            </div>
            @if (Auth::check() && !Auth::user()->isSuperUser())
                <div class="action_btns">
                    @if ($draft == 1 && $pending == 0)
                        @if ($comics->step !== null)
                            @php
                                $type = '';
                                $vID = '';
                                if (!$comics->image) {
                                    $comics->step = 1;
                                }
                            @endphp
                            @if ($comics->step == 2)
                                @php
                                    $vID = '';
                                    $type = $comics->type;
                                    $Vresult = Versions::getFirstLevel($comics->id);
                                    if ($Vresult) {
                                        $vID = $Vresult->id;
                                    }
                                @endphp
                            @endif
                            <a href="{{ route(config('app.locale') . '::comics-create', [$comics->step, $comics->id, $type, $vID]) }}"
                                class="comics_edit icon_pen"></a>
                        @endif
                    @endif
                    <button class="popup_btn icon_delete delete_btn removeComicsPopupBtn" data-id="{{ $comics->id }}"
                        data-popup="delete_popup"></button>
                </div>
            @endif
        </li>
    @endforeach
</ul>
