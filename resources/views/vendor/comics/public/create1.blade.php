@extends('pages::public.master')

@section('bodyClass', 'body-comics body-comics-index body-page body-page-' . $page->id)
@section('css')
    <link href="{{ App::environment('production') ? mix('css/comics_create.css') : asset('css/comics_create.css') }}"
        rel="stylesheet">
        <style>
            .attach_label_me{
                width: 100%;
            }
        </style>
@endsection
@section('js')
    <script src="{{ asset('js/attach.js') }}"></script>
    <script src="{{ asset('js/select.js') }}"></script>
    <script src="{{ asset('js/jquery.form-validator.js') }}"></script>
    <script src="{{ asset('js/autosize.js') }}"></script>
    <script src="{{ App::environment('production') ? mix('js/comics_create.js') : asset('js/comics_create.js') }}">
    </script>
    <script src="{{ App::environment('production') ? mix('js/back.js') : asset('js/back.js') }}"> </script>
    <script>
        $(':file').on('change', function() {
            var file = this.files[0];

            if (file.size > 0 && file.size < 5120) {
                $('.nextStepBtn').prop('disabled', false);
            }
        });
    </script>
@endsection
@section('page')
    <div class="content">
        <div class="breadcrumbs">
            <div class="page_container">
                <ul>
                    <li><a href="{{ TypiCMS::homeUrl() }}">@lang('Home')</a></li>
                    <li>
                        <div>@lang('Create Comics Step 1')</div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="create_page">
            <div class="page_container">
                <div class="steps_block">
                    <ul class="steps_list">
                        <li>@lang('Step') <span class="step_num">2</span></li>
                        <li class="current_step">@lang('Step') <span class="step_num">1</span></li>
                    </ul>
                </div>
                <h1 class="page_title">@lang('Step 1')</h1>
                @if(config('typicms.howtouse'))
                <a class="guide_link" href="{{ config('typicms.howtouse') }}" data-fancybox="guide_video">@lang('How To Use')</a>
                @endif
                @if ($errors->any())
                    @foreach ($errors->all() as $message)
                        <div class="login_error">@lang($message)</div>
                    @endforeach
                @endif
                {!! BootForm::open()->action(route('comics-save'))->multipart()->role('form')->addClass('main_options') !!}
                {!! BootForm::hidden('keyVal')->value(base64_encode(Auth::user()->nickname)) !!}
                {!! BootForm::hidden('key')->value(base64_encode(Auth::user()->id)) !!}
                {!! isset($draft->id) ? BootForm::hidden('keyDr')->value(base64_encode(1)) : BootForm::hidden('keyDr')->value(base64_encode(0)) !!}
                {!! isset($draft->id) ? BootForm::hidden('keyID')->value(base64_encode($draft->id)) : BootForm::hidden('keyID')->value(base64_encode(0)) !!}
                <div class="page_row">
                    <div class="comics_poster attach_block">
                        <div class="image_block comicsCoverPhoto">@isset($draft->image)<input type="hidden" value="filexist"
                                name="filexist"><img src="{{ $draft->imagepath() }}" alt="">@endisset</div>
                        <div class="actions_block">
                            <div class="actions_label">@lang('Cover photo')</div>
                            <label class="attach_label">
                                <input type="file" name="image" data-preview="on" data-maxsize="5" accept=".png, .jpg, .jpg,.JPEG"
                                    data-sizeerror="large file(max. 5mb)"
                                    data-typeerror="invalid file format (.png, .JPEG, .jpg or .jpg)" />
                                <span class="attach_btn icon_edit"></span>
                            </label>
                            <span class="attach_remove icon_delete" data-popup="delete_popup"></span>
                        </div>
                    </div>
                    <div class="fields_section">
                        <div class="info_fields">
                            <div class="field_block">
                                <div class="field_name">@lang('Title')</div>
                                <input type="text" name="title" placeholder="@lang('Title')" data-validation="required"
                                    value="{{ old('title') ? old('title') : (isset($draft->title) ? $draft->title : '') }}" />
                                <span class="error_hint">@lang('This section is requited') </span>
                            </div>
                            <div class="field_block">
                                <div class="field_name">@lang('Description')</div>
                                <textarea name="body" placeholder="@lang('Description')"
                                    data-validation="required">{!! old('body') ? old('body') : (isset($draft->body) ? $draft->body : '') !!}</textarea>
                                <span class="error_hint">@lang('This section is requited')</span>
                            </div>
                            <div class="field_block">
                                <div class="field_name">@lang('Co-owners')</div>
                                <textarea name="co_owners" placeholder="@lang('Co-owners')"
                                    data-validation="required">{!! old('co_owners') ? old('co_owners') : (isset($draft->co_owners) ? $draft->co_owners : '') !!}</textarea>
                                <span class="error_hint">@lang('This section is requited')</span>
                            </div>
                            <div class="field_block">
                                <div class="field_name">@lang('PDF')</div>
                                <div class="attach_block">
                                    <label class="attach_label attach_label_me {{ isset($draft->pdf) ? 'hidden' : '' }}">
                                        <input type="file" name="pdf" accept=".pdf" data-type="simple" data-maxsize="50"
                                            data-sizeerror="@lang('large file(max. 50mb)')"
                                            data-typeerror="@lang('invalid file format (only pdf)')" />
                                        <span class="attach_btn">@lang('Upload comics pdf here')</span>
                                    </label>
                                    @isset($draft->pdf)
                                        <div class="attached_block" id="PdfBlock">
                                            <input type="hidden" value="pdfexist" name="pdfexist">
                                            <div class="file_name">{{ $draft->pdf }}</div><span
                                                class="file_remove"></span>
                                        </div>  
                                    @endisset
                                   
                                </div>
                            </div>
                        </div>
                        <div class="select_fields">
                            <div class="field_block">
                                <div class="field_name">@lang('Language')</div>
                                <select name="lang" data-placeholder="@lang('Select Language')">
                                    @if (Comics::getLanguages($lang))
                                        @foreach (Comics::getLanguages($lang) as $key => $lang_)
                                            <option @isset($draft->lang) @if ($draft->lang == $key) selected @endif @endisset
                                                value="{{ $key }}">{{ $lang_ }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="field_block">
                                <div class="field_name">@lang('Type')</div>
                                <select name="type" data-placeholder="@lang('Select Type')">
                                    @if (Comics::getTypes())
                                        @foreach (Comics::getTypes() as $key => $type)
                                            <option @isset($draft->type) @if ($draft->type == $key) selected @endif @endisset
                                                value="{{ $key }}">@lang($type)</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="field_block">
                                <div class="field_name">@lang('Category')</div>
                                <select name="category_id" data-placeholder="@lang('Select Category')">
                                    @if ($categories)
                                        @foreach ($categories as $category)
                                            <option @isset($draft->type) @if ($draft->category_id == $category->id) selected @endif @endisset
                                                value="{{ $category->id }}">{{ $category->title }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="btns_section">
                            <button type="submit" class="draft_btn" name="action" value="draft">@lang('Save
                                Draft')</button>
                            <button type="submit" class="next_step_btn validate_btn nextStepBtn" @if(!isset($draft->image))disabled="disabled"@endif
                                name="action" value="next">@lang('Next
                                Step')</button>
                        </div>
                    </div>
                </div>
                {!! BootForm::close() !!}
            </div>
        </div>
    </div>

    <div class="popup_block delete_popup">
        <div class="popup_inner">
            <div class="popup_container">
                <div class="description_block">@lang('Are you sure you want to delete comics poster?')</div>
                <div class="btns_block">
                    <span class="cancel_btn">@lang('Cancel')</span>
                    <button class="delete_btn removeComicsImg">@lang('Delete')</button>
                </div>
            </div>
        </div>
    </div>
@endsection
