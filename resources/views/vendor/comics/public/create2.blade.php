@extends('pages::public.master')

@section('bodyClass', 'body-comics body-comics-index body-page body-page-' . $page->id)
@section('css')
    <link href="{{ App::environment('production') ? mix('css/comics_create.css') : asset('css/comics_create.css') }}"
        rel="stylesheet">
@endsection
@section('js')
    <script src="{{ asset('js/attach.js') }}"></script>
    <script src="{{ asset('js/select.js') }}"></script>
    <script src="{{ asset('js/jquery.form-validator.js') }}"></script>
    <script src="{{ asset('js/autosize.js') }}"></script>
    <script src="{{ App::environment('production') ? mix('js/comics_create.js') : asset('js/comics_create.js') }}">
    </script>
    <script src="{{ App::environment('production') ? mix('js/back.js') : asset('js/back.js') }}"></script>
    <script type="text/javascript">
        function checkallimg() {
            var arr = {};
            $(".files_list .attach_block").each(function(key, value) {
                var images = $(this).find(".image_block img");
                if (images) {
                    if (images.attr('data-imgkey') && (images.attr('src').search("blob") < 0)) {
                        arr['image' + (key + 1)] = images.attr('data-imgkey');
                    } else {
                        arr['image' + (key + 1)] = images.attr('src');
                    }
                }
            });
            var myJSON = JSON.stringify(arr);
            $('#allimgs').val(myJSON);
        }

        function checkVideo() {
            var arr = {};
            var video = $('.video_block').find('video');
            if (video.length) {
                if (video.attr('data-videokey')) {
                    arr['video_mp4'] = video.attr('data-videokey');
                } else {
                    arr['video_mp4'] = video.attr('src');
                }
            }
            var myJSON = JSON.stringify(arr);
            $('#videomp4').val(myJSON);
        }
        $('.removeVpopupBtn').click(function(e) {
            $('.removeVbtn').attr('data-id', $(this).data('id'));
        });

        $(".upload_section, .removeVbtn").click(function(e) {
            var tm = 500;
            if (e.target.className == 'move_down' || e.target.className == 'move_up') {
                tm = 1500;
            }
            setTimeout(function() {
                checkallimg();
                checkVideo();
            }, tm);
        });
        function chekupload() {
            setTimeout(function() {
                checkallimg();
                checkVideo();
            }, 500);
        }
    </script>
@endsection
@php
$errorID = Session::get('errorId');
@endphp
@section('page')
    <div class="content">
        <div class="breadcrumbs">
            <div class="page_container">
                @include('core::public._back-btn')
                <ul>
                    <li><a href="{{ TypiCMS::homeUrl() }}">@lang('Home')</a></li>
                    <li><a href="{{ route(config('app.locale') . '::comics-create', [1, $id]) }}">@lang('Create Comics
                            Step 1')</a></li>
                    <li>
                        <div>@lang('Create Comics Step 2')</div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="create_page">
            <div class="large_container">
                <div class="steps_block">
                    <ul class="steps_list">
                        <li class="current_step">@lang('Step') <span class="step_num">2</span></li>
                        <li>@lang('Step') <span class="step_num">1</span></li>
                    </ul>
                </div>
                @isset($comics)
                @endisset
                <h1 class="page_title">@lang('Step') 2</h1>
                @if ($errors->any())
                    @foreach ($errors->all() as $message)
                        <div class="login_error">@lang($message)</div>
                    @endforeach
                @endif
                @if ($message = session('status'))
                    <div class="login_success">@lang($message)</div>
                @endif
                <div class="create_section">
                    <div class="comics_tree">
                        <span class="toggle_btn"></span>
                        <div class="tree_inner">
                            <div class="tree_list">
                                @isset($model)
                                    @if ($model->id)
                                        <input type="hidden" class="addVu" value="{{ route('version-save') }}">
                                        <input type="hidden" class="rVu" value="{{ route('version-remove') }}">
                                        <input type="hidden" class="curU"
                                            value="{{ route(config('app.locale') . '::comics-create', [2, $id, $type]) }}">
                                        <input type="hidden" class="verT"
                                            value="{{ base64_encode($type . 'goanimatedgoFuckYourself') }}">
                                        <input type="hidden" class="cID" value="{{ $id ? $id : '' }}">
                                        <input type="hidden" class="keyVal"
                                            value="{{ base64_encode(Auth::user()->nickname) }}">
                                        <input type="hidden" class="keyU"
                                            value="{{ base64_encode(Auth::user()->id) }}">
                                        <div class="version_block">
                                            <span class="sublist_toggle"></span>
                                            <div class="version_name"><a
                                                    href="{{ route(config('app.locale') . '::comics-create', [$step, $comics->id, $type, $model->id]) }}">@lang('Starting
                                                    comics')</a></div>
                                            <div class="action_btns">
                                                <a href="javascript:void(0)" data-parent-id="{{ $model->id }}"
                                                    class="add_version icon_plus addVersion showLoader"></a>
                                                <a href="{{ route(config('app.locale') . '::comics-create', [$step, $comics->id, $type, $model->id]) }}"
                                                    class="edit_version icon_pen showLoader"></a>
                                            </div>
                                        </div>
                                    @endif
                                @endisset
                                @includeWhen($models->count() > 0, 'versions::public._list', ['items' =>
                                $models,'errorID'=>$errorID])
                            </div>
                        </div>
                    </div>
                    <div class="upload_section">
                        <div class="section_inner">
                            @if ($type)
                                @if ($vID)
                                    {!! BootForm::open()->action(route('versioncontent-save'))->multipart()->role('form') !!}
                                    <input type="hidden" name="verID"
                                        value="{{ base64_encode($vID . '63598254515616goFuckYourself') }}">
                                    <input type="hidden" name="u_htua_i"
                                        value="{{ base64_encode(Auth::user()->id . '854952goFuckYourself') }}">
                                    <input type="hidden" name="u_htua_n"
                                        value="{{ base64_encode(Auth::user()->nickname . 'hixc5t6565____dsfsdfdfvcv___43543543fbdfdgdfgfdgfgoFuckYourself') }}">
                                    <input type="hidden" name="verT"
                                        value="{{ base64_encode($type . 'goanimatedgoFuckYourself') }}">
                                    <input type="hidden" name="cID"
                                        value="{{ base64_encode($id . '8645314986513goFuckYourself') }}">
                                    @if ($vmodel->parent_id > 0 || old('title'))
                                        @php
                                            $title = '';
                                            if (old('title')) {
                                                $title = old('title');
                                            } elseif (isset($vmodel->title)) {
                                                $title = $vmodel->title;
                                            }
                                        @endphp
                                        <div class="version_title">
                                            <div class="field_block">
                                                <div class="field_name">@lang('Version title')</div>
                                                <input type="text" name="title" placeholder="@lang('Enter Version title')"
                                                    data-validation="required" value="{{ $title }}" />
                                                <span class="error_hint">@lang('This section is
                                                    requited')</span>
                                            </div>
                                        </div>
                                    @endif
                                    @if ($type == 'reading')
                                        @isset($imgVidoFiles)
                                            @if (!empty($imgVidoFiles))
                                                <input type="hidden" value="filexist" name="filexist">
                                                <input type="hidden" name="allimgs" value="{{ $vmodel->name }}" id="allimgs">
                                            @endisset
                                        @endif
                                        <div class="files_list">
                                            @isset($imgVidoFiles)
                                                @if (!empty($imgVidoFiles))
                                                    @foreach ($imgVidoFiles as $key => $image)
                                                        <div class="attach_block imgBl">
                                                            <div class="image_block">
                                                                <img data-imgkey="{{ $image }}"
                                                                    src="{{ Storage::url($vmodel->path . '/' . $image) }}">
                                                                <div class="actions_block">
                                                                    <span class="action_toggle icon_pen"></span>
                                                                    <ul class="actions_list">
                                                                        <li>
                                                                            <label class="attach_label">
                                                                                <input type="file" name="name[]"
                                                                                    onchange="chekupload()" data-preview="on"
                                                                                    data-maxsize="5" accept=".png, .jpg, .jpg, .JPEG"
                                                                                    data-sizeerror="large file(max. 5mb)"
                                                                                    data-typeerror="invalid file format (.png, .JPEG, .jpg or .jpg)" />
                                                                                <span
                                                                                    class="attach_btn icon_refresh">@lang('Replace')</span>
                                                                            </label>
                                                                        </li>
                                                                        <li><span class="icon_layout move_btn">@lang('Change
                                                                                Layout')</span></li>
                                                                        <li><span
                                                                                class="icon_delete attach_remove">@lang('Delete')</span>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                    <span class="add_attach icon_upload">@lang('Add Upload')</span>
                                                @endif
                                            @else
                                                <div class="attach_block imgBl">
                                                    <label class="attach_label">
                                                        <input type="file" name="name[]" onchange="chekupload()"
                                                            data-preview="on" data-maxsize="5" accept=".png, .jpg, .jpg, .JPEG"
                                                            data-sizeerror="large file(max. 5mb)"
                                                            data-typeerror="invalid file format (.png, .JPEG, .jpg or .jpg)" />
                                                        <span class="attach_btn icon_upload_image"></span>
                                                    </label>
                                                </div>
                                            @endisset
                                        </div>
                                    @elseif($type == 'animated')
                                        <div class="files_list">
                                            <div class="attach_block">
                                                @if (!empty($imgVidoFiles))
                                                    <input type="hidden" value="filexist" name="filexist">
                                                    <input type="hidden" name="videomp4" value="{{ $vmodel->name }}"
                                                        id="videomp4">
                                                    <div class="video_block"><video
                                                            data-videokey="{{ $imgVidoFiles->video_mp4 }}"
                                                            preload="auto">
                                                            <source
                                                                src="{{ Storage::url($vmodel->path . '/' . $imgVidoFiles->video_mp4) . '#t=1' }}">
                                                        </video><span class="video_screen"
                                                            data-video="start_video"></span>
                                                        <div class="actions_block">
                                                            <span class="action_toggle icon_pen"></span>
                                                            <ul class="actions_list">
                                                                <li>
                                                                    <label class="attach_label">
                                                                        <input type="file" data-type="video" name="name"
                                                                        onchange="chekupload()"
                                                                            data-preview="on" data-maxsize="100"
                                                                            accept=".mp4"
                                                                            data-sizeerror="large file(max. 100mb)"
                                                                            data-typeerror="invalid file format (.mp4 only)" />
                                                                        <span
                                                                            class="attach_btn icon_refresh">@lang('Replace')</span>
                                                                    </label>
                                                                </li>
                                                                <li><span
                                                                        class="icon_delete attach_remove">@lang('Delete')</span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                @else
                                                    <label class="attach_label">
                                                        <input type="file" onchange="chekupload()" data-type="video" name="name" data-preview="on"
                                                            data-maxsize="100" accept=".mp4"
                                                            data-sizeerror="large file(max. 100mb)"
                                                            data-typeerror="invalid file format (.mp4 only)" />
                                                        <span class="attach_btn icon_video"></span>
                                                    </label>
                                                @endif
                                            </div>
                                        </div>
                                    @endif
                                    <div class="save_btn_block">
                                        <button type="submit" class="save_btn validate_btn">@lang('Save')</a>
                                    </div>
                                    {!! BootForm::close() !!}
                                @endif
                            @endif
                        </div>
                        {!! BootForm::open()->action(route('versionfinish-save'))->multipart()->role('form') !!}
                        <input type="hidden" name="cID" value="{{ base64_encode($id . '8645314986513goFuckYourself') }}">
                        <input type="hidden" name="u_htua_i"
                            value="{{ base64_encode(Auth::user()->id . '854952goFuckYourself') }}">
                        <input type="hidden" name="verT" value="{{ base64_encode($type . 'goanimatedgoFuckYourself') }}">
                        <div class="btns_section">
                            <button type="submit" name="action" value="draft"
                                class="draft_btn showLoader">@lang('Draft')</button>
                            <button type="submit" name="action" value="publish"
                                class="next_step_btn showLoader">@lang('Publish')</button>
                        </div>
                        {!! BootForm::close() !!}
                    </div>
                </div>
            </div>
        </div>

        <div class="popup_block delete_popup">
            <div class="popup_inner">
                <div class="popup_container">
                    <div class="description_block rDb">@lang('Are you sure you want to remove?')</div>
                    <div class="btns_block">
                        <span class="cancel_btn">@lang('Cancel')</span>
                        <button class="delete_btn removeVbtn">@lang('Delete')</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="tools_block">
            @if ($type)
                @if ($type == 'reading')
                    <div class="attach_block imgBl">
                        <label class="attach_label">
                            <input type="file" name="name[]" onchange="chekupload()" data-preview="on" data-maxsize="5"
                                accept=".png, .jpg, .jpg, .JPEG" data-sizeerror="large file(max. 5mb)"
                                data-typeerror="invalid file format (.png, .JPEG, .jpg or .jpg)" />
                            <span class="attach_btn icon_upload_image"></span>
                        </label>
                    </div>
                    <div class="actions_block">
                        <span class="action_toggle icon_pen"></span>
                        <ul class="actions_list">
                            <li>
                                <label class="attach_label">
                                    <span class="attach_btn icon_refresh">@lang('Replace')</span>
                                </label>
                            </li>
                            <li><span class="icon_layout move_btn">@lang('Change Layout')</span></li>
                            <li><span class="icon_delete attach_remove">@lang('Delete')</span></li>
                        </ul>
                    </div>
                    <span class="add_attach icon_upload">@lang('Add Upload')</span>
                @elseif($type == 'animated')
                    <div class="attach_block">
                        <label class="attach_label">
                            <input type="file" data-type="video" name="name" onchange="chekupload()" data-preview="on" data-maxsize="100"
                                accept=".mp4" data-sizeerror="large file(max. 100mb)"
                                data-typeerror="invalid file format (.mp4 only)" />
                            <span class="attach_btn icon_video"></span>
                        </label>
                    </div>
                    <div class="actions_block">
                        <span class="action_toggle icon_pen"></span>
                        <ul class="actions_list">
                            <li>
                                <label class="attach_label">
                                    <span class="attach_btn icon_refresh">@lang('Replace')</span>
                                </label>
                            </li>
                            <li><span class="icon_delete attach_remove">@lang('Delete')</span></li>
                        </ul>
                    </div>
                @endif
            @endif
        </div>
    @endsection
