@extends('pages::public.master')

@section('bodyClass', 'body-comics body-comics-index body-page body-page-'.$page->id)
@section('css')
    <link href="{{ App::environment('production') ? mix('css/comics_listing.css') : asset('css/comics_listing.css') }}" rel="stylesheet">
@endsection
@section('js')
    <script src="{{ App::environment('production') ? mix('js/faq.js') : asset('js/faq.js') }}"></script>
@endsection
@section('page')
<div class="content">
    <div class="breadcrumbs">
        <div class="page_container">
        <ul>
            <li><a href="{{ TypiCMS::homeUrl() }}">@lang('Home')</a></li>
            <li><div>@lang(ucfirst($type).' Comics' )</div></li>
        </ul>
    </div>
    </div>
    <div class="listing_inner">
        <div class="page_container">
            <h1 class="page_title">@lang(ucfirst($type).' Comics' ) </h1>
            <div class="category_filter" data-more="More">
                <ul>
                    <li><a href="{{ route($lang.'::comics-type',$type) }}" @if(!isset($current_category))  {{ 'class=selected' }}  @endif data-text="@lang('All Categories')"><span>@lang('All Categories')</span></a></li>
                    @isset($categories)
                        @if($categories)
                        @foreach ($categories as $item)
                            <li><a href="{{ route($lang.'::comics-category',[$type,$item->slug]) }}" @isset($current_category) {{$item->id == $current_category->id ? 'class=selected' : '' }} @endisset data-text="{{ $item->title }}"><span>{{ $item->title }}</span></a></li>
                        @endforeach
                        @endif
                    @endisset
                </ul>
            </div>
            @includeWhen($models->count() > 0, 'comics::public._list', ['items' => $models])
            {!! $models->appends(Request::except('page'))->links('pagination::default') !!}
        </div>
    </div>
 </div>
@endsection
