@extends('core::public.master')

@section('title', $model->title . ' – ' . __('Comics') . ' – ' . $websiteTitle)
@section('ogTitle', $model->title)
@section('description', $model->present()->body)
@section('ogImage', $model->imagepath())
@section('bodyClass', 'body-comics body-comic-' . $model->id . ' body-page body-page-' . $page->id)
@section('css')
    <link href="{{ App::environment('production') ? mix('css/comics_inner.css') : asset('css/comics_inner.css') }}"
        rel="stylesheet">
@endsection
@section('js')
    <script src="{{ App::environment('production') ? mix('js/comics_inner.js') : asset('js/comics_inner.js') }}"></script>
    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-589071e66b72346f"></script>
@endsection
@section('content')
    <input type="hidden" id="comicsID" value="{{ $model->id }}">
    <div class="content">
        <div class="breadcrumbs">
            <div class="page_container">
                <ul>
                    <li><a href="{{ TypiCMS::homeUrl() }}">@lang('Home')</a></li>
                    <li><a href="{{ Comics::getTypeUri($lang, $model->type) }}">@lang(ucfirst($model->type).' Comics' )</a>
                    </li>
                    <li>
                        <div>{{ $model->title }}</div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="comics_page">
            <div class="comics_main">
                <div class="page_container">
                    <div class="container_inner">
                        <h1 class="page_title">{{ $model->title }}</h1>
                        <div class="image_block">
                            <img src="{{ $model->imagepath() }}" alt="{{ $model->title }}"
                                title="{{ $model->title }}" />
                        </div>
                        <div class="info_block">
                            <div class="short_info">
                                <div class="description_block">{!! $model->present()->body !!}</div>
                            </div>
                            <ul class="comics_params">
                                <li>@lang('Language:') <span class="param_info"> {{ ucfirst($model->lang) }}</span>
                                </li>
                                <li>@lang('Category:') <span class="param_info">
                                        {{ ucfirst($model->category->title) }}</span></li>
                            </ul>
                            <div class="comics_creators">
                                {!! nl2br($model->co_owners) !!}
                            </div>
                        </div>
                        <div class="share_btns">
                            <div class="share_label">@lang('Share')</div>
                            <div class="addthis_inline_share_toolbox_c8yg"></div>
                        </div>
                    </div>

                </div>
            </div>
            @if($vmodel)
            @if ($vmodel->type == 'reading')
                <div class="comics_inner" data-comics="comics1">
                    <div class="comics_block">
                        <div class="page_container">
                            @if ($vmodel)
                                @foreach ($vmodel->firstlevelimages() as $img)
                                    <a href="{{ $img }}" data-fancybox="comics_images"><img
                                            src="{{ $img }}" alt="" title="" /></a>
                                @endforeach
                            @endif
                        </div>
                    </div>
                    @if ($model->pdf)
                        <div class="download_btn">
                            <a href="{!! route('downloadFile', $model->pdf) !!}" download="{{ $model->title }}.pdf">@lang('Download
                                PDF')</a>
                        </div>
                    @endif
                </div>
            @elseif($vmodel->type == 'animated')
                <div class="comics_inner" data-comics="comics2" data-type="video">
                    <div class="comics_block">
                        <div class="page_container">
                            <div class="video_block">
                                <video preload="auto" id="start_video">
                                    <source type="video/mp4" src="{{ $vmodel->getFirstLevelVideo() }}#t=0" />
                                </video>
                                <span class="video_screen" data-video="start_video"></span>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
            @endif
        </div>
    </div>
    <button class="back_to_top icon_down"></button>
    <input type="hidden" class="comics_inner_urlJson" value="{{ route('versionjson-get', $model->id) }}">
    <input type="hidden" id="continue_text" value="@lang('Choose your favorite version to continue')">
    <input type="hidden" id="back_btn_text" value="@lang('Previous list of the versions')">
    <input type="hidden" id="back_start_text" value="@lang('Back to start of comics')">
    <input type="hidden" id="pbPath" value="{{ asset('storage') . '/' }}">
@endsection
