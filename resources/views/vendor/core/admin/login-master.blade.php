<!doctype html>
<html lang="{{ config('typicms.admin_locale') }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="api-token" content="{{ auth()->user()->api_token ?? '' }}">
    @include('core::public._favicons')
    <title>@yield('title') – {{ config('typicms.' . config('typicms.admin_locale') . '.website_title') }}</title>
    <link href="{{ App::environment('production') ? mix('css/login.css') : asset('css/login.css') }}" rel="stylesheet">
    @section('css')
    @show
    @stack('css')
</head>

<body ontouchstart="" class="@can('see navbar')has-navbar @endcan @yield('bodyClass')">
    @section('site-header')
        @include('core::public._site-header')
    @show
    <div id="app" class="@section('mainClass')main @show">
        @yield('content')

    </div>
    @section('site-footer')
        @include('core::public._site-footer')
    @show
    <script src="{{ App::environment('production') ? mix('js/public.js') : asset('js/public.js') }}"></script>
    <script src="{{ asset('js/jquery.form-validator.js') }}"></script>
    <script src="{{ asset('js/select.js') }}"></script>
    <script src="{{ App::environment('production') ? mix('js/login.js') : asset('js/login.js') }}"></script>
    @section('js')
    @show
    @stack('js')


</body>

</html>
