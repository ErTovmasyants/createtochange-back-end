@if (($enabledLocales = TypiCMS::enabledLocales()) and count($enabledLocales) > 1)
    <ul class="lg_list">
        @foreach ($enabledLocales as $locale)
            <li>
                @isset($page)
                    @if ($page->isPublished($locale))
                        @php
                            if (str_replace($lang, $locale, Request::path()) == $page->uri($locale)) {
                                $pgurl = url($page->uri($locale));
                            } else {
                                $pgurl = url(str_replace($lang, $locale, Request::path()));
                            }
                        @endphp
                        <a @if ($locale == $lang)  class="current_lg" @endif
                            href="{{ isset($model) ? url($model->uri($locale)) : $pgurl }}">@lang('db.'.$locale)</a>
                    @else
                        <a @if ($locale == $lang)  class="current_lg" @endif href="{{ url('/' . $locale) }}">@lang('db.'.$locale)</a>
                    @endif
                @else
                    <a @if ($locale == $lang)  class="current_lg"  @endif
                        href="{{ url(str_replace($lang, $locale, Request::path())) }}">@lang('db.'.$locale)</a>
                @endisset
            </li>
        @endforeach
    </ul>
@endif
