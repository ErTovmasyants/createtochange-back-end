<div class="footer">
    <div class="footer_top">
        <div class="page_container">
            <div class="partners_section">
                <div class="footer_subtitle">@lang('Our Partners')</div>
                <ul class="partners_list">
                    <li><img src="{{ asset('css/images/eu_for_armenia.svg') }}" alt="" title="" /></li>
                    <li><img src="{{ asset('css/images/zinanshan.svg') }}" alt="" title="" /></li>
                    <li><img src="{{ asset('css/images/kolba.svg') }}" alt="" title="" /></li>
                    <li><img src="{{ asset('css/images/undp.svg') }}" alt="" title="" /></li>
                </ul>
                <div class="info_block">@block('our_partners')</div>
            </div>
            <div class="kasa_info">
                <div class="image_block">
                    <img src="{{ asset('css/images/kasa.svg') }}" alt="" title="" />
                </div>
                <div class="info_block">@block('kasa')</div>
            </div>
        </div>
    </div>
    <div class="footer_middle">
        <span class="waves">
            <span class="yellow_wave" style="background-image: url('{{ asset('css/images/wave_yellow.svg') }}')"></span>
            <span class="pink_wave" style="background-image: url('{{ asset('css/images/wave_pink.svg') }}')"></span>
            <span class="blue_wave" style="background-image: url('{{ asset('css/images/wave_blue.svg') }}')"></span>
            <span class="white_wave" style="background-image: url('{{ asset('css/images/wave_white.svg') }}')"></span>
        </span>
        <div class="middle_inner">
            <div class="page_container">
                <div class="logo_block">
                    <img src="{{ asset('css/images/main_logo.svg') }}" alt="" title="" />
                </div>
                @menu('footer')
                <div class="footer_contacts">
                    <div class="contacts_subtitle">{{ strip_tags(config('typicms.' . ($lang ?: config('app.locale')) . '.webmaster_company')) }}</div>
                    <ul>
                        <li><a href="tel:{{ config('typicms.webmaster_phone') }}" class="phone_link icon_phone">{{ config('typicms.webmaster_phone') }}</a></li>
                        <li><a href="mailto:{{ config('typicms.webmaster_email') }}" class="icon_globe">{{ config('typicms.webmaster_email') }}</a></li>
                        <li><span class="icon_location">{!! config('typicms.' . ($lang ?: config('app.locale')) . '.webmaster_address') !!}</span></li>
                        <li><a href="{{ config('typicms.facebook_app_id') }}" class="icon_facebook" target="_blank">{{ config('typicms.facebook_app_title') }}</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="footer_bottom">
        <div class="page_container">
            <div class="page_row">
                <div class="copyrights">© {{ now()->year }} @lang('COPY RIGHT RESERVED, KASSA SWIS') <a href="{{ url( ($lang ?: config('app.locale')).'/terms-and-conditions') }}"
                        target="_blank">@lang('Copyright') &#38; @lang('Terms of Use')</a></div>
                <div class="developer">@lang('Development by') <a href="https://smartapaga.com/" target="_blank">Smart Apaga</a></div>
            </div>
        </div>
    </div>

</div>

<div class="loader">
    <img src="{{ asset('css/images/storm.svg') }}" alt="" title="" />
</div>
