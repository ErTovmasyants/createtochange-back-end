<div class="header">
    <div class="header_inner">
        <div class="main_logo">
            <a href="{{ url(TypiCMS::homeUrl()) }}">
                <img src="{{ asset('css/images/main_logo.svg') }}" alt="" title="" />
                </a> 
            </div>
            <div class="menu_block">
                <div class="menu_inner">
                    @menu('main')
                    @section('lang-switcher')
                        @include('core::public._lang-switcher')
                    @show
                </div>
            </div>
            <div class="sign_block">
                @if (Auth::check() && !Auth::user()->isSuperUser())
                @if(Auth::user()->roles()->first()->pivot->role_id == 3)
                    <button class="profile_btn">
                        @if (Auth::user()->image != 'no-image.jpg')
                        <img src="{{ asset('avatar/'.Auth::user()->id.'/'.Auth::user()->image) }}" alt="" title="" />
                        @else
                        <img src="{{ asset('avatar/'.Auth::user()->image) }}" alt="" title="" />
                        @endif
                       
                    </button>
                    <ul class="profile_menu">
                        <li><a href="{!! route($lang.'::account-user',['nickname'=>Auth::user()->nickname]) !!}">@lang('Profile')</a></li>
                        <form action="{{ route(TypiCMS::mainLocale().'::logout') }}" method="post">
                            {{ csrf_field() }}
                        <li> <button type="submit">@lang('Logout', [], config('typicms.admin_locale'))</button></li>
                    </form>
                    </ul>
                    @endif
                @else
                @if (Auth::check() && Auth::user()->isSuperUser())
                <a href="{{ route('admin::dashboard') }}" target="_blank" class="sign_btn" >@lang('Admin Panel')</a>
                @else
                <a href="{{ route($lang . '::login') }}" class="sign_btn @if (Request::path() == $lang . '/login') current_page @endif">@lang('Login')</a>
                @endif
                    
                @endif
            </div>

            <button class="menu_btn">
                <span></span>
            </button>
        </div>
    </div>
