<!doctype html>
<html lang="{{ config('app.locale') }}" class="loaded">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0"/>
    <meta name="format-detection" content="telephone=no"/>
    <title>@yield('title')</title>
    <meta name="description" content="@yield('description')">
    <meta name="keywords" content="@yield('keywords')">

    <meta property="og:site_name" content="{{ $websiteTitle }}">
    <meta property="og:title" content="@yield('ogTitle')">
    <meta property="og:description" content="@yield('description')">
    <meta property="og:type" content="website">
    <meta property="og:url" content="{{ URL::full() }}">
    <meta property="og:image" content="@yield('ogImage')">

    @if (config('typicms.twitter_site') !== null)
        <meta name="twitter:site" content="{{ config('typicms.twitter_site') }}">
        <meta name="twitter:card" content="summary_large_image">
    @endif

    @if (config('typicms.facebook_app_id') !== null)
        <meta property="fb:app_id" content="{{ config('typicms.facebook_app_id') }}">
    @endif

    <link href="{{ App::environment('production') ? mix('css/public.css') : asset('css/public.css') }}"
        rel="stylesheet">

    @include('core::public._feed-links')
    @section('css')
    @show
    @stack('css')
    @include('core::public._favicons')
</head>

<body class="body-{{ $lang }} @yield('bodyClass') @if ($navbar)has-navbar @endif" id="top">
    @section('site-header')
        @include('core::public._site-header')
    @show

    @yield('content')

    @section('site-footer')
        @include('core::public._site-footer')
    @show

    <script src="{{ App::environment('production') ? mix('js/public.js') : asset('js/public.js') }}"></script>
    <script src="{{ asset('js/select.js') }}"></script>
    @can('see unpublished items')
        @if (request('preview'))
            <script src="{{ asset('js/previewmode.js') }}"></script>
        @endif
    @endcan
    @section('js')
    @show
    @stack('js')

</body>

</html>
