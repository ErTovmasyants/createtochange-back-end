@extends('core::admin.master')

@section('title', __('New faq'))

@section('content')

    <div class="header">
        @include('core::admin._button-back', ['module' => 'faqs'])
        <h1 class="header-title">@lang('New faq')</h1>
    </div>

    {!! BootForm::open()->action(route('admin::index-faqs'))->multipart()->role('form') !!}
        @include('faqs::admin._form')
    {!! BootForm::close() !!}

@endsection
