@extends('core::admin.master')

@section('title', __('Faqs'))

@section('content')

<item-list
    url-base="/api/faqs"
    locale="{{ config('typicms.content_locale') }}"
    fields="id,status,title,position"
    table="faqs"
    title="faqs"
    :exportable="true"
    :searchable="['title']"
    :sorting="['position']">

    <template slot="add-button" v-if="$can('create faqs')">
        @include('core::admin._button-create', ['module' => 'faqs'])
    </template>

    <template slot="columns" slot-scope="{ sortArray }">
        <item-list-column-header name="checkbox" v-if="$can('update faqs')||$can('delete faqs')"></item-list-column-header>
        <item-list-column-header name="edit" v-if="$can('update faqs')"></item-list-column-header>
        <item-list-column-header name="status_translated" sortable :sort-array="sortArray" :label="$t('Status')"></item-list-column-header>
        <item-list-column-header name="position" sortable :sort-array="sortArray" :label="$t('Position')"></item-list-column-header>
        <item-list-column-header name="title_translated" sortable :sort-array="sortArray" :label="$t('Title')"></item-list-column-header>
    </template>

    <template slot="table-row" slot-scope="{ model, checkedModels, loading }">
        <td class="checkbox" v-if="$can('update faqs')||$can('delete faqs')"><item-list-checkbox :model="model" :checked-models-prop="checkedModels" :loading="loading"></item-list-checkbox></td>
        <td v-if="$can('update faqs')">@include('core::admin._button-edit', ['module' => 'faqs'])</td>
        <td><item-list-status-button :model="model"></item-list-status-button></td>
        <td><item-list-position-input :model="model"></item-list-position-input></td>
        <td v-html="model.title_translated"></td>
    </template>

</item-list>

@endsection
