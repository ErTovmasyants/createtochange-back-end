<li>
    <span class="num_block">{{ $key+1 }}</span>
    <div class="question_block">{{ $faq->title }}</div>
    <div class="answer_block"> {!! nl2br($faq->summary) !!} </div>
    <button class="toggle_btn icon_down">@lang('Answer')</button>
</li>