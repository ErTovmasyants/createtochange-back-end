<ul class="faq-list-results-list">
    @foreach ($items as $faq)
    <li class="faq-list-results-item">
        <a class="faq-list-results-item-link" href="{{ $faq->uri() }}" title="{{ $faq->title }}">
            <span class="faq-list-results-item-title">{{ $faq->title }}</span>
        </a>
    </li>
    @endforeach
</ul>
