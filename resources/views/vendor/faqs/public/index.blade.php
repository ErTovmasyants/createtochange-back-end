@extends('pages::public.master')

@section('bodyClass', 'body-faqs body-faqs-index body-page body-page-'.$page->id)
@section('css')
    <link href="{{ App::environment('production') ? mix('css/faq.css') : asset('css/faq.css') }}" rel="stylesheet">
@endsection
@section('js')
    <script src="{{ App::environment('production') ? mix('js/faq.js') : asset('js/faq.js') }}"></script>
@endsection
@section('page')
        <div class="content">
			<div class="breadcrumbs">
				<div class="page_container">
				@include('core::public._back-btn')
				<ul>
					<li><a href="{{ TypiCMS::homeUrl() }}">@lang('Home')</a></li>
					<li><div>{{ $page->title }}</div></li>
				</ul>
			</div>
			</div>
			<div class="faq_inner">
				<div class="page_container">
					<h1 class="page_title">{{ $page->title }}</h1>
                    @includeWhen($models->count() > 0, 'faqs::public._list', ['items' => $models])
				</div>
			</div>
 		</div>
@endsection
