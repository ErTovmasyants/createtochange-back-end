@extends('core::public.master')

@section('title', $model->title.' – '.__('Faqs').' – '.$websiteTitle)
@section('ogTitle', $model->title)
@section('description', $model->summary)
@section('ogImage', $model->present()->image(1200, 630))
@section('bodyClass', 'body-faqs body-faq-'.$model->id.' body-page body-page-'.$page->id)

@section('content')

<article class="faq">
    <header class="faq-header">
        <div class="faq-header-container">
            <div class="faq-header-navigator">
                @include('core::public._items-navigator', ['module' => 'Faqs', 'model' => $model])
            </div>
            <h1 class="faq-title">{{ $model->title }}</h1>
        </div>
    </header>
    <div class="faq-body">
        @include('faqs::public._json-ld', ['faq' => $model])
        @empty(!$model->summary)
        <p class="faq-summary">{!! nl2br($model->summary) !!}</p>
        @endempty
        @empty(!$model->image)
        <picture class="faq-picture">
            <img class="faq-picture-image" src="{{ $model->present()->image(2000, 1000) }}" width="{{ $model->image->width }}" height="{{ $model->image->height }}" alt="">
            @empty(!$model->image->description)
            <legend class="faq-picture-legend">{{ $model->image->description }}</legend>
            @endempty
        </picture>
        @endempty
        @empty(!$model->body)
        <div class="rich-content">{!! $model->present()->body !!}</div>
        @endempty
        @include('files::public._documents')
        @include('files::public._images')
    </div>
</article>

@endsection
