@php
    $class = str_replace('active','',$menulink->class);
@endphp
<li id="menuitem_{{ $menulink->id }}" role="menuitem">
    <a href="@if (trim($class) == 'reading' || trim($class) ==
    'animated'){{ url($menulink->href . '/' . $class) }} @else
        {{ $menulink->items->count() > 0 ? '#' : url($menulink->href) }}@endif" @if ($menulink->items->count() > 0) class="submenu_btn icon_down" @endif @if (Request::path() == $menulink->href) class="current" @endif id="
        menuitem_{{ $menulink->id }}_id">
        {{ $menulink->title }}
    </a>
    @if ($menulink->items->count() > 0)
        <ul class="submenu_list" aria-labelledby="menuitem_{{ $menulink->id }}_id">
            @foreach ($menulink->items as $menulink)
                @include('menus::public._item', ['menulink' => $menulink])
            @endforeach
        </ul>
    @endif
</li>
