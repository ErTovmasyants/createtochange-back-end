<li id="menuitem_{{ $menulink->id }}" role="menuitem">
    <a href="{{ $menulink->items->count() > 0 ? '#' : url($menulink->href) }}@if($menulink->class=='read' || $menulink->class=='animate'){{ '/'.$menulink->class }} @endif" @if ($menulink->items->count() > 0) class="submenu_btn icon_down" @endif @if(Request::path() ==  $menulink->href) class="current_page" @endif id="menuitem_{{ $menulink->id }}_id">
       {{ $menulink->title }}
    </a>
</li>