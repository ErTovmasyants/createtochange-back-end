@if ($name == 'main')
    @if ($menu = Menus::getMenu($name))
        @if ($menu->menulinks->count() > 0)
            <ul class="main_menu">
                @foreach ($menu->menulinks as $menulink)
                    @include('menus::public._item')
                @endforeach
            </ul>
        @endif
    @endif
@elseif($name == 'footer')
    @if ($menu = Menus::getMenu($name))
        @if ($menu->menulinks->count() > 0)
            <ul class="footer_menu">
                @foreach ($menu->menulinks as $menulink)
                    @include('menus::public._itemFooter')
                @endforeach
            </ul>
        @endif
    @endif
@endif
