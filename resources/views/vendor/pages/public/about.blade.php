@extends('pages::public.master')
@section('css')
    <link href="{{ App::environment('production') ? mix('css/about.css') : asset('css/about.css') }}" rel="stylesheet">
@endsection
@section('js')
    <script src="{{ asset('js/jquery.scrollbar.js') }}"></script>
    <script src="{{ App::environment('production') ? mix('js/about.js') : asset('js/about.js') }}"></script>
@endsection
@section('page')
    <div class="content">
        <div class="breadcrumbs">
            <div class="page_container">
            <ul>
                <li><a href="{{ TypiCMS::homeUrl() }}">@lang('Home')</a></li>
                <li><div>{{ $page->title }}</div></li>
            </ul>
        </div>
        </div>
        <div class="about_bnner">
            @empty(!$page->image)
                <img src="{{ $page->present()->image() }}" alt="About page banner" title="About page banner" />
            @endempty
        </div>
        <div class="text_section">
            <div class="page_container">
                <h1 class="page_title">{{ $page->title }}</h1>
                <div class="description_block">{!! $page->present()->body !!}</div>
            </div>
        </div>
        <div class="participants">
            <h2 class="page_title">@lang('Our Participants')</h2>
            <div class="description_block">@lang('our participants description')</div>
            @if ($users = Participants::published()->order()->with('image')->get() and
        $users->count() > 0)
                <div class="members_list">
                    <ul>
                        @foreach ($users as $user)
                                <li>
                                    <div class="member_block">
                                        <div class="member_image">
                                            <img src="{{ $user->present()->image() }}" alt=""
                                                title="" />
                                        </div>
                                        <div class="info_block">
                                            <div class="member_name">{{ $user->title }}</div>
                                            <div class="about_member">{!! nl2br($user->summary) !!}</div>
                                            @if($user->url)
                                            <a href="{{ $user->url }}"
                                                class="member_link icon_arrow">@lang('Profile')</a>
                                      @endif
                                            </div>
                                    </div>
                                </li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
    </div>
@endsection
