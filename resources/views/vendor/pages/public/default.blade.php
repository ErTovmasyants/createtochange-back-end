@extends('pages::public.master')
@section('css')
    <link href="{{ App::environment('production') ? mix('css/method.css') : asset('css/method.css') }}" rel="stylesheet">
@endsection
@section('js')
    <script src="{{ App::environment('production') ? mix('js/faq.js') : asset('js/faq.js') }}"></script>
@endsection
@section('page')
    <div class="content">
        <div class="breadcrumbs">
            <div class="page_container">
                <ul>
                    <li><a href="{{ TypiCMS::homeUrl() }}">@lang('Home')</a></li>
                    <li>
                        <div>{{ $page->title }}</div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="text_section">
            <div class="page_container">
                <h1 class="page_title">{{ $page->title }}</h1>
                <div class="description_block">{!! $page->present()->body !!}</div>
                @if ($page->publishedSections->count() > 0)
                <ul class="faq_list">
                    @foreach ($page->publishedSections as $section)
                    <li>
                        <div class="question_block">{{ $section->title }}</div>
                        <div class="answer_block">{!! $section->present()->body !!}</div>
                    </li>
                    @endforeach
                </ul>
                @endif
            </div>
        </div>
    </div>
@endsection
