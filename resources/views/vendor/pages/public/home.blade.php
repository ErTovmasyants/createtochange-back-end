@extends('pages::public.master')
@section('css')
    <link href="{{ App::environment('production') ? mix('css/home.css') : asset('css/home.css') }}" rel="stylesheet">
@endsection
@section('js')
    <script src="{{ App::environment('production') ? mix('js/home.js') : asset('js/home.js') }}"></script>
@endsection
@section('page')
    <div class="content">
        <div class="main_section">
            <div class="section_inner">
                <div class="section_block">
                    <h2 class="section_title"><a href="{{ Comics::getTypeUri($lang, 'reading') }}">@lang('Reading Comics')</a></h2>
                    @if ($r_comicses = Comics::where('status', 1)->where('draft', 0)->where('type', 'reading')->inRandomOrder()->limit(3)->get())
                        @includeWhen($r_comicses->count() > 0, 'comics::public._home-comicses', ['items' => $r_comicses])
                        <div class="see_more">
                            <a href="{{ Comics::getTypeUri($lang, 'reading') }}" class="icon_arrow">@lang('See
                                more')</a>
                        </div>
                    @endif
                </div>
                <div class="section_block">
                    <h2 class="section_title"><a href="{{ Comics::getTypeUri($lang, 'animated') }}">@lang('Animated Comics')</a></h2>
                    @if ($a_comicses = Comics::where('status', 1)->where('draft', 0)->where('type', 'animated')->inRandomOrder()->limit(3)->get())
                        @includeWhen($a_comicses->count() > 0, 'comics::public._home-comicses', ['items' => $a_comicses])
                        <div class="see_more">
                            <a href="{{ Comics::getTypeUri($lang, 'animated') }}" class="icon_arrow">@lang('See
                                more')</a>
                        </div>
                    @endif
                </div>
            </div>
        </div>

        <div class="about_section">
            <div class="page_container">
                <h2 class="section_title">@lang('Be creative! Log in and upload your comics here')</h2>
                <div class="description_block">{!! $page->present()->body !!}</div>
                <ul class="steps_list">
                    <li>
                        <div class="step_name">@lang('Register')</div>
                        <a href="{{ route(config('app.locale').'::comics-create', 1) }}"><img src="{{ asset('css/images/step_2.svg') }}" alt="" title="" /></a>
                    </li>
                    <li>
                        <div class="step_name">@lang('Upload your comics')</div>
                        <a href="{{  route(config('app.locale').'::comics-create', 1) }}"><img src="{{ asset('css/images/step_1.svg') }}" alt="" title="" /></a>
                    </li>
                    <li>
                        <div class="step_name">@lang('Make interactive')</div>
                        <a href="{{  route(config('app.locale').'::comics-create', 1) }}"><img src="{{ asset('css/images/step_3.svg') }}" alt="" title="" /></a>
                    </li>
                    <li>
                        <div class="step_name">@lang('Share with friends')</div>
                        <a href="{{  route(config('app.locale').'::comics-create', 1) }}"><img src="{{ asset('css/images/step_4.svg') }}" alt="" title="" /></a>
                    </li>
                </ul>
                <div class="about_join">
                    @if (!Auth::check())
                    <a href="{{ route(config('app.locale') . '::register') }}">@lang('Sign up')</a>
                    <div class="text_block">@lang('Join us and make comics')</div>
                    @endif
                </div>
            </div>
        </div>
    </div>


    {{-- @if (($slides = Slides::published()->order()->get()) and
    $slides->count() > 0)
                @include('slides::public._slider', ['items' => $slides])
            @endif --}}

    {{-- @if (($latestNews = News::published()->order()->take(3)->get()) and
    $latestNews->count() > 0)
                <div class="news-list-container">
                    <h3 class="news-list-title"><a href="{{ Route::has($lang.'::index-news') ? route($lang.'::index-news') : '/' }}">@lang('Latest news')</a></h3>
                    @include('news::public._list', ['items' => $latestNews])
                    <a class="news-list-btn-more btn btn-light btn-sm" href="{{ Route::has($lang.'::index-news') ? route($lang.'::index-news') : '/' }}">@lang('All news')</a>
                </div>
            @endif --}}

    {{-- @if (($upcomingEvents = Events::upcoming()) and $upcomingEvents->count() > 0)
                <div class="event-list-container">
                    <h3 class="event-list-title"><a href="{{ Route::has($lang.'::index-events') ? route($lang.'::index-events') : '/' }}">@lang('Upcoming events')</a></h3>
                    @include('events::public._list', ['items' => $upcomingEvents])
                    <a class="event-list-btn-more btn btn-light btn-sm" href="{{ Route::has($lang.'::index-events') ? route($lang.'::index-events') : '/' }}">@lang('All events')</a>
                </div>
            @endif --}}

    {{-- @if (($partners = Partners::published()->where('homepage', 1)->get()) and
    $partners->count() > 0)
                <div class="partner-list-container">
                    <h3 class="partner-list-title"><a href="{{ Route::has($lang.'::index-partners') ? route($lang.'::index-partners') : '/' }}">@lang('Partners')</a></h3>
                    @include('partners::public._list', ['items' => $partners])
                    <a class="partner-list-btn-more btn btn-light btn-sm" href="{{ Route::has($lang.'::index-partners') ? route($lang.'::index-partners') : '/' }}">@lang('All partners')</a>
                </div>
            @endif --}}

@endsection
