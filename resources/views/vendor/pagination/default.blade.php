@if ($paginator->hasPages())
<div class="paging">
        <ul>
            {{-- Previous Page Link --}}
            @if ($paginator->onFirstPage())
                <li class="disabled" aria-disabled="true" aria-label="@lang('Previous')">
                </li>
            @else
                <li>
                    <a class="prev_page icon_left" href="{{ $paginator->previousPageUrl() }}" rel="prev" aria-label="@lang('Previous')">@lang('Previous')</a>
                </li>
            @endif

            {{-- Pagination Elements --}}
            @foreach ($elements as $element)
                {{-- "Three Dots" Separator --}}
                @if (is_string($element))
                    <li aria-disabled="true"><span>...</span></li>
                @endif

                {{-- Array Of Links --}}
                @if (is_array($element))
                    @foreach ($element as $page => $url)
                        @if ($page == $paginator->currentPage())
                            <li aria-current="page"><a class="current_page">{{ $page }}</a></li>
                        @else
                            <li><a href="{{ $url }}">{{ $page }}</a></li>
                        @endif
                    @endforeach
                @endif
            @endforeach

            {{-- Next Page Link --}}
            @if ($paginator->hasMorePages())
                <li>
                    <a class="next_page icon_right" href="{{ $paginator->nextPageUrl() }}" rel="next" aria-label="@lang('Next')">@lang('Next')</a>
                </li>
            @else
                <li class="disabled" aria-disabled="true" aria-label="@lang('Next')">
                   
                </li>
            @endif
        </ul>
    </div>
@endif
