@extends('core::admin.master')

@section('title', __('New participant'))

@section('content')

    <div class="header">
        @include('core::admin._button-back', ['module' => 'participants'])
        <h1 class="header-title">@lang('New participant')</h1>
    </div>

    {!! BootForm::open()->action(route('admin::index-participants'))->multipart()->role('form') !!}
        @include('participants::admin._form')
    {!! BootForm::close() !!}

@endsection
