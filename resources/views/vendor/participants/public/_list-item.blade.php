<li class="participant-list-item">
    <a class="participant-list-item-link" href="{{ $participant->uri() }}" title="{{ $participant->title }}">
        <div class="participant-list-item-title">{{ $participant->title }}</div>
        <div class="participant-list-item-image-wrapper">
            @empty (!$participant->image)
            <img class="participant-list-item-image" src="{{ $participant->present()->image(null, 200) }}" width="{{ $participant->image->width }}" height="{{ $participant->image->height }}" alt="{{ $participant->image->alt_attribute }}">
            @endempty
        </div>
    </a>
</li>
