<ul class="participant-list-results-list">
    @foreach ($items as $participant)
    <li class="participant-list-results-item">
        <a class="participant-list-results-item-link" href="{{ $participant->uri() }}" title="{{ $participant->title }}">
            <span class="participant-list-results-item-title">{{ $participant->title }}</span>
        </a>
    </li>
    @endforeach
</ul>
