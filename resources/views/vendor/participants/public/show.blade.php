@extends('core::public.master')

@section('title', $model->title.' – '.__('Participants').' – '.$websiteTitle)
@section('ogTitle', $model->title)
@section('description', $model->summary)
@section('ogImage', $model->present()->image(1200, 630))
@section('bodyClass', 'body-participants body-participant-'.$model->id.' body-page body-page-'.$page->id)

@section('content')

<article class="participant">
    <header class="participant-header">
        <div class="participant-header-container">
            <div class="participant-header-navigator">
                @include('core::public._items-navigator', ['module' => 'Participants', 'model' => $model])
            </div>
            <h1 class="participant-title">{{ $model->title }}</h1>
        </div>
    </header>
    <div class="participant-body">
        @include('participants::public._json-ld', ['participant' => $model])
        @empty(!$model->summary)
        <p class="participant-summary">{!! nl2br($model->summary) !!}</p>
        @endempty
        @empty(!$model->image)
        <picture class="participant-picture">
            <img class="participant-picture-image" src="{{ $model->present()->image(2000, 1000) }}" width="{{ $model->image->width }}" height="{{ $model->image->height }}" alt="">
            @empty(!$model->image->description)
            <legend class="participant-picture-legend">{{ $model->image->description }}</legend>
            @endempty
        </picture>
        @endempty
        @empty(!$model->body)
        <div class="rich-content">{!! $model->present()->body !!}</div>
        @endempty
        @include('files::public._documents')
        @include('files::public._images')
    </div>
</article>

@endsection
