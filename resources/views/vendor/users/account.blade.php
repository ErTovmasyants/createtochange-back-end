@extends('core::public.master')

@section('title', 'My Account - ' . $nickname)
@section('bodyClass', 'auth-background')
@section('css')
    <link href="{{ App::environment('production') ? mix('css/profile.css') : asset('css/profile.css') }}"
        rel="stylesheet">
@endsection
@section('js')
    <script src="{{ App::environment('production') ? mix('js/login.js') : asset('js/login.js') }}"></script>
    <script type="text/javascript">
        $('.removeComicsPopupBtn').click(function(e) {
            $('.removeComicsBtn').attr('data-id', $(this).data('id'));
        });
    </script>
@endsection

@section('content')
    <div class="content">
        <div class="breadcrumbs">
            <div class="page_container">
                <ul>
                    <li><a href="{{ TypiCMS::homeUrl() }}">@lang('Home')</a></li>
                    <li>
                        <div>@lang('Profile')</div>
                    </li>
                    <li>
                        <div>{{ $pageTitle }}</div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="profile_inner">
            <div class="page_container">
                <div class="profile_main">
                    <div class="image_socials">
                        <div class="image_block">
                            <div class="image_inner">
                                @if (Auth::check() && !Auth::user()->isSuperUser())
                                    @if (Auth::user()->image != 'no-image.jpg')
                                        <img src="{{ asset('avatar/' . $accountData->id . '/' . $accountData->image) }}"
                                            alt="" title="" />
                                    @else
                                        <img src="{{ asset('avatar/' . $accountData->image) }}" alt="" title="" />
                                    @endif
                                @else
                                    <img src="{{ asset('avatar/' . $accountData->image) }}" alt="" title="" />
                                @endif
                            </div>
                            @if (Auth::check() && !Auth::user()->isSuperUser())
                                @if (Auth::user()->roles()->first()->pivot->role_id == 3)
                                    <div class="edit_btn">
                                        <a href="{{ $accountData->accountEditUrl() }}" class="icon_edit">@lang('Edit
                                            Profile')</a>
                                    </div>
                                @endif
                            @endif
                        </div>
                        <ul class="socials_list">
                            @if ($icons)
                                @foreach ($icons as $name => $link)
                                    @if ($link)
                                        @foreach ($link as $item)
                                            <li><a href="{{ $item }}" target="_blank"
                                                    class="icon_{{ trim($name) }}_round"></a></li>
                                        @endforeach
                                    @endif
                                @endforeach
                            @endif
                        </ul>
                    </div>
                    <div class="info_block">
                        @if (Auth::check() && !Auth::user()->isSuperUser())
                            @if (Auth::user()->roles()->first()->pivot->role_id == 3)
                                <form action="{{ route(TypiCMS::mainLocale() . '::logout') }}" method="post">
                                    {{ csrf_field() }}
                                    <div class="logout_btn">
                                        <button class="icon_exit" type="submit">@lang('Logout', [],
                                            config('typicms.admin_locale'))</button>
                                    </div>
                                </form>
                            @endif
                        @endif
                        <h1 class="name_block">
                            <span class="nickname">{{ $nickname }}</span>
                            <span class="fullname">(
                                @if (Auth::check() && !Auth::user()->isSuperUser())
                                    @if (Auth::user()->roles()->first()->pivot->role_id == 3)
                                        {{ Auth::user()->first_name . '  ' . Auth::user()->last_name }}
                                    @endif
                                @else
                                    @if ($accountData->first_name && $accountData->last_name)
                                        {{ $accountData->first_name . '  ' . $accountData->last_name }}
                                    @endif
                                @endif)
                            </span>
                        </h1>
                        <div class="description_block">
                            <div class="location_block">
                                @if ($accountData->country && $accountData->city)
                                    {{ $accountData->city . ' , ' . $accountData->country }}
                                @endif
                            </div>
                            <div class="short_info">
                                @if ($accountData->bio)
                                    {{ $accountData->bio }}
                                @endif
                            </div>
                        </div>
                        @if (Auth::check() && !Auth::user()->isSuperUser())
                            @if (Auth::user()->roles()->first()->pivot->role_id == 3)
                                <input type="hidden" id="removeComicsurl" value="{{ route('remove-comics') }}">
                                <input type="hidden" id="csrf" value="{{ csrf_token() }}">
                                <div
                                    class="{{ Request::path() }} page_btns {{ route(config('app.locale') . '::account-user', $nickname) }}">
                                    <a href="{{ route(config('app.locale') . '::account-user', [$nickname, 'reading']) }}"
                                        @if (isset($draft) && $draft == 0 && $pending == 0)
                                        class="selected"
                            @endif
                        @endif
                        >@lang('My Comicses')</a>
                        <a href="{{ route(config('app.locale') . '::account-user-draft', $nickname) }}" @if (isset($draft) && $draft == 1)
                            class="selected"
                            @endif>@lang('Draft')({{ $draftCount ? $draftCount : 0 }})</a>
                        @if (isset($pendingCount))
                            <a href="{{ route(config('app.locale') . '::account-user-pending', $nickname) }}"
                                @if (isset($pending) && $pending == 1)
                                class="selected"
                        @endif>@lang('Pending') ({{ $pendingCount ? $pendingCount : 0 }})</a>
                        @endif
                        <a href="{{ route(config('app.locale') . '::comics-create', 1) }}"
                            class="add_btn icon_plus">@lang('Add Comics')</a>
                    </div>
                    @endif
                </div>
            </div>
            @if (isset($draft) && $draft == 0 && $pending == 1)
                <div class="about_draft">
                    <div class="description_block">@lang('Pending Description')</div>
                </div>
            @endif
            @if (isset($draft) && $draft == 1)
                <div class="about_draft">
                    <div class="description_block">@lang('Draft Description')</div>
                </div>
            @elseif($draft == 0 && $pending == 0)
                <ul class="switch_buttons">
                    <li><a href="{{ route(config('app.locale') . '::account-user', [$nickname, 'reading']) }}"
                            {{ $type == 'reading' || !$type ? 'class=selected' : '' }}>@lang('Reading Comics')</a></li>
                    <li><a href="{{ route(config('app.locale') . '::account-user', [$nickname, 'animated']) }}"
                            {{ $type == 'animated' ? 'class=selected' : '' }}>@lang('Animated Comics')</a></li>
                </ul>
            @endif
            @includeWhen($comicses->count() > 0, 'comics::public._my_comicses', ['items' => $comicses,'draft'=>$draft])
        </div>
    </div>
    </div>

    <div class="popup_block delete_popup">
        <div class="popup_inner">
            <div class="popup_container">
                <div class="description_block">@lang('Are you sure you want to delete this comics?')</div>
                <div class="btns_block">
                    <span class="cancel_btn">@lang('Cancel')</span>
                    <button class="delete_btn removeComicsBtn">@lang('Delete')</button>
                </div>
            </div>
        </div>
    </div>
@endsection
