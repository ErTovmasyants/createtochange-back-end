@extends('core::public.master')

@section('title', Auth::user()->nickname)
@section('bodyClass', 'auth-background')
@section('css')
    <link href="{{ App::environment('production') ? mix('css/profile-edit.css') : asset('css/profile-edit.css') }}"
        rel="stylesheet">
@endsection
@section('js')
    <script src="{{ asset('js/attach.js') }}"></script>
    <script src="{{ asset('js/jquery.form-validator.js') }}"></script>
    <script src="{{ App::environment('production') ? mix('js/profile-edit.js') : asset('js/profile-edit.js') }}"></script>
    <script src="{{ App::environment('production') ? mix('js/back.js') : asset('js/back.js') }}"></script>
@endsection

@section('content')
    <div class="content">
        <div class="breadcrumbs">
            <div class="page_container">
                <ul>
                    <li><a href="{{ TypiCMS::homeUrl() }}">@lang('Home')</a></li>
                    <li><a href="{!! route(config('app.locale') . '::account-user', ['nickname' => Auth::user()->nickname]) !!}">@lang('Profile')</a></li>
                    <li>
                        <div>@lang('Edit')</div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="profile_inner editProfileBlock">
            <div class="page_container">
                <h1 class="page_title">@lang('Edit Profile')</h1>
                @if ($errors->any())
                    @foreach ($errors->all() as $message)
                        <div class="login_error">@lang($message)</div>
                    @endforeach
                @endif
                @include('users::_status')
                {!! BootForm::open()->put()->action(route(config('app.locale') . '::account-action-update', Auth::user()->id))->multipart()->role('form')->addClass('edit_section') !!}
                <div class="profile_image">
                    <div class="fields_label">@lang('Profile image')</div>
                    <div class="attach_block">
                        @if (Auth::user()->image != 'no-image.jpg')
                        <div class="image_block"><img src="{{ asset('avatar/' . Auth::user()->id . '/' . Auth::user()->image) }}" alt=""></div>
                        @else
                        <div class="image_block"></div>
                        @endif
                        <label class="attach_label">
                            <input type="file"  id="image" name="image" data-preview="on" data-maxsize="5" accept=".png, .jpg, .JPEG" data-sizeerror="large file(max. 5mb)"  data-typeerror="invalid file format (.png, .jpg or .JPEG)"/>
                            <span class="attach_btn icon_edit">@lang('Edit Photo')</span>
                        </label>
                    </div>
                </div>
                <div class="fields_section">
                    <div class="fields_label">@lang('Personal')</div>
                    <div class="form_fields">
                        <div class="field_block">
                            <div class="field_name">@lang('Name')</div>
                            <input type="text" name="first_name" placeholder="Name" id="first_name" autocomplete="off"
                                data-validation="required" value="{{ Auth::user()->first_name }}" />
                            <span class="error_hint">@lang('This section is required') </span>
                        </div>
                        <div class="field_block">
                            <div class="field_name">@lang('Surname')</div>
                            <input type="text" name="last_name" id="last_name" placeholder="Surname" autocomplete="off"
                                data-validation="required" value="{{ Auth::user()->last_name }}" />
                            <span class="error_hint">@lang('This section is required') </span>
                        </div>
                        <div class="field_block">
                            <div class="field_name">@lang('Nickname')</div>
                            <input type="text" name="nickname" id="nickname" placeholder="Nickname" autocomplete="off"
                                data-validation="required" value="{{ Auth::user()->nickname }}" />
                            <span class="error_hint">@lang('This section is required') </span>
                        </div>
                        <div class="field_block">
                            <div class="field_name">@lang('Phone Number')</div>
                            <input type="text" name="phone" id="phone" placeholder="Phone Number" autocomplete="off"
                                oninput="this.value=this.value.replace(/[^0-9+]/g,'');"
                                value="{{ Auth::user()->phone }}" />
                        </div>
                        <div class="field_block full_field">
                            <div class="field_name">@lang('E-mail address')</div>
                            <div class="static_field">{{ Auth::user()->email }}</div>
                        </div>
                    </div>
                    <div class="fields_label">@lang('About')</div>
                    <div class="form_fields">
                        <div class="field_block">
                            <div class="field_name">@lang('Country')</div>
                            <input type="hidden" class="countryAjaxUrl" value="{{ route('register-ajax-country') }}">
                            <select name="country" id="country" data-placeholder="@lang('Country')" data-search="5">
                                <option value=""></option>
                                @if ($countries)
                                    @foreach ($countries as $item)
                                        <option @if ($item->iso == Auth::user()->country) selected @endif value="{{ $item->iso }}">
                                            {{ $item->nicename }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="field_block cityBlock">
                            <div class="field_name">@lang('City')</div>
                            <input type="hidden" value="@lang('City')" class="cityFieldName">
                            <input type="hidden" value="{{ Auth::user()->city }}" class="selectedCity">
                            <select name="city" id="city" data-placeholder="@lang('City')" disabled data-search="5">
                            </select>
                        </div>
                        <div class="field_block full_field">
                            <div class="field_name">@lang('Bio')</div>
                            <textarea name="bio" id="bio" placeholder="@lang('Bio')"
                                data-validation="required">{{ Auth::user()->bio }}</textarea>
                            <span class="error_hint">@lang('This section is required') </span>
                        </div>
                        <div class="field_block full_field">
                            <div class="field_name">@lang('Socials')</div>
                            @if (Auth::user()->socials)
                                @foreach (json_decode(Auth::user()->socials) as $item)
                                    <div class="input_block">
                                        <input type="url"
                                            pattern="http(s?):\/\/(?:(?:[a-zA-Z\u00a1-\uffff0-9]+-?)*[a-zA-Z\u00a1-\uffff0-9]+)(?:\.(?:[a-zA-Z\u00a1-\uffff0-9]+-?)*[a-zA-Z\u00a1-\uffff0-9]+)*(?:\.(?:[a-zA-Z\u00a1-\uffff]{2,}))(?::\d{2,5})?(?:\/[^\s]*)?"
                                            name="socials[]" placeholder="@lang('Social media link')"
                                            value="{{ $item }}" />
                                        <span class="field_remove icon_close"></span>
                                    </div>
                                @endforeach
                            @else
                                <div class="input_block">
                                    <input type="url"  autocomplete="off"
                                        pattern="http(s?):\/\/(?:(?:[a-zA-Z\u00a1-\uffff0-9]+-?)*[a-zA-Z\u00a1-\uffff0-9]+)(?:\.(?:[a-zA-Z\u00a1-\uffff0-9]+-?)*[a-zA-Z\u00a1-\uffff0-9]+)*(?:\.(?:[a-zA-Z\u00a1-\uffff]{2,}))(?::\d{2,5})?(?:\/[^\s]*)?"
                                        name="socials[]" placeholder="@lang('Social media link')" />
                                </div>
                            @endif
                            <span class="add_field icon_add">@lang('Add new link')</span>
                        </div>
                    </div>
                    <div class="fields_label">@lang('Change Pasword')</div>
                    <div class="form_fields">
                        <div class="field_block full_field">
                            <div class="field_name">@lang('Current Password')</div>
                            <input type="password" name="current_password"
                                placeholder="&#8226; &#8226; &#8226; &#8226; &#8226; &#8226; &#8226;" />
                            <span class="error_hint">@lang('This section is requited')</span>
                            <span class="type_switch"></span>
                        </div>
                        <div class="field_block">
                            <div class="field_name">@lang('New Password')</div>
                            <input class="password_field" type="password" name="new_password"
                                placeholder="&#8226; &#8226; &#8226; &#8226; &#8226; &#8226; &#8226;" />
                            <span class="type_switch"></span>
                        </div>
                        <div class="field_block">
                            <div class="field_name">@lang('Confirm new password')</div>
                            <input class="confirm_field" type="password" name="password_confirmation"
                                placeholder="&#8226; &#8226; &#8226; &#8226; &#8226; &#8226; &#8226;" />
                            <span class="error_hint">@lang('Passwords do not match')</span>
                            <span class="type_switch"></span>
                        </div>
                    </div>
                    <div class="btn_block">
                        <button type="submit" class="validate_btn">@lang('Save changes')</button>
                    </div>
                </div>
                {!! BootForm::close() !!}
            </div>
        </div>
    </div>
@endsection
