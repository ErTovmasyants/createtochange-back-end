@php
    if(!Str::contains( Redirect::intended()->getTargetUrl(),'admin')){
        $rf = false;
        $ext = 'core::admin.login-master';
    }else{
        $rf = true;
        $ext = 'core::admin.master';
    }
@endphp
@extends($ext)

@section('title', __('Login'))
@section('bodyClass', 'auth-background')
@if(!$rf)
@section('js')
    <script>
        var searchRequest = null;

        $(function() {
            $(".clerr").click(function() {
                $('.emailBlock .emailxisterror').remove();
            });
            var minlengthemail = 3;
            var emailUrl = "{{ route('checkemail') }}";
            var existmsgemail = "{{ __('The email not exist.') }}";
            $("#email").keyup(function() {
                var that = this,
                    value = $(this).val();
                if (value.indexOf("@") >= 0) {
                    if (value.length >= minlengthemail) {
                        if (searchRequest != null)
                            searchRequest.abort();
                        searchRequest = $.ajax({
                            type: "GET",
                            url: emailUrl + '/' + value,
                            dataType: "text",
                            success: function(msg) {
                                if (!msg) {
                                    if ($('.emailxisterror').length <= 0) {
                                        $('.emailBlock').append(
                                            '<span class="error_hint emailxisterror">' +
                                            existmsgemail + '</span>');
                                    }
                                } else {
                                    $('.emailBlock .emailxisterror').remove();
                                }

                            }
                        });
                    }
                }else{
                    $('.emailBlock .emailxisterror').remove();
                }
            });
        });
    </script>
@endsection
@else
@section('page-header')
@endsection
@section('sidebar')
@endsection
@endif

@section('mainClass')
@endsection
@section('errors')
@endsection

@section('content')
@if($rf)
<div id="login" class="container-login auth-container auth-container-sm">

    @include('users::_logo')

    {!! BootForm::open()->addClass('auth-container-form') !!}

        <h1 class="auth-container-title">{{ __('Login') }}</h1>

        @include('users::_status')

        {!! BootForm::email(__('Email'), 'email')->addClass('form-control-lg')->autofocus(true)->required()->autocomplete('username') !!}
        {!! BootForm::password(__('Password'), 'password')->addClass('form-control-lg')->required()->autocomplete('current-password') !!}

        <div class="mb-3">
            {!! BootForm::checkbox(__('Remember Me'), 'remember') !!}
        </div>

        <div class="mb-3 d-grid">
            {!! BootForm::submit(__('Login'), 'btn-primary')->addClass('btn-lg') !!}
        </div>

        <div class="mb-3">
            <span class="form-text">
                <a href="{{ route(app()->getLocale().'::password.request') }}">{{ __('Forgot Your Password?') }}</a>
            </span>
        </div>

    {!! BootForm::close() !!}

    <p class="auth-container-back-to-website">
        <a class="auth-container-back-to-website-link" href="{{ TypiCMS::homeUrl() }}">
            <svg class="me-1" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" d="M15 8a.5.5 0 0 0-.5-.5H2.707l3.147-3.146a.5.5 0 1 0-.708-.708l-4 4a.5.5 0 0 0 0 .708l4 4a.5.5 0 0 0 .708-.708L2.707 8.5H14.5A.5.5 0 0 0 15 8z"/>
            </svg>{{ __('Back to the website') }}
        </a>
    </p>

</div>
@else
<div class="content">
    <div class="sign_inner">
        <div class="welcome_block">
            <h1 class="large_title">@lang('Hi! Welcome back')</h1>
            <div class="welcome_text">@lang('Create to change is a platform where you can upload your comics or animated videos')</div>
            <div class="image_block">
                <img src="{{ asset('img/welcome_image.png') }}" alt="Welcome Image" title="Welcome Image" />
            </div>
        </div>
        <div class="sign_block">
            <ul class="switch_buttons">
                <li><a href="{{ route(config('app.locale') . '::login') }}"
                        @if (Request::path() == config('app.locale') . '/login' || Request::path() == 'login') class="selected" @endif>@lang('Login')</a>
                </li>
                <li><a href="{{ route(config('app.locale') . '::register') }}" @if (Request::path() == config('app.locale') . '/register' || Request::path() == 'register') class="selected" @endif>@lang('Sign
                        Up')</a></li>
            </ul>
            <div id="login" class="form_container">
                @if ($errors->any())
                    @foreach ($errors->all() as $message)
                        <div class="login_error">@lang($message)</div>
                    @endforeach
                @endif
                @include('users::_status')
                {!! BootForm::open() !!}

                <div class="field_block emailBlock">
                    <div class="field_name">@lang('E-mail')</div>
                    <input type="text" name="email" id="email" autofocus="false" placeholder="@lang('E-mail')"
                        data-validation="required">
                    <span class="error_hint">
                        <span class="standard_hint">@lang('This section is required')</span>
                        <span class="individual_hint">@lang('Invalid E-mail address')</span>
                    </span>
                </div>
                <div class="field_block">
                    <div class="field_name">@lang('Password')</div>
                    <input type="password" name="password" id="password" class="password_field"
                        data-validation="required"
                        placeholder="&#8226; &#8226; &#8226; &#8226; &#8226; &#8226; &#8226;">
                    <span class="error_hint">@lang('This section is required')</span>
                    <span class="type_switch"></span>
                </div>
                <div class="remember_forgot">
                    <label class="remember_checkbox">
                        <input type="checkbox" name="remember" value="1" id="remember">
                        <span>{{ __('Remember Me') }}</span>
                    </label>
                    <a href="{{ route(app()->getLocale() . '::password.request') }}"
                        class="forgot_pass_link">{{ __('Forgot Your Password?') }}</a>
                </div>
                <div class="btn_block">
                    <button type="submit" class="validate_btn clerr">@lang('Login')</button>
                </div>
                {!! BootForm::close() !!}
            </div>
        </div>
    </div>
</div>
@endif
    
@endsection
