@extends('core::admin.login-master')

@section('title', __('New password'))
@section('bodyClass', 'auth-background')
@section('css')
    <link href="{{ App::environment('production') ? mix('css/login.css') : asset('css/login.css') }}" rel="stylesheet">
@endsection
@section('js')
    <script src="{{ App::environment('production') ? mix('js/login.js') : asset('js/login.js') }}"></script>
@endsection
@section('page-header')
@endsection
@section('sidebar')
@endsection
@section('mainClass')
@endsection
@section('errors')
@endsection

@section('content')

<div class="content">
    <div class="sign_inner">
       <div class="welcome_block">
        <h1 class="large_title">@lang('Hi! Welcome back')</h1>
        <div class="welcome_text">@lang('Create to change is a platform where you can upload your comics or animated videos')</div>
        <div class="image_block">
            <img src="{{ asset('img/welcome_image.png') }}" alt="Welcome Image" title="Welcome Image" />
        </div>
       </div>
       <div class="sign_block">
           <div class="title_block">
               <h2 class="page_title">{{ __('New password') }}</h2>
           </div>
           <div id="login" class="form_container">
            @if ($errors->any())
            @foreach ($errors->all() as $message)
                <div class="login_error">@lang($message)</div>
            @endforeach
        @endif
            @include('users::_status')
               {!! BootForm::open()->action(route(app()->getLocale().'::password.request')) !!}
                   <div class="field_block">
                       <div class="field_name">@lang('E-mail')</div>
                       <input type="text" name="email" id="email" placeholder="@lang('E-mail')" autofocus="autofocus" data-validation="email" autocomplete="username">
                       <span class="error_hint">
                        <span class="standard_hint">@lang('This section is required') </span>
                        <span class="individual_hint">@lang('Invalid E-mail address')</span>
                       </span>
                   </div>
                   <div class="field_block">
                    <div class="field_name">@lang('Password')</div>
                    <input type="password" name="password" class="password_field" id="password" 
                        autocomplete="off" placeholder="&#8226; &#8226; &#8226; &#8226; &#8226; &#8226; &#8226;"
                        aria-autocomplete="list" data-validation="required">
                    <span class="error_hint">@lang('This section is required')</span>
                    <span class="type_switch"></span>
                </div>
                <div class="field_block">
                    <div class="field_name">@lang('Confirm password')</div>
                    <input type="password" name="password_confirmation" class="confirm_field"
                        id="password_confirmation"
                        placeholder="&#8226; &#8226; &#8226; &#8226; &#8226; &#8226; &#8226;" autocomplete="off">
                    <span class="error_hint">@lang('Passwords do not match')</span>
                    <span class="type_switch"></span>
                </div>
                   {!! BootForm::hidden('token')->value($token) !!}
                   <div class="btn_block">
                       <button type="submit" class="validate_btn">@lang('Change Password')</button>
                   </div>
                 
                   {!! BootForm::close() !!}
           </div>
       </div>
    </div>
   
</div>
@endsection
