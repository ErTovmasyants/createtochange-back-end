@extends('core::admin.login-master')

@section('title', __('Register'))
@section('bodyClass', 'auth-background')
@section('js')
    <script src="{{ App::environment('production') ? mix('js/back.js') : asset('js/back.js') }}"></script>
    <script>
        var searchRequest = null;

        $(function() {
            $('.nicknameBlock .nicknamexisterror').remove();
            var minlength = 4;
            var minlengthemail = 3;
            var nickUrl = "{{ route('checknick') }}";
            var emailUrl = "{{ route('checkemail') }}";
            var existmsgnickname = "{{ __('The nickname has already been taken.') }}";
            var existmsgemail = "{{ __('The email has already been taken.') }}";

            $(".clear").click(function() {
                $('.nicknameBlock .nicknamexisterror').remove();
                $('.emailBlock .emailxisterror').remove();
            });

            $("#nickname").keyup(function() {
                var that = this,
                    value = $(this).val();

                if (value.length >= minlength) {
                    if (searchRequest != null)
                        searchRequest.abort();
                    searchRequest = $.ajax({
                        type: "GET",
                        url: nickUrl + '/' + value,
                        dataType: "text",
                        success: function(msg) {
                            if (msg) {
                                if ($('.nicknamexisterror').length <= 0) {
                                    $('.nicknameBlock').append(
                                        '<span class="error_hint nicknamexisterror">' +
                                        existmsgnickname + '</span>');
                                }
                            } else {
                                $('.nicknameBlock .nicknamexisterror').remove();
                            }
                        }
                    });
                } else {
                    $('.nicknameBlock .nicknamexisterror').remove();
                }
            });
            $("#email").keyup(function() {
                var that = this,
                    value = $(this).val();
                if (value.indexOf("@") >= 0) {
                    if (value.length >= minlengthemail) {
                        if (searchRequest != null)
                            searchRequest.abort();
                        searchRequest = $.ajax({
                            type: "GET",
                            url: emailUrl + '/' + value,
                            dataType: "text",
                            success: function(msg) {
                                if (msg) {
                                    if ($('.emailxisterror').length <= 0) {
                                        $('.emailBlock').append(
                                            '<span class="error_hint emailxisterror">' +
                                            existmsgemail + '</span>');
                                    }
                                } else {
                                    $('.emailBlock .emailxisterror').remove();
                                }
                            }
                        });
                    } else {
                        $('.emailBlock .emailxisterror').remove();
                    }
                } else {
                    $('.emailBlock .emailxisterror').remove();
                }
            });
        });
    </script>
    @if ($errors->any())
        <script>
            setTimeout(function() {
                $('#country option').filter(':selected').trigger('change');
            }, 500)
        </script>
    @endif
@endsection
@section('page-header')
@endsection
@section('sidebar')
@endsection
@section('mainClass')
@endsection
@section('errors')
@endsection

@section('content')
    <div class="content">
        <div class="sign_inner">
            <div class="welcome_block">
                <h1 class="large_title">@lang('Welcome')</h1>
                <div class="welcome_text">@lang('Create to change is a platform where you can upload your comics or animated videos')
                </div>
                <div class="image_block">
                    <img src="{{ asset('img/welcome_image.png') }}" alt="" title="" />
                </div>
            </div>
            <div class="sign_block">
                <ul class="switch_buttons">
                    <li><a href="{{ route(config('app.locale') . '::login') }}"
                            @if (Request::path() == config('app.locale') . '/login' || Request::path() == 'login') class="selected" @endif>@lang('Login')</a></li>
                    <li><a href="{{ route(config('app.locale') . '::register') }}" @if (Request::path() == config('app.locale') . '/register' || Request::path() == 'register') class="selected" @endif>@lang('Sign
                            Up')</a></li>
                </ul>
                <div id="register" class="form_container">
                    @if ($errors->any())
                        @foreach ($errors->all() as $message)
                            <div class="login_error">@lang($message)</div>
                        @endforeach
                    @endif
                    @include('users::_status')
                    {!! BootForm::open() !!}

                    <div class="form_fields">
                        <div class="field_block">
                            <div class="field_name">@lang('Name')</div>
                            <input type="text" name="first_name" placeholder="Name" id="first_name"
                                data-validation="required" value="{{ old('first_name') }}">
                            <span class="error_hint">@lang('This section is required') </span>
                        </div>
                        <div class="field_block">
                            <div class="field_name">@lang('Surname')</div>
                            <input type="text" name="last_name" id="last_name" placeholder="Surname"
                                data-validation="required" value="{{ old('last_name') }}">
                            <span class="error_hint">@lang('This section is required') </span>
                        </div>
                        <div class="field_block nicknameBlock">
                            <div class="field_name">@lang('Nickname')</div>
                            <input type="text" name="nickname" id="nickname" placeholder="Nickname"
                                data-validation="required" value="{{ old('nickname') }}" />
                            <span class="error_hint">@lang('This section is required') </span>
                        </div>
                        <div class="field_block">
                            <div class="field_name">@lang('Phone Number')</div>
                            <input type="text" name="phone" id="phone" placeholder="Phone Number"
                                oninput="this.value=this.value.replace(/[^0-9+]/g,'');" value="{{ old('phone') }}" />

                        </div>
                        <div class="field_block full_field emailBlock">
                            <div class="field_name">@lang('E-mail')</div>
                            <input type="text" name="email" id="email" placeholder="@lang('E-mail')" autocomplete="off"
                                data-validation="email" value="{{ old('email') }}">
                            <span class="error_hint">
                                <span class="standard_hint">@lang('This section is required') </span>
                                <span class="individual_hint">@lang('Invalid E-mail address')</span>
                            </span>
                        </div>
                        <div class="field_block">
                            <div class="field_name">@lang('Country')</div>
                            <input type="hidden" class="countryAjaxUrl" value="{{ route('register-ajax-country') }}">
                            <select name="country" id="country" data-placeholder="@lang('Country')" data-search="5">
                                <option value=""></option>
                                @if ($countries)
                                    @foreach ($countries as $item)
                                        <option
                                            {{ old('country') ? (old('country') == $item->iso ? 'selected' : '') : '' }}
                                            value="{{ $item->iso }}">{{ $item->nicename }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="field_block cityBlock">
                            <div class="field_name">@lang('City')</div>
                            <input type="hidden" value="@lang('City')" class="cityFieldName">
                            <input type="hidden" value="{{ old('city') }}" class="selectedcity">
                            <select name="city" id="city" data-placeholder="@lang('City')" disabled data-search="5">
                            </select>
                        </div>
                        <div class="field_block">
                            <div class="field_name">@lang('Password')</div>
                            <input type="password" name="password" class="password_field" id="password" autocomplete="off"
                                placeholder="&#8226; &#8226; &#8226; &#8226; &#8226; &#8226; &#8226;"
                                aria-autocomplete="list" data-validation="required">
                            <span class="error_hint">@lang('This section is required')</span>
                            <span class="type_switch"></span>
                        </div>
                        <div class="field_block">
                            <div class="field_name">@lang('Confirm password')</div>
                            <input type="password" name="password_confirmation" class="confirm_field"
                                id="password_confirmation"
                                placeholder="&#8226; &#8226; &#8226; &#8226; &#8226; &#8226; &#8226;" autocomplete="off">
                            <span class="error_hint">@lang('Passwords do not match')</span>
                            <span class="type_switch"></span>
                        </div>
                    </div>
                    <div class="btn_block">
                        {!! BootForm::submit(__('Sign Up'), 'validate_btn clear') !!}
                    </div>
                    {!! BootForm::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
