@extends('core::admin.login-master')

@section('title', __('Verify'))
@section('bodyClass', 'auth-background')
@section('css')
    <link href="{{ App::environment('production') ? mix('css/login.css') : asset('css/login.css') }}" rel="stylesheet">
@endsection
@section('js')
    <script src="{{ App::environment('production') ? mix('js/login.js') : asset('js/login.js') }}"></script>
@endsection
@section('mainClass')
@endsection
@section('errors')
@endsection

@section('content')
    <div class="content">
        <div class="sign_inner">
            <div class="welcome_block">
                <h1 class="large_title">@lang('Hi! Welcome back')</h1>
                <div class="welcome_text">@lang('Create to change is a platform where you can upload your comics or animated videos')</div>
                <div class="image_block">
                    <img src="{{ asset('img/welcome_image.png') }}" alt="Welcome Image" title="Welcome Image" />
                </div>
            </div>
            <div class="sign_block">
                <div class="title_block">
                    <h2 class="page_title">@lang('Verification')</h2>
                </div>
                <div class="success_info">
                    <div class="login_success"> {{ __('Your email address has been verified.') }}</div>
                    <a href="{{ TypiCMS::homeUrl() }}" class="homepage_btn">@lang('Go to our homepage')</a>
                </div>
            </div>
        </div>
    </div>

@endsection
