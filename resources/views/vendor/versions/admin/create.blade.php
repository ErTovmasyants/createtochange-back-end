@extends('core::admin.master')

@section('title', __('New version'))

@section('content')

    <div class="header">
        @include('core::admin._button-back', ['module' => 'versions'])
        <h1 class="header-title">@lang('New version')</h1>
    </div>

    {!! BootForm::open()->action(route('admin::index-versions'))->multipart()->role('form') !!}
        @include('versions::admin._form')
    {!! BootForm::close() !!}

@endsection
