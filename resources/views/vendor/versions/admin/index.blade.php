@extends('core::admin.master')

@section('title', __('Versions'))

@section('content')

<item-list
    url-base="/api/versions"
    locale="{{ config('typicms.content_locale') }}"
    fields="id,status,title"
    table="versions"
    title="versions"
    :exportable="true"
    :searchable="['title']"
    :sorting="['title']">

    <template slot="add-button" v-if="$can('create versions')">
        @include('core::admin._button-create', ['module' => 'versions'])
    </template>

    <template slot="columns" slot-scope="{ sortArray }">
        <item-list-column-header name="checkbox" v-if="$can('update versions')||$can('delete versions')"></item-list-column-header>
        <item-list-column-header name="edit" v-if="$can('update versions')"></item-list-column-header>
        <item-list-column-header name="title" sortable :sort-array="sortArray" :label="$t('Title')"></item-list-column-header>
    </template>

    <template slot="table-row" slot-scope="{ model, checkedModels, loading }">
        <td class="checkbox" v-if="$can('update versions')||$can('delete versions')"><item-list-checkbox :model="model" :checked-models-prop="checkedModels" :loading="loading"></item-list-checkbox></td>
        <td v-if="$can('update versions')">@include('core::admin._button-edit', ['module' => 'versions'])</td>
        <td v-html="model.title"></td>
    </template>

</item-list>

@endsection
