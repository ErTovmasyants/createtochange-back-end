<li>
    <div class="version_block {{ isset($errorID) ? ($errorID == $version->id ? 'current ErrorCurrent' : '') : ''  }}{{ isset($vID) ? ($vID == $version->id ? 'current' : '') : ''  }}">
        <span class="sublist_toggle"></span>
        <div class="version_name"><a href="{{ route(config('app.locale') . '::comics-create', [$step, $comics->id,$type,$version->id]) }}">{{ $version->title ? $version->title : __('Version').' '.($key+1) }}</a></div>
        <div class="action_btns">
            <a href="javascript:void(0)" class="add_version icon_plus addVersion showLoader" data-parent-id="{{ $version->id }}"></a>
            <a href="{{ route(config('app.locale') . '::comics-create', [$step, $comics->id,$type,$version->id]) }}" class="edit_version icon_pen showLoader"></a>
            <span data-id="{{ $version->id }}" class="popup_btn icon_delete removeVersion removeVpopupBtn" data-popup="delete_popup"></span>
        </div>
    </div>
    @if ($version->children->count() > 0)
    <ul>
        @foreach ($version->children as $key=> $version)
        @include('versions::public._list-results', ['version' => $version])
        @endforeach
    </ul>
    @endif
</li>