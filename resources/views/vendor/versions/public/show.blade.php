@extends('core::public.master')

@section('title', $model->title.' – '.__('Versions').' – '.$websiteTitle)
@section('ogTitle', $model->title)
@section('description', $model->summary)
@section('ogImage', $model->present()->image(1200, 630))
@section('bodyClass', 'body-versions body-version-'.$model->id.' body-page body-page-'.$page->id)

@section('content')

<article class="version">
    <header class="version-header">
        <div class="version-header-container">
            <div class="version-header-navigator">
                @include('core::public._items-navigator', ['module' => 'Versions', 'model' => $model])
            </div>
            <h1 class="version-title">{{ $model->title }}</h1>
        </div>
    </header>
    <div class="version-body">
        @include('versions::public._json-ld', ['version' => $model])
        @empty(!$model->summary)
        <p class="version-summary">{!! nl2br($model->summary) !!}</p>
        @endempty
        @empty(!$model->image)
        <picture class="version-picture">
            <img class="version-picture-image" src="{{ $model->present()->image(2000, 1000) }}" width="{{ $model->image->width }}" height="{{ $model->image->height }}" alt="">
            @empty(!$model->image->description)
            <legend class="version-picture-legend">{{ $model->image->description }}</legend>
            @endempty
        </picture>
        @endempty
        @empty(!$model->body)
        <div class="rich-content">{!! $model->present()->body !!}</div>
        @endempty
        @include('files::public._documents')
        @include('files::public._images')
    </div>
</article>

@endsection
